function wbrandstudiocookiecheck() {
    if (sessionStorage.getItem("wbrandstudiocookieagree") == "true") {
        document.getElementById("wbrandstudio-cookie-notice").style.display = "none";
    } else {
        document.getElementById("wbrandstudio-cookie-notice").style.display = "flex";
    }
}
function wbrandstudiocookieagreement() {
    sessionStorage.setItem("wbrandstudiocookieagree", "true");
    document.getElementById("wbrandstudio-cookie-notice").style.display = "none";
}

window.onload = wbrandstudiocookiecheck;