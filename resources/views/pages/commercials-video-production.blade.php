@extends('layouts.default')

@section('title')
Award Winning Branding & Digital Marketing Agency | Orange County, CA
@endsection

@section('description')
    We are an award-winning branding and marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('keywords')
branding agency, marketing agency, branding orange county, marketing orange county
@endsection

@section('abstract')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section ('subject')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('brandingHeader')
@section('brandTitle', 'W BRAND STUDIO')
@include('partials.branding-header')
@endsection

@section('content')

    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }
            }
        });
    });
    </script>
    <div id="web-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background-color: #efefef; border-radius: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;
    right: -25px;
    top: -25px;
    background: black;
    color: white;
    opacity: 1;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body">
                    <form action="/website-contact-submit" method="post" style="margin-bottom: 50px;">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="company" placeholder="Company">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="price" required>
                                <option>My budget is:</option>
                                <option>$5,000 - $10,000</option>
                                <option>$10,001 - $20,000</option>
                                <option>$20,001 - $30,000</option>
                                <option>$30,001 - $40,000</option>
                                <option>$40,001 - $50,000</option>
                                <option>$50,000 +</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>I am looking for:</label>
                            <br>
                            <div class="row" style="display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;">
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input type="checkbox" value="branding">Branding</label>
                                    <label class="checkbox-inline w-20p"><input name="service1" type="checkbox" value="visual ID">Visual Identity</label>
                                    <label class="checkbox-inline w-20p"><input name="service2" type="checkbox" value="logo design">Logo Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service3" type="checkbox" value="web dev">Website Development</label>
                                    <label class="checkbox-inline w-20p"><input name="service4" type="checkbox" value="social media management">Social Media Management</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p"  style="margin-left: 10px;"><input name="service5" type="checkbox" value="seo">SEO</label>
                                    <label class="checkbox-inline w-20p"><input name="service6" type="checkbox" value="ppc">PPC</label>
                                    <label class="checkbox-inline w-20p"><input name="service7" type="checkbox" value="video prod">Video/Commercial Production</label>
                                    <label class="checkbox-inline w-20p" ><input name="service8" type="checkbox" value="photography">Photography</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input name="service9" type="checkbox" value="int/ext design">Interior/Exterior Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service10" type="checkbox" value="print">Print Design (i.e. brochure)</label>
                                    <label class="checkbox-inline w-20p"><input name="service11" type="checkbox" value="Naming">Naming</label>
                                    <label class="checkbox-inline w-20p"><input name="service12" type="checkbox" value="marketing">Marketing</label>
                                </div>


                            </div>

                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="comments" placeholder="Comments:">
                        </div>
                        <input type="hidden" name="recaptcha" class="recaptcha">
                        <input type="hidden" name="page" value="video-production">
                        <button type="submit" class="" style="margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;    margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;
    background: orange;
    color: white;
    padding: 10px;
    font-weight: bold;">SUBMIT</button>
                    </form>

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

        <section class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            Local and Nationwide Video Production.
                        </h1>
                        <div class="sm-underline">&nbsp;</div>
                        <p>
                            We are an independent creative, full-service agency located in Newport Beach. From international award-winning commercials for Guinness Book of Records to local campaigns for California Business Bank and others, you get a team that constantly push themselves to innovate. 

<br><br>
Whether you are looking for a 30 second commercial or a five minute video about your company, we do it all. <br><br>
<strong>Contact us today and let's show you what a different type of agency can do for your brand. </strong></p>
<a href="/contact" class="myButton org-btn">Contact us for a free consultation</a>
                            
                   
<div class="page page-about">
        <div class="iframe-container">
            <iframe src="https://player.vimeo.com/video/237808763" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        
 <div class="page page-about">
        <div class="iframe-container">
            <iframe src="https://player.vimeo.com/video/275938930" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        
     
         <div class="page page-about">
        <div class="iframe-container">
            <iframe src="https://player.vimeo.com/video/258029508" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        
        
         <div class="page page-about">
        <div class="iframe-container">
            <iframe src="https://player.vimeo.com/video/276099146" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        
         <div class="page page-about">
        <div class="iframe-container">
            <iframe src="https://player.vimeo.com/video/347934260" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        
   

                      
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6  display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/images/wbrand-lobby.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/images/wbrand-lobby.jpg')}}" alt="Branding for Menu">
                    </div>
                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">Our promise to you</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>
                                            We believe in exploring what's outside the box for the simple solutions that drive relevance, originality and impact. When you partner with us, we guarantee that you'll be immersed with endless possibilities and discover a whole new way of thinking. They've come to realize that we outsmart instead of outspend, and keep returning because we understand that their bottom line is our top priority.
                                        
                                            It's time to work with an agency that will immerse you with possibilities, brainstorm without restriction, work tirelessly beyond all expectation and make you more profitable.
                                        
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/awards-clean.jpg')}}" alt="Smarter Branding">

                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/awards-mobile.jpg')}}" alt="Smarter Branding">

                        <div class="subtext-img-cap hidden-xs">
                            <p>
                                We don't like to brag, but in case you wondered.<br> Yes, we have our share of awards from Cannes France to New York and LA. We have also helped judge the amazing work entered in CLIO Awards.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title right">W. The difference down to the letter.</h2>

                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>
                                            We not only think outside the box, we think outside of the Americas. Our international backgrounds and worldly experiences give real meaning to the phrase 'Think globally, act locally.' We are extremely passionate about branding and totally committed to a higher standard of excellence. And with over 50 major awards for branding, design, video production and marketing in the last 18 months, our peers agree that we are one of the top agencies around.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray text-center tk-futura-pt">
                                            <p class="text-left bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                                <br><br>Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.

                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>

            <!-- <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/michael-k-wollner2.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                   <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">THE W PHILOSOPHY</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>If you put yourself in my shoes, you'll notice they match my wall. </strong>
                                        </h2>
                                        <p class="text-left">We're not kidding when we say that we take branding seriously. The wall in my office is teal with black baseboards. My chair is a modern, black-leather pillow chair. And yes, my shoes are the same color as my wall. Which also compliment the dark teal sofa with black legs that sits across from my desk. Crazy right? Wrong. I truly believe that everything has to reflect your brand. Think of your brand as being liquid, running throughout your corporation and filling every inch of your persona with who you are. The look. The feel. The image. If you're creative and colorful, then your audience should have that same perception.
                                            <br><a href="branding-philosophy">
                                                <span style="color: #f48325">Read the rest of the story &#187;</span></a>
                                        </p> -->
                                        <!-- <div>
                                        <a href="javascript:void(0)" class="hidden-xs button-outline button-outline-about b-first mar-t-2 mar-b-1 bio-button">BIO</a>		-->
                                        <!--<a href="#" class="hidden-xs button-outline button-outline-about mar-t-2 mar-b-1">Interview</a>	-->
                                    <!-- <a href="{{url('interview')}}" class="hidden-xs button-outline button-outline-about b-first mar-t-2 mar-b-1 ">INTERVIEW</a>	-->

                                    </div>

                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>


        <!-- <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/rsquebrand-poster.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title right">Results that pull ahead of the pack. </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">Our goal from the beginning is to make your brand relevant. We'll ensure that it not only resonates with your customers, but your employees as well. By combining the audience's interests with original solutions we'll capture the powerful imagery and unique perspective of your brand. One that has a lasting impact and is best for growing your business. One that will engage the new generation of consumers across both traditional and digital touchpoints. And one that can be created by W Brand Studio, your partner in success. </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>-->

        <div align="center">
            <img class="img-responsive" src="{{url('/assets/images/logos.jpg')}}" alt="" style="PADDING-TOP: 20px; PADDING-BOTTOM: 20px;">
        </div>
    </div>
@endsection
