@extends('layouts.default')

@section('title')
Award Winning Branding & Digital Marketing Agency | Orange County, CA
@endsection

@section('description')
    We are an award-winning branding and marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('keywords')
branding agency, marketing agency, branding orange county, marketing orange county
@endsection

@section('abstract')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section ('subject')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('brandingHeader')
@section('brandTitle', 'Branding')
@section('brandColor', '#0ebed8')
@section('brandHeaderImage','#')
@include('partials.branding-header-capabilities')
@endsection

@section('content')
<div class="page page-about">

	<img class="wid-100 hidden-xs" src="/assets/images/branding-agency.jpg" alt="branding and marketing">

	<div style="color:#ffffff;text-transform: uppercase;padding:10px;margin-bottom:60px;" class="text-center blueish">
		<a class="white-link" href="#simplicity">SIMPLICITY</a> |
		<a class="white-link" href="#standout">MAKE YOUR BRAND STAND OUT</a> |
		<a class="white-link" href="#visual">VISUAL IDENTITY</a> | 
		<a class="white-link" href="#steps">STEPS IN BRANDING</a> | 
		<a class="white-link" href="#total">TOTAL BRANDING</a> | 
		<a class="white-link" href="#clients">WHAT OUR CLIENTS SAY</a>
	</div>

	<div id="simplicity" class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0 container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
	    <h2 class="page-title blueish-text" style="margin-bottom:45px;">
	         The Most Powerful Branding Principle is simplicity.
	     </h2>

	    <div class="sm-underline-blueish">&nbsp;</div>
	    <p style="margin-bottom:15px;">In a world of ever-increasing complexity, brand simplicity is critical for brands to get right, or risk customer disappointment and defection. Successful brands that succeed due to simplicity understand that everything must work together, seamlessly. We believe in simplicity, R.O.I. (Relevance, Originality & Impact), and engaging storytelling. As a full-service branding and digital marketing agency you have everyone you need under one roof. Whether you are looking for brand strategy, brand development, brand messaging, visual identity, SEO & SEM, advertising, logo design, brand-based interior design, or video production, we do it all. Our result-oriented solutions span research, strategy, creative, engagement and execution.</p>
	    <p style="margin-bottom:15px;">We understand the effect a great brand story can have on its audience, and we work around the clock with lots of coffee to produce a design that speaks volumes. We are a decades-old pure branding agency at the core, and since we are a full-service agency, we offer everything you can desire. </p>
	    <p style="margin-bottom:15px;">Our clients are more famous than we are, but our people and experience come from the largest agencies on the planet. We love working with startups and entrepreneurs, helping them achieve their dream with an impressive visual identity and positioning in the market. We are passionate go-getters who will make sure that your vision comes to life boldly.</p>
	    <div style="margin:60px;">
	    	<a href="/contact" class="orange-btn">CONTACT US</a>
	    </div>
	</div>
        <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0" style="margin-bottom:60px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/brand-audit-icon.jpg')}}" alt="">
                                <h2>Brand Audit</h2>
                                <p>Time to dig deep. Do others see you as you see yourself? Are you consistent with your messaging and visual identity? We'll do the research necessary to uncover the staus quo of yoru organization and its products and services.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/brand-strategy-icon.jpg')}}" alt="">
                                <h2>Brand Strategy</h2>
                                <p>Great brand strategies are deeply rooted in the organization's vision., its culture, and its core capabilities. We'll apply our decades of proven skills in brand development to vreate a strategic platform that captures your brand's essence.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/positioning-icon.jpg')}}" alt="">
                                <h2>Positioning</h2>
                                <p>We'll develop the right strategy to position your company in the minds of your customers, create a brand promise for them and encapsulate the company's vision for the future. From there, we'll discover that unique tagline that'll set you apart from your competition. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/design-icon.jpg')}}" alt="">
                                <h2>Design</h2>
                                <p>Visual appeal is one thing, but intelligent design comes from a solid brand strategy. Once we bring your visual identity to life it'll connect to your customers and excite your employees.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="visual" class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn"
                         data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/visual-identity.jpg')}}"
                             alt="Branding works">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/visual-identity.jpg')}}"
                             alt="Branding works">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="blueish-title right">Visual Identity</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">
                                            Brand identity is the visible elements of a brand. It includes your logo, corporate colors, corporate fonts, imagery, tone-of-voice in your writing, website, and physical assets like business cards, interior design, signage, and more. Consistency is vital for visual identity. Click here to read more about visual identity.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

	<div id="standout" class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0 container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
	    <h2 class="page-title blueish-text" style="margin-bottom:45px;">
	        MAKE YOUR BRAND STAND OUT
	    </h2>
	    <div class="sm-underline-blueish">&nbsp;</div>
	    <p style="margin-bottom:15px;">We do all the traditional branding disciplines from Brand Audit, Brand Strategy, and Brand Positioning to Brand Identity and Brand Management. However, as part of Total Branding we do more than that. Consistency is important, so we extend branding to every aspect of your business, from how you answer the phones, the music on hold to your vehicles, uniforms, pictures, interior and furniture.</p>
	    <p style="margin-bottom:15px;">Interior Design is a significant part of branding, but not in the conventional sense. Branding is the basis for all our interior and exterior designs. Most interior designers can make any room look good, but not necessarily on brand. Through collaboration and in-depth consultation, we deliver expert branding, design, and brand communications services to help you achieve your potential.</p>
	</div>

	<!-- SLIDE SHOW -->
	<div id="clients" style="background:#efefef;" class="wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active text-center pt-4 pb-4">
                <div class="container text-center pt-4 pb-4">
                    <h2 class="pb-4">We could never have made it to where we ended up<br>without the branding expertise of Michael and his team.</h2>
                    <i>"I had the pleasure of working with Michael and his team at W Brand during the design, construction and commissioning of our new location in Altadena, CA. Michael and his team have done a superb job trying to meet our requirements
                    while keeping the cost within our allocated budget. They understand where we should and should not spend the money while trying to keep the overall theme robotics/aerospace/high tech intact. Our new facilities look amazing and yet are very functional, which is critical to winning new contracts and staying under budget. W Brand bears my highest recommendations. Michael has also redesigned our logo and developed new branding these were critical as we have been growing and acquiring new companies."</i>
                    <h4 class="pt-4">Kris Zacny, Ph.D.</h4>
                    <p>VP, exploration technologies. HONEYBEE ROBOTICS</p>
                </div>
            </div>
            <div class="carousel-item pt-4 pb-4">
                <div class="container text-center pt-4 pb-4">
                    <h2 class="pb-4">"Michael and his team masterfully guided us from the start to the launch of a sleek and sophisticated brand. </h2>
                    <i>We've been working with W Brand Studio for a little more than 6 years, and they've always exceeded our every expectation. They initially rebuilt a website for a client seeking a total makeover of an uninspired hotel. The new site proved to be instrumental in the rebranding of the hotel. And the results were swift, with the on-line booking pace accelerating rapidly within weeks of the go-live date. They stayed on budget and within the time frame. They absolutely captured the vision for our website and, ultimately, the business. Since then, we worked with W Brand Studio on the branding and launch of a new hotel on South Beach - Vintro. Michael and his team masterfully guided us from the onset, from the birth of the name, to the launch of a sleek and sophisticated brand and website. They hotel claimed a special place in a city where lifestyle hotels are so prevalent but many lacking in authenticity. We trust W Brand Studio; their team and work are phenomenal."</i>
                    <h4 class="pt-4">Robert Todak</h4>
                    <p>Managing Director, Tailored Hospitality, Int.</p>
                </div>
            </div>
            <div class="carousel-item pt-4 pb-4">
                <div class="container text-center pt-4 pb-4">
                     <h2 class="pb-4">The Wollner team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. </h2>
                     <i>W Brand Studio embraced the difficult challenge of re-branding a 40-year-old "active adult" community with over 18,000 residents, and produced a product that is fresh, current, attractive and appropriate for the client. The new Laguna Woods Village logo, an elegant stylized green leaf, reflects the vitality and strength of the community. The Wollner team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection, and the presentation was outstanding. Thank you, W Brand Studio!" </i>
                    <h4 class="pt-4">Wendy Bucknum </h4>
                    <p>CMCA, AMS, CACM, Government & Public Affairs Manager Professional Community Management, Inc </p>
                </div>
            </div>
            <div class="carousel-item pt-4 pb-4">
                <div class="container text-center pt-4 pb-4">
                    <h2 class="pb-4">We are extremely excited about our new direction and excited with what we have created with Michael's assistance and incite.</h2> 
                    <i>"As a regional Insurance Agency, we found the task of creating a marketing theme and identity nearly impossible. We had spent many hours searching for a marketing agency that helped us understand what we are about and what we stand for. Most people we spoke to never took time to understand what we are about and what made us unique. Every time a consultant was brought in it seemed that we were getting the same tired, retread options. After many frustrating meetings, I had the opportunity to meet through a mutual friend, Michael Wollner and our marketing/identity process took off. Michael and his team spent time understanding the culture of Bowermaster and where we have been and where we wanted to go. He was able to get us to think out of the insurance box, which is not easy to do. He gained our confidence through incisive questioning conversation, and observation, which eased us into a decision-making process that, took all the factors we were concerned with in effect. No journey in recreating an image is without its rough moments. With Michael's insight and patience, we were able to get through these rough moments and turn them into learning experiences. I cannot say enough kind words on our dealings with Michael and the process. We are extremely excited about our new direction and excited with what we have created with Michael's assistance and incite. If I can be of any assistance in understanding what Michael and W Brand Studio did for this agency, please don't hesitate to contact me personally." </i>
                    <h4 class="pt-4">Mike Bowermaster</h4>
                    <p>Bowermaster and Associates</p>
                </div>
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
	</div>

	<div id="steps" class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0 container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
		<div class="col-sm-12">
		    <h2 class="page-title blueish-text" style="margin-bottom:45px;">
		        We are serious about your brand, are you?
		    </h2>
		    <div class="sm-underline-blueish">&nbsp;</div>
		    <p style="margin-bottom:15px;">Every brand thinks it's most important thing in their user's life. Usually, it's not. Great brands look to where the brand and the experience fit within their user's life, looking to make a user's life simpoer and easier.</p>
		    <p style="margin-bottom:15px;">These are the steps to follow.</p>	
		</div>
	    <div class="col-sm-12 col-md-6 text-left">
	    	
    		<ul style="font-weight:normal!important;font-size:18px!important;">
				<li style="font-weight:normal!important;font-size:18px!important;">Research. Before we start, we'd love to get to know you, your goals, and your industry.</li>
				<li style="font-weight:normal!important;font-size:18px!important;">Secondly, we will take a good look at your competitors.</li>
				<li style="font-weight:normal!important;font-size:18px!important;">We perform a brand audit to see how your brand is perceived internally and in the marketplace.</li>
				<li style="font-weight:normal!important;font-size:18px!important;">SWOT Analysis.</li>
				<li style="font-weight:normal!important;font-size:18px!important;">Interview management, staff, and customers</li>
				<li style="font-weight:normal!important;font-size:18px!important;">Our strategists look for trends in the marketplace</li>
				<li style="font-weight:normal!important;font-size:18px!important;">A Brand Alignment Strategy is developed based on the Brand Audit, interviews, and SWOT analysis</li>
			</ul>
    		
	    </div>
	    <div class="col-sm-12 col-md-6 text-left">
	    	<p style="margin-bottom:15px;">
	    		<ul style="font-weight:normal!important;font-size:18px!important;">
					<li style="font-weight:normal!important;font-size:18px!important;">We will develop a brand strategy, which includes positioning in the marketplace, messaging, brand architecture, brand promise, elevate brand storytelling and experiences, staying consistent through touchpoints, employee engagement, and more.</li>
					<li style="font-weight:normal!important;font-size:18px!important;">Create a Visual Identity or Brand Identity, including logo design, corporate colors, corporate fonts, tone-of-voice, and imaging</li>
					<li style="font-weight:normal!important;font-size:18px!important;">Implementation of the Brand Identity; website development, interior design, exterior design, uniforms, and vehicles, stationary, sales sheets, presentation materials, and brochures</li>
				</ul>
	    	</p>
	    </div>
	</div>

	<div class="wow fadeInUp" data-wow-offset="100" data-wow-delay="100ms">
		<img class="wid-100 hidden-xs" src="/assets/images/branding-visual-identity2-0.jpg" alt="branding and marketing">
	</div>

</div>
@endsection