@extends('layouts.default')

@section('title')
    Contact Us Today | (657) 232-0110 @endsection

@section('description')
    Give us 15 minutes and we'll let you know how we can help using innovative, proven marketing strategies, and unique designs to deliver measurable results.  @endsection

@section('keywords')
    contact, web design, marketing @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection




@section('brandingHeader')
@section('brandTitle', 'Write to Us')
@include('partials.branding-header')
@endsection



@section('content')
    <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt position-r">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <div class="contact-block waves-effect wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="200ms">
                        <a href="mailto:hello@wbrandstudio.com?Subject=hello">
                            <i class="fa fa-envelope"></i>
                            <h2>Write to us</h2>
                            <p>If you just want to say hello, or you are a vendor, drop us a line so we at least can put your information in our VIP vault.</p>

                            <p class="padding-t-1">
                                hello@wbrandstudio.com
                            </p>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
        <div class="container contact-form">
            <div class="contact-form-close-button">

                <div>x</div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <section id="slick">
                        <div class="feedback-form">
                            <form action="/contact" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title">name</div>
                                                <div class="field">
                                                    <input type="text" name="name" id="name" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your name</span>
                                                </div>
                                                <div class="form-title">company</div>
                                                <div class="field">
                                                    <input type="text" name="company" id="company" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Company</span>
                                                </div>
                                                <div class="form-title">email</div>
                                                <div class="field">
                                                    <input type="email" name="email" id="email" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Email</span>
                                                </div>
                                                <div class="form-title">phone</div>
                                                <div class="field">
                                                    <input type="text" name="phone" id="phone" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title">tell us what you need</div>
                                                <div class="field">
                                                    <textarea name="message" placeholder="" id="message" class="message" required></textarea>
                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                    <span class="slick-tip left">Your message</span>
                                                </div>
                                                <div class="field">
                                                    <div class="g-recaptcha" data-sitekey="6Lf_GycUAAAAANppw1PuSwscyXLSuwIcZOvBJ69r"></div>
                                                </div>
                                                <input type="submit" value="Submit" class="send"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div><!--row-->
        </div><!--container-->
    </section>

    <section class="wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="600ms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 padding-l-0 padding-r-0">
                    <img class="width-100" src="{{url('/assets/images/w-brand-studio-offices.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </section>


@endsection