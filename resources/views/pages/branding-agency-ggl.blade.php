@extends('layouts.default')

@section('title')
    Brand Development | Orange County | Los Angeles | California @endsection

@section('description')
    We are an innovative and creative digital brand development company in Orange County, CA.  Marketing and Branding Agency in Newport Beach, CA. @endsection

@section('keywords')
    branding, graphic design, logo design @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'BRANDING AGENCY')
@include('layout._brandingHeaderLanding')
@endsection



@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                document.getElementById('recaptcha').value = token;
            }
        });
    });
    </script>
    <!-- <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0"  data-wow-offset="100" data-wow-delay="100ms">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>
					We're the Biggest Small Branding Agency in Town.
				</h1>
				<p>
					Big when we need to be, and small when we want to. We have successfully told our client's stories in simple, repeatable and memorable ways with unique design since 1997. Consistency is crucial to the success of your brand. Being consistent in everything you do from website development and logo design, to stationary and vehicle wraps will display a strong a strong visual identity. With the right brand strategy, positioning, messaging and brand promise, we can make you stand out in the crowd and leave your competitors in the dust. All our designs are rooted in a strategy. Nothing gets design before we really know you. We are a one-stop creative branding agency located in Newport Beach. Below is some of the elements from our latest award winning job, created for Renew Landscapes in Bixby Knolls, CA.<br><br> <a href="http://www.wbrandstudio.com/contact" class="myButton">Let's talk</a>

					<br><br><img class="wid-100 hidden-xs" src="{{url('/assets/images/branding-agency-newport.jpg')}}" alt="Branding for Menu"><br><br>



					<h2>Call us today at (949) 955-2705 and get your brand noticed too.</h2>
				</p>
			</div>
		</div>
	</div>
</section> -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <section class="wow fadeInUp opacity-0" id="branding-contact-section" data-wow-offset="100" data-wow-delay="100ms">
        <div class="container-fluid-table display-t-sm width-100">
            <div class="row-table display-t-r-sm">
                <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                    <img class="wid-100 hidden-xs" src="{{url('assets/images/work-sq.jpg')}}" alt="Results">
                    <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                </div>
                <div class="col-sm-6 col-sm-pull-6 display-t-c-sm vertical-a-m-sm" style="padding-top: 8%;vertical-align: top;">
                    <article>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 orange-offset">
                                <div class="text-left tk-futura-pt">
                                    <h1>How do you brand yourself?</h1>

                                    <div class="header-copy">
                                        <p>
                                            The core essence of a brand starts from within. It's a vision from the top management that needs to flow all the way down to the person that sits at the front desk.
                                        </p>
                                        <p>
                                            Your visual identity, the interior and exterior of your building, and all communication, whether online or offline, need to reflect that vision.
                                        </p>
                                        <p>
                                            Allow W Brand Studio to unlock the true power, full potential, and exponential value of your company branding.
                                        </p>
                                        <br>
                                        <a href="javascript:void(0)" class="start-a-project-button"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <br/>
                    <div class="col-sm-4 col-sm-offset-4">
                        <div style="" class="btn btn-lg orange-button-landing text-uppercase" id="contact-contact-block">
                            Let's Talk
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container contact-form">
            <div class="contact-form-close-button branding-close-button">
                <div>X</div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <section id="slick">
                        <div class="feedback-form">
                            <form action="/contact" method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title landing-form-title">name</div>
                                                <div class="field">
                                                    <input type="text" name="name" id="name" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your name</span>
                                                </div>
                                                <div class="form-title landing-form-title">company</div>
                                                <div class="field">
                                                    <input type="text" name="company" id="company" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Company</span>
                                                </div>
                                                <div class="form-title landing-form-title">email</div>
                                                <div class="field">
                                                    <input type="email" name="email" id="email" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Email</span>
                                                </div>
                                                <div class="form-title landing-form-title">phone</div>
                                                <div class="field">
                                                    <input type="text" name="phone" id="phone" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title landing-form-title">tell us what you need</div>
                                                <div class="field">
                                                    <textarea name="message" placeholder="" id="message" class="message" required></textarea>
                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                    <span class="slick-tip left">Your message</span>
                                                </div>
                                                <div class="field">
                                                    <div class="g-recaptcha" data-sitekey="6Lf_GycUAAAAANppw1PuSwscyXLSuwIcZOvBJ69r"></div>
                                                </div>
                                                <input type="submit" value="Submit" class="send"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div><!--row-->
        </div><!--container-->
    </section>

    {{--<section class="wow fadeInUp opacity-0 pad-t-3 pad-b-3 tk-futura-pt" data-wow-offset="100" data-wow-delay="100ms">--}}
    {{--<div class="container-fluid-table display-t-sm width-100">--}}
    {{--<div class="row-table display-t-r-sm">--}}
    {{--<div class="col-md-5 text-right" >--}}
    {{--<img style = "max-width: 50%;" src="{{url('/assets/images/book-landing.png')}}" alt="Branding for Menu">--}}
    {{--</div>--}}
    {{--<div class="col-sm-4 col-sm-offset-1 text-center" >--}}
    {{--<br><br>--}}
    {{--<h2 style="color: #f48325"> "The Branding Blueprint"</h2>--}}
    {{--<h3 style="font-weight: 200">An Insider Guide to Corporate Idenity.</h3>--}}
    {{--<div class="btn btn-lg col-xs-4 col-xs-offset-4 red-button-landing" >--}}
    {{--Download Now--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

    <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

        <div class="container-fluid-table display-t-sm width-100">
            <div class="row-table display-t-r-sm">
                <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                    <img class="wid-100 hidden-xs" src="{{url('/assets/images/about-office.jpg')}}" alt="Branding for Menu">
                    <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                </div>

                <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                    <article>
                        <h2 class="orange-title left">SERVICES</h2>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                <div class="copy-block-gray text-center tk-futura-pt">
                                    <h2 class="text-left">
                                        <strong>We practice what we preach. We walk the walk, not just talk the talk. </strong>
                                    </h2>
                                    <p class="text-left">We're one of the fastest growing branding agencies in Orange County, with insight, experience and energy. Our offices are located in Newport Beach, it's where we provide our clients with these essential services:
                                    </p>
                                    <ul>
                                        <li>Brand Audit</li>
                                        <li>Brand Strategy</li>
                                        <li>Brand Positioning and Promise</li>
                                        <li>Brand Voice</li>
                                        <li>Brand Implementation, incl. Interior Design</li>
                                        <li>Naming</li>
                                        <li>Visual Identity</li>
                                        <li>Logo Development</li>
                                        <li>Website Development</li>
                                        <li>Online Marketing incl. PPC, SEO and Social Media</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>
                        Here's why you should choose us.
                    </h2>
                    <p>

                        We are a passionate and dedicated group of professional madmen, celebrating 20 years in business in 2017. We are uniquely interested in listening to our clients so we can really get to know them before we create a single dot. We are proactive, accessible, collaborative and will always make sure you are happy with the results. If you are looking for someone who can do it all in-house, then you've come to right place. We are a full-service agency and can help you with everything from total branding to online marketing (SEM) and website development, to your logo re-design, visual identity and advertising needs.
                        <br><br><em>"W Brand Studio embraced the difficult challenge of re-branding a 40-year-old "active adult" community with over 18,000 residents, and produced a product that is fresh, current, attractive and appropriate for the client. The new Laguna Woods Village logo, an elegant stylized green leaf, reflects the vitality and strength of the community. The W Brand Studio team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection, and the presentation was outstanding. Thank you, W Brand Studio!"</em>
                        Wendy Bucknum, CMCA, AMS, CACM.
                        <br><br>Government & Public Affairs Manager. Professional Community Management, Inc. - Agent for Laguna Woods Village<br><br><a href="testimonials">Click here to read more testimonials, or simply click below for a no-obligation quote.</a>
                        <br><br><a href="http://www.wbrandstudio.com/contact"><img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;"></a>


                    </p>
                </div>
            </div>
        </div>
    </section>





    <!-- Commented out part is same as the Top Content Row-->
    <!-- <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
		<div class="container-fluid-table display-t-sm width-100">
			<div class="row-table display-t-r-sm">
				<div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
					<img class="wid-100 hidden-xs" src="{{url('assets/images/work-sq.jpg')}}" alt="Results">
					<img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
				</div>				
				<div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text " >
					<article>
						<h2 class="orange-title ">How do you brand yourself?       </h2>
						<div class="row">
							<div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
								<div class="copy-block-gray text-center tk-futura-pt">
									<p class="text-left">The core essence of a brand starts from within. It's a vision from the top management that needs to flow all the way down to the person that sits at the front desk. Your visual identity, the interior and exterior of your building, and all communication, whether online or offline, need to reflect that vision. Allow W Brand Studio to unlock the true power, full potential, and exponential value of your company branding.</p>
								</div>
							</div>
						</div>						
					</article>
				</div>
			</div>
		</div>
</section> -->



@endsection