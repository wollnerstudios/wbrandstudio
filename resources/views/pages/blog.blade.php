@extends('layouts.default')
@section('brandingHeader')
@section('brandTitle', 'W PRO BLOG')
@include('partials.branding-header')
@endsection
@section('content')



        <div class="blog-feature-image" style="background: url('/images/blog_cover.jpg'); background-size: cover; background-position: bottom; background-repeat: no-repeat; height: 100%; min-height: 600px;">
            <div>


                <p>


                </p>
            </div>

        </div>


    <div class="album text-muted">
        <div class="container">
            <h1 class="text-center">Be unique. Be memorable. Branding 101.</h1>
            <div class="row">
                @foreach($posts as $post)
                <div class="col-md-4 my-25" style="overflow: hidden; height: 550px;">
                    @if($post->image)
                    <img src="/storage/{{$post->image}}" alt="Card image cap" style="height: 200px; width: 100%; object-fit: cover;">
                    @else
                    <img src="/assets/images/w-brand-studio-offices.jpg" alt="Card image cap" style="height: 200px; width: 100%; object-fit: cover;">
                    @endif
                    @if($post->title)
                        <h3 class="mb-3">{{$post->title}}</h3>
                        @endif
                    @if($post->excerpt)
                    <p style="margin-top: 20px; line-height: 1.2; font-size: 1.6rem">{{$post->excerpt}}</p>
                    @endif
                    <br>
                    <p>
                        <a href="/blog/blog-post/{{$post->slug}}" class="" style="padding: 10px; border: 1px solid black; color: black;">Read More</a>

                    </p>

                </div>
                @endforeach



            </div>
            <div>
                {{ $posts->links() }}
            </div>

        </div>
    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
    <script>
    $(function () {
        Holder.addTheme("thumb", { background: "#55595c", foreground: "#eceeef", text: "Thumbnail" });
    });
    </script>


@endsection