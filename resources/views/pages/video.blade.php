@extends('layouts.default')

@section('title')
    W Brand Studio | W Brand Studio | Branding, SEO and SEM @endsection

@section('description')
    Page Title @endsection

@section('keywords')
    branding, branding agency, marketing agency @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('brandingHeader')
@section('brandTitle', 'VIDEO')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-video">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            VIDEO EXAMPLES.
                        </h1>
                        <iframe src="https://player.vimeo.com/video/157215180" width="980" height="551" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        <p>Rancho Valencia</a> from our crew, The Dark Brothers. <br><br>
                            <iframe src="https://player.vimeo.com/video/146979694" width="980" height="551" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        <p>Ballast Point Brewing &amp; Spirits - Dedicated to the Craft </p>

                        <br><br>
                        <iframe src="https://player.vimeo.com/video/150035155" width="980" height="551" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        <p>GL Veneer | Company Video .</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 single-column-gray" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-12 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection