@extends('layouts.default')

@section('title')
    WollnerStudios | Creative Branding Agency | Brand Strategy | Brand Identity
@endsection

@section('description')
    WollnerStudios is a creative branding and design agency that helps businesses build their brand identity and grow their online presence...
@endsection

@section('keywords')
    branding agency, branding firm, marketing agency, marketing agency orange county, branding agency orange count
@endsection

@section('abstract')
    We are an established strategic branding agency and marketing firm, and one of the fastest growing, most winning branding agencies in Orange County.
@endsection

@section ('subject')
    We are a fullservice branding and marketing agency with proven success for our clients.
@endsection

@section('brandingHeader')
@section('brandTitle', ' ')
@include('partials.branding-header')
@endsection

@section('content')


    <script type="text/javascript"
            src="//www.googleadservices.com/pagead/conversion_async.js">
    </script>

    <div class="page page-home">
        <!-- ##MOBILE SECTION TOP BLOCK### -->
        <section class="visible-xs background-c-shade-3 text-center front-page-service-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        branding - SEO/PPC - logos
                    </div>
                    <div class="col-xs-12">
                        marketing - web design
                    </div>
                </div>
            </div>
        </section>
        <section class="visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 padding-l-0 padding-r-0">
                        <!-- <img class="width-100" src="{{url('storage/wb_images/wbrand-mobile-work-home.jpg')}}" alt=" "> -->
                        <img class="width-100" src="{{url('/images/websitedevelopment.jpg')}}" alt=" ">
                    </div>
                </div>
            </div>
        </section>
        <!-- ##END MOBILE SECTION TOP BLOCK### -->

        <!-- ##MAIN PAGE COPY BLOCK### -->
        <section class="page-copy-section tk-futura-pt wow fadeInUp opacity-0">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm padding-l-0 padding-r-0  vertical-a-m-sm">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
                                <h1>
                                    INNOVATIVE MARKETING AND BRANDING SOLUTIONS FOR TOMORROW
                                </h1>
                                
                              
                                <p>Branding is a critical aspect of business success, and it should be a top priority for any business looking to establish a strong identity, build recognition, trust, and loyalty, and increase its overall value. For almost three decades, we’ve helped companies discover what their brand truly is, what it has the potential to become, and succeed. Because changing perception changes results.
                                <br><br>
                                We are a full-service branding and marketing agency dedicated to doing the best work of our lives every day with the clients that inspire us. Contact us and work with an agency that thinks dramatically different.</p>

                                <a href="/about" class="mar-t-2 mar-b-1 myButton" style="font-size:18px!important;">ABOUT US</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 display-t-c-sm padding-l-0 padding-r-0 hidden-xs">
                        <div class="box-75"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ##END MAIN PAGE COPY BLOCK### -->

        <!-- ##MOBILE SECTION BOTTOM BLOCK### -->
        <section class="visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 padding-l-0 padding-r-0">
                        <!-- <img class="width-100" src="{{url('storage/wb_images/wbrand-lobby-awards.jpg')}}" alt=" "> -->
                        <img class="width-100" src="{{url('/images/wbrand-lobby.jpg')}}" alt=" ">
                    </div>
                </div>
            </div>
        </section>
        <section class="mobile-social-section">
            <div class="container-fluid-table visible-xs">
                <div class="row-table display-t-xs width-100">
                    <div class="col-xs-6 mobile-social-block mobile-social-block-fb display-t-c-xs">
                        <a href="https://www.facebook.com/wbrandstudio?fref=ts" target="_blank" class="social-facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                    <div class="col-xs-6 mobile-social-block background-c-shade-4 mobile-social-block-twitter display-t-c-xs">
                        <a href="https://twitter.com/wbrandstudio" target="_blank"
                           class="social-twitter color-white background-c-shade-4">
                            <i class="fa fa-twitter "></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- ##END SECTION BOTTOM BLOCK### -->

        <!-- ##MAIN PAGE GRID LEFT COPY### -->
        <section class="page-copy-section hidden-xs tk-futura-pt">
            <div class="container-fluid-table display-t-xs width-100">
                <div class="row-table display-t-r-xs">
                    <div class="col-sm-6 display-t-c-sm text-center vertical-a-b-sm ">
                        <div itemprop="review" itemscope itemtype="http://schema.org/Review" class="row">
                            <div class="col-sm-6 padding-l-0 padding-r-0 position-r hover-image ">
                                <img class="width-100 opacity-0 wow fadeInLeft"
                                     src="{{url('assets/images/aaf-silver.jpg')}}" alt="2017 Top Advertising Awards"
                                     data-wow-delay="600ms">
                                <div class="hover-effect-text">
                                    <a href="{{url('award-winning-agency')}}" class="take-a-look display-i-b-xs">
                                        RECOGNITION
                                    </a>

                                    <span itemprop="author" itemscope itemtype="http://schema.org/Person">
                                        <div class="description">
                                        Awards, Press Releases
                                        </div>
                                        <div class="client-name">
                                        
                                        </div>
                                    </span>
                                </div>
                                <div class="image-hover-effect"></div>
                            </div>
                            <div class="col-sm-6 padding-l-0 padding-r-0 position-r hover-image ">
                                <img class="width-100 opacity-0 wow fadeInRight"
                                     src="/assets/images/SEM-SEO-PPC.jpg"
                                     alt="restaurant design and marketing" data-wow-delay="600ms">
                                <div class="hover-effect-text">

                                    <a href="/capabilities/seo-sem" class="take-a-look display-i-b-xs">
                                        SEO - PPC
                                    </a>
                                    <div class="description">
                                        Search Engine Optimization, PPC Management, SEM
                                    </div>
                                    <div class="client-name">
                                        
                                    </div>
                                </div>
                                <div class="image-hover-effect"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 display-t-c-sm padding-l-0 padding-r-0">
                        <div class="box-70"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ##END MAIN PAGE GRID LEFT COPY### -->

        <!-- ##MAIN PAGE GRID LEFT IMAGE### -->
        <section class="hidden-xs">
            <div class="container-fluid-table display-t-xs width-100">
                <div class="row-table display-t-r-xs">
                    <div class="col-sm-6 display-t-c-sm padding-l-0 padding-r-0  position-r hover-image vertical-a-t-sm ">
                        <img src="{{url('assets/images/cutting-edge-website-development.jpg')}}" alt="industrial paint marketing"
                             class="wid-100 wow fadeInLeft opacity-0">
                        <div class="hover-effect-text">

                            <a href="/logo-design" class="take-a-look display-i-b-xs">
                                Take a look
                            </a>
                            <div class="description">
                                Logo, branding, web:
                            </div>
                            <div class="client-name">
                                Cutting Edge
                            </div>
                        </div>
                        <div class="image-hover-effect"></div>
                    </div>
                    <a href="https://www.facebook.com/WBrandStudio?fref=ts" target="_blank"
                       class="col-sm-3 display-t-c-sm padding-l-0 padding-r-0 vertical-a-b-sm social-facebook wow slideInUp"
                       data-wow-delay="600ms">

                        <i class="fa fa-facebook "></i>
                    </a>
                    <a href="https://twitter.com/wbrandstudio" target="_blank"
                       class="col-sm-3 display-t-c-sm padding-l-0 padding-r-0 vertical-a-b-sm social-twitter color-white background-c-shade-4 wow slideInUp"
                       data-wow-delay="800ms">
                        <div class="social-spacer"></div>
                        <i class="fa fa-twitter "></i>
                    </a>
                </div>
            </div>
        </section>
        <!-- ##END MAIN PAGE GRID LEFT IMAGE### -->

        <!-- ##MAIN PAGE GRID RIGHT IMAGES### -->
        <section class="position-a rig-0 top-0 width-50 hidden-xs ">
            <div class="wid-100 pull-left position-r hover-image">
                <img src="/assets/images/branding-eliant-interior.jpg" alt=" " class="opacity-0 wid-100 wow fadeInDown ">
                <div class="hover-effect-text">

                    <a href="/capabilities/visual-identity" class="take-a-look display-i-b-xs">
                        VISUAL IDENTITY
                    </a>
                    <div class="description">
                        Corporate Identity, Stationary, Logo Design
                    </div>
                    <div class="client-name">
                         
                    </div>
                </div>
                <div class="image-hover-effect"></div>
            </div>
            <div class="wid-50 pull-left position-r hover-image">
                <img src="/assets/images/HBT-labs-total-branding-exterior.jpg" alt=" "
                     class="opacity-0 wid-100 wow fadeInUp" data-wow-delay="200ms">
                
             
                <div class="hover-effect-text">
                    
                    <a href="/capabilities/branding" class="take-a-look">
                        BRANDING
                    </a>
                    <div class="description">
                        Strategy, Positioning, Messaging, Tone-of-Voice
                    </div>
                    <div class="client-name">
                        
                    </div>
                </div>
                
                
                <div class="image-hover-effect"></div>
            </div>
            <div class="wid-50 pull-left position-r hover-image">
                <img src="/assets/images/website-development-the-Edmon.jpg" alt=" "
                     class="opacity-0 wow fadeInLeft wid-100" data-wow-delay="400ms">
                <div class="hover-effect-text">

                    <a href="{{url('/capabilities/website-design')}}" class="take-a-look">
                        WEBSITE DESIGN
                    </a>
                    <div class="description">
                        Custom corporate & ecommerce sites 
                    </div>
                    <div class="client-name">
                        
                    </div>
                </div>
                <div class="image-hover-effect"></div>
            </div>
            <div class="wid-50 pull-left position-r hover-image">
                <img src="{{url('/images/skidata-interior-design.jpg')}}" alt=" " class="opacity-0 wid-100 wow fadeInRight "
                     data-wow-delay="600ms">
                <div class="hover-effect-text">

                    <a href="{{url('interior-design')}}" class="take-a-look">
                        INTERIOR DESIGN
                    </a>
                    <div class="description">
                        Branded interior & exterior design 
                    </div>
                    <div class="client-name">
                        
                    </div>
                </div>
                <div class="image-hover-effect"></div>
            </div>
            <div class="wid-100 pull-left position-r hover-image" style="top:-2px;">
                <img src="{{url('assets/images/newhome/prr.jpg')}}" alt=" " class="opacity-0 wid-100 wow fadeInLeft"
                     data-wow-delay="400ms">
                <div class="hover-effect-text">

                    <a href="/logo-design" class="take-a-look">
                        LOGO DESIGN
                    </a>
                    <div class="description">
                         Logo Design, Graphic Design, Visual Identity
                    </div>
                    <div class="client-name">
                        PRR
                    </div>
                </div>
                <div class="image-hover-effect"></div>
            </div>
            
        </section>
        <!-- ##END MAIN PAGE GRID RIGHT IMAGES### -->
    </div>
@endsection