@extends('layouts.default')

@section('title')
     W Brand Studio | Contact Us Today | (657) 232-0110
@endsection

@section('description')
    An award branding and marketing agency. Give us 15 minutes and we'll let you know how we can help using innovative, proven marketing strategies.
@endsection

@section('keywords')
    branding agency, marketing agency, orange county marketing
@endsection

@section('abstract')
    An award branding and marketing agency. Give us 15 minutes and we'll let you know how we can help using innovative, proven marketing strategies
@endsection




@section('brandingHeader')
@section('brandTitle', 'Contact Us')
@include('partials.branding-header')
@endsection



@section('content')
    <!--<script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                document.getElementById('recaptcha').value = token;
            }
        });
    });
    </script>-->
    {{--<!-- Google Code for Sign Up Conversion Page -->--}}
    {{--<script type="text/javascript">--}}
    {{--/* <![CDATA[ */--}}
    {{--var google_conversion_id = 944395903;--}}
    {{--var google_conversion_label = "hIE5CIb48HYQ_6ypwgM";--}}
    {{--var google_remarketing_only = false;--}}
    {{--/* ]]> */--}}
    {{--</script>--}}
    {{--<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">--}}
    {{--</script>--}}
    {{--<noscript>--}}
        {{--<div style="display:inline;">--}}
            {{--<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/944395903/?label=hIE5CIb48HYQ_6ypwgM&amp;guid=ON&amp;script=0"/>--}}
        {{--</div>--}}
    {{--</noscript>--}}
    <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt position-r">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="contact-block waves-effect wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="200ms">
                        <a href="mailto:hello@wbrandstudio.com?Subject=hello">
                            <i class="fa fa-envelope"></i>
                            <h2>Write To Us</h2>
                            <p>If you are a vendor, drop us a line and send us your information.</p>

                            <p class="padding-t-1">
                                hello@wbrandstudio.com
                            </p>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-block waves-effect wow fadeInUp opacity-0" id="contact-contact-block" data-wow-offset="100" data-wow-delay="400ms">
                        <a href="javascript:void(0)" class="start-a-project-button">
                            <img src="/assets/images/sign-up-icon.png" style="width: 72px;">
                            <h2>Get A Free Quote</h2>
                            <p>It only takes 15 minutes to learn how we can help. Call us at 657-232-0110 or
                                <b>click here</b> to inquire about our services and pricing.</p>
                            <p class="padding-t-1">Branding - Web Design - Marketing - SEO - More</p>
                            <p><br>We’re located at 151 Kalmus Dr., Suite E-100, Costa Mesa, CA 92626 </p>
                        </a> </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-block waves-effect wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="600ms">
                        <a href="mailto:hello@wbrandstudio.com?Subject=hello">
                            <i class="fa fa-cogs"></i>
                            <h2>Careers</h2>
                            <p>If you’re as passionate about your work as we are, let’s talk! No positions are open right now, but do send us your resume because hey, you never know… </p>

                            <p class="padding-t-1">hello@wbrandstudio.com</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container contact-form">
            <div class="contact-form-close-button">

                <div>x</div>
            </div>
         <!--   <div class="row">
                <div class="col-xs-12">
                    <section id="slick">
                        <div id="crmWebToEntityForm" class="feedback-form">
                            <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
                            <form action='' id="contactform" name=WebToLeads3854640000000246003 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory3854640000000246003()' accept-charset='UTF-8'>

                                <input type='text' style='display:none;' name='xnQsjsdp' value='dff6044e2fe0006a781ac82ec51d0cb2a1d5dff2b90cd9e4a61dc1982eaae57e'/>
                                <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
                                <input type='text' style='display:none;' name='xmIwtLD' value='33b1e1dc3194d300e4d5dd159a8733b2249cab57b8e656ae13879ac6a1e03df4'/>
                                <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>

                                <input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;wbrandstudio.com&#x2f;contact' />


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title">First name</div>
                                                <div class="field">
                                                    <input type="text" name="First Name" required/>
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your first name</span>
                                                </div>
                                                <div class="form-title">Last name</div>
                                                <div class="field">
                                                    <input type="text" name="Last Name" required/>
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your last name</span>
                                                </div>
                                                <div class="form-title">company</div>
                                                <div class="field">
                                                    <input type="text" name="Company" id="company" required/>
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your Company</span>
                                                </div>
                                                <div class="form-title">email</div>
                                                <div class="field">
                                                    <input type="email" name="Email" id="email" required/>
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your Email</span>
                                                </div>
                                                <div class="form-title">phone</div>
                                                <div class="field">
                                                    <input type="text" name="Phone" id="phone" required/>
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                </div>
                                                <div class="field hidden">
                                                    <input type="text" name="Address" id="address" />
                                                    <!-- <span class="entypo-user icon"></span>
                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title">tell us what you need</div>
                                                <div class="field">
                                                    <textarea name="Description" placeholder="" id="Description" maxlength='32000' class="message" required></textarea>
                                                    <!-- <span class="entypo-comment icon"></span>
                                                    <span class="slick-tip left">Your message</span>
                                                </div>
                                                <input id="submitbutton" type="submit" value="Submit" class="send" disabled/>
                                                <input  type='reset' style='font-size:12px;color:black' value='Reset' />
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <script>
                                var mndFileds=new Array('Company','First Name','Last Name','Email','Phone','Description');
                                var fldLangVal=new Array('Company','First Name','Last Name','Email','Phone','Tell Us What You Need');
                                var name='';
                                var email='';

                                function checkMandatory3854640000000246003() {
                                    for(i=0;i<mndFileds.length;i++) {
                                        var fieldObj=document.forms['WebToLeads3854640000000246003'][mndFileds[i]];
                                        if(fieldObj) {
                                            if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
                                                if(fieldObj.type =='file')
                                                {
                                                    alert('Please select a file to upload.');
                                                    fieldObj.focus();
                                                    return false;
                                                }
                                                alert(fldLangVal[i] +' cannot be empty.');
                                                fieldObj.focus();
                                                return false;
                                            }  else if(fieldObj.nodeName=='SELECT') {
                                                if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
                                                    alert(fldLangVal[i] +' cannot be none.');
                                                    fieldObj.focus();
                                                    return false;
                                                }
                                            } else if(fieldObj.type =='checkbox'){
                                                if(fieldObj.checked == false){
                                                    alert('Please accept  '+fldLangVal[i]);
                                                    fieldObj.focus();
                                                    return false;
                                                }
                                            }
                                            try {
                                                if(fieldObj.name == 'Last Name') {
                                                    name = fieldObj.value;
                                                }
                                            } catch (e) {}
                                        }
                                    }
                                }

                                </script>
                                {{--RECAPTCHA --}}
                                <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
                                <script src="https://www.google.com/recaptcha/api.js?render=6Lcil58UAAAAAACsEt61oZXpDb5nHxFhO4zx1yAL"></script>
                                {{--HCAPTCHA--}}
                                {{--<script src="https://js.hcaptcha.com/1/api.js" async defer></script>--}}
                                <script>
                                grecaptcha.ready(function() {
                                    grecaptcha.execute('6Lcil58UAAAAAACsEt61oZXpDb5nHxFhO4zx1yAL', {action: 'homepage'}).then(function(token) {
                                    var honey = document.getElementById("address").value;
                                    if(honey === '' || honey === null){
                                        document.getElementById('contactform').action = "https://crm.zoho.com/crm/WebToLeadForm";
                                    }

                                        document.getElementById("submitbutton").disabled = false;
                                        var config = {
                                            headers: {'Access-Control-Allow-Origin': '*'}
                                        };
                                        axios.post('https://www.google.com/recaptcha/api/siteverify', {
                                            secret: '6Lcil58UAAAAAI_S3OMspzoqN1Y5RIDyf3V6KGna',
                                                response: token,

                                        }, config).then(function (response) {
                                                console.log(response);
                                                alert(response);
                                            })
                                            .catch(function (error) {
                                                console.log(error);
                                            });


                                    });
                                });
                                </script>

                            </form>
                        </div>
                    </section>
                </div>
            </div><!--row-->
            <div class="row">
                <div class="col-xs-12">
                    <section id="slick">
                        <div class="feedback-form">
                            <form action="/contact" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-title" style="font-size: 20px;">name</div>
                                                <div class="field">
                                                    <input type="text" name="name" id="name" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your name</span>
                                                </div>
                                                <div class="form-title" style="font-size: 20px;">company</div>
                                                <div class="field">
                                                    <input type="text" name="company" id="company" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Company</span>
                                                </div>
                                                <div class="form-title" style="font-size: 20px;">email</div>
                                                <div class="field">
                                                    <input type="email" name="email" id="email" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Email</span>
                                                </div>
                                                <div class="form-title" style="font-size: 20px;">phone</div>
                                                <div class="field">
                                                    <input type="text" name="phone" id="phone" required/>
                                                    <!-- <span class="entypo-user icon"></span> -->
                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                <div class="form-group custom-section">
                                                    <label>I am looking for:</label>
                                                    <br>
                                                    <div class="row my-4" style="margin-top: 20px; margin-bottom: 20px;">
                                                        <div class="col-md-6" style="display: flex; flex-direction: column; align-items: flex-start;">
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input type="checkbox" value="branding"><span style="margin-left: 20px !important; font-size: 20px !important;">Branding</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service1" type="checkbox" value="visual ID"><span style="margin-left: 20px !important; font-size: 20px !important;">Visual Identity</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service2" type="checkbox" value="logo design"><span style="margin-left: 20px !important; font-size: 20px !important;">Logo Design</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service3" type="checkbox" value="web dev"><span style="margin-left: 20px !important; font-size: 20px !important;">Website Development</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service4" type="checkbox" value="social media management"><span style="margin-left: 20px !important; font-size: 20px !important;">Social Media Management</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service9" type="checkbox" value="int/ext design"><span style="margin-left: 20px !important; font-size: 20px !important;">Interior/Exterior Design</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service10" type="checkbox" value="print"><span style="margin-left: 20px !important; font-size: 20px !important;">Print Design (i.e. brochure)</span></label>
                                                        </div>
                                                        <div class="col-md-6" style="display: flex; flex-direction: column; align-items: flex-start;">

                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service5" type="checkbox" value="seo"><span style="margin-left: 20px !important; font-size: 20px !important;">SEO</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service6" type="checkbox" value="ppc"><span style="margin-left: 20px !important; font-size: 20px !important;">PPC</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service7" type="checkbox" value="video prod"><span style="margin-left: 20px !important; font-size: 20px !important;">Video/Commercial&nbsp;Production</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;" ><input name="service8" type="checkbox" value="photography"><span style="margin-left: 20px !important; font-size: 20px !important;">Photography</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service11" type="checkbox" value="Naming"><span style="margin-left: 20px !important; font-size: 20px !important;">Naming</span></label>
                                                            <label class="checkbox-inline w-20p" style="display: flex; align-items: center;"><input name="service12" type="checkbox" value="marketing"><span style="margin-left: 20px !important; font-size: 20px !important;">Marketing</span></label>
                                                        </div>



                                                    </div>

                                                </div>
                                                <div class="form-title" style="font-size: 20px;">tell us what you need</div>
                                                <div class="field">
                                                    <textarea name="message" placeholder="" id="message" class="message" required></textarea>
                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                    <span class="slick-tip left">Your message</span>
                                                </div>
                                                <!--<input type="hidden" name="recaptcha" id="recaptcha">-->

                                                <!-- HCAPTCHA
                                                <div class="h-captcha" data-sitekey="HCAPTCHA_SITE_KEY"></div> -->
                                                <input type="submit" value="Submit" class="send"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div><!--row-->
        </div><!--container-->
    </section>

    <section class="wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="600ms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 padding-l-0 padding-r-0">
                    <img class="width-100" src="{{url('/assets/images/w-brand-studio-offices-costa-mesa.jpeg')}}" alt="">
                </div>
            </div>
        </div>
    </section>


@endsection