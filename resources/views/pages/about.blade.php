@extends('layouts.default')

@section('title')
Award Winning Branding & Digital Marketing Agency | Orange County, CA
@endsection

@section('description')
    We are an award-winning branding and marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('keywords')
branding agency, marketing agency, branding orange county, marketing orange county
@endsection

@section('abstract')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section ('subject')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('brandingHeader')
@section('brandTitle', 'W BRAND STUDIO')
@include('partials.branding-header')
@endsection

@section('content')
<div class="page page-about">

	<img class="wid-100 hidden-xs" src="/images/about-w-brand-agency.jpg" alt="branding and marketing">

	<!--<div style="background-color: #BDA078;color:#ffffff;text-transform: uppercase;padding:10px;margin-bottom:60px;" class="text-center">
		<a class="white-link" href="#who">WHO WE ARE</a> |
		<a class="white-link" href="#promise">OUR PROMISE</a> |
		<a class="white-link" href="#recognition">RECOGNITION</a> | 
		<a class="white-link" href="#difference">THE DIFFERENCE</a> | 
		<a class="white-link" href="#thinking">THINKING DIFFERENTLY</a>  
	</div>

	<div id="who" class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0 container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
	    <h1 class="page-title text-bronze" style="margin-bottom:45px;  font-size: 4.8rem">
	        Getting to know you is our pleasure. And our business. 
	    </h1>
	    <div class="sm-underline">&nbsp;</div>
	    <p style="margin-bottom:15px;">We already love your brand. That’s because W Brand Studio is built on 25 years of passion for brands: For helping companies discover what their brand truly is, what it has the potential to become, and seeing it succeed. </p>
	    <p style="margin-bottom:15px;">We’re a full-service branding and marketing agency serving small to Fortune 500 companies. Our clients range from fast-casual restaurant chains and Medical companies, to aerospace corporations and financial institutions; but to tell your company’s story in a way that excites customers and grows business, you don’t need people who know your industry. You need people who know branding.  </p>
	    <p style="margin-bottom:15px;">Our superpower is getting at core of your brand and what makes it unlike any other – the magic that makes customers engage and business grow. And we bring your brand to life under one inspired, creative, highly caffeinated roof with services from website development, SEO and PPC to graphic and interior design, to video and photography and social media.</p>
	    <p style="margin-bottom:30px;">Co-founded in 1997 by award-winning DDB and Ogilvy creative Michael Wollner, W Brand Studio’s work for clients including Marriott Hotels and Resorts, Boeing, and Blue Origin, has been recognized worldwide with multiple industry honors including the AVA Digital Award, the Dot.Comm Award, the American Advertising Award, the Hermes Creative Award, and the prestigious Cannes Lions. Our success – and that of our clients – is based on a few simple beliefs:</p>
		<p style="margin-bottom:30px; color:#BDA078!important; font-weight:bold;">CHANGING PERCEPTION CHANGES RESULTS</p>
        <p style="margin-bottom:30px; color:#BDA078!important; font-weight:bold;">BIG IDEAS ARENT JUST FOR BIG COMPANIES</p>
        <p style="margin-bottom:30px; color:#BDA078!important; font-weight:bold;">DRIVING AWARENESS DRIVES BUSINESS</p>
        <p style="margin-bottom:30px; color:#BDA078!important; font-weight:bold;">OUTSMARTING BEATS OUTSPENDING EVERY TIME</p>
        <p style="margin-bottom:30px; font-weight:bold;">We love your brand. We’d love to meet you. hello@wbrandstudio.com  657.232.0110</p>
		<div style="margin:35px;">
	    	<a href="/contact" class="myButton org-btn">CONTACT US</a>
	    </div>
	</div>-->

	<section class="page-copy-section text-center pad-t-2 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
						Award-Winning Branding Company WollnerStudios: Helping Businesses Stand Out Since 1997.
					</h1>
					<div class="sm-underline">&nbsp;</div>

					<p>
						With over 40 years of branding experience, the company is led by Michael Wollner, a multi-award-winning branding expert. WollnerStudios has a proven track record for coming up with original and creative ideas that help businesses achieve success, regardless of their size or industry.
					</p>
					<br>
					<p>
						At WollnerStudios, we believe that changing perception changes results. Our team of branding experts works tirelessly to create unique and memorable brand identities that set our clients apart from their competitors. Our approach is focused on creating a brand that resonates with customers and inspires them to take action.
					</p>
					<p>
						Whether it's working with Fortune 500 companies or entrepreneurs, WollnerStudios has the expertise and experience to help businesses succeed. Our team takes the time to understand our clients' goals and objectives, and we work closely with them to develop a strategy that aligns with their vision.
					</p>
					<br>
					<p>
						As a boutique agency, WollnerStudios prides itself on providing personalized service to our clients. We understand that big ideas aren't just for big companies, and we are passionate about helping businesses of all sizes achieve their full potential. Our focus on originality and creativity sets us apart from other branding agencies, and our clients appreciate the results we deliver.
					</p>
					<br>
					<p>
						Overall, WollnerStudios is a branding company that is committed to helping businesses achieve success through original and creative branding strategies. With a proven track record of success and a team of branding experts with over 40 years of experience, we are one of the best boutique agencies around.
					</p>

					<!--<br>

                    <p><a href="/contact">Like? Call us and we'll talk about your strategy today.</a></p>-->
				</div>
			</div>
		</div>
	</section>

	<section class="pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" style="background:#BFA179;">
		<div class="container text-white">
			<div class="row page-copy-section text-white">
				<div class="col-sm-12 text-center ">
					OUR MAIN SERVICES
				</div>
			</div>
			<div class="row pt-5" style="margin: 0 auto; max-width:600px; font-size: 18px;">
				<div class="col-sm-12 col-md-6 pt-4">
					<ul>
						<li class="mb-4">
							Brand Strategy
						</li>
						<li class="mb-4">
							Brand Identity
						</li>
						<li class="mb-4">
							Logo Design
						</li>
						<li class="mb-4">
							Website Design and Development
						</li>
						<li class="mb-4">
							Content Creation
						</li>
						<li class="mb-4">
							Social Media Marketing
						</li>
						<li class="mb-4">
							Email Marketing
						</li>
						<li class="mb-4">
							Direct Mail
						</li>
					</ul>
				</div>
				<div class="col-sm-12 col-md-6 pt-4">
					<ul>
						<li class="mb-4">
							Search Engine Optimization (SEO)
						</li>
						<li class="mb-4">
							Pay-Per-Click Advertising (PPC
						</li>
						<li class="mb-4">
							Interior & Exterior Design
						</li>
						<li class="mb-4">
							Booth Design / Expo Materials
						</li>
						<li class="mb-4">
							Brand Photography
						</li>
						<li class="mb-4">
							Video Production
						</li>
						<li class="mb-4">
							Marketing Strategy
						</li>
						<li class="mb-4">
							Advertising
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="page-copy-section text-center pad-t-2 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

					<p>
						At our full-service branding and marketing agency, we offer a comprehensive suite of services to help you elevate your brand both inside and out. From logo design and brand strategy to interior design and booth design, we take care of every aspect of your brand to create a cohesive and memorable experience for your customers. Our team of branding and marketing experts, interior designers, and graphic designers work closely with you to understand your business goals and develop a strategy that aligns with your vision. We believe that a strong brand is key to success, and we are committed to helping you achieve your full potential.
					</p>
					<div class="page-copy-section text-center pad-t-1 hidden-xs tk-futura-pt wow fadeInUp container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
						<img class="wid-100 " src="/assets/images/clients-logos.jpg" alt="branding and marketing">
					</div>
					<!--<br>

                    <p><a href="/contact">Like? Call us and we'll talk about your strategy today.</a></p>-->
				</div>
			</div>
		</div>
	</section>

        <section id="promise" class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn"
                         data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="/assets/images/wbrand-creative-brainstorm.jpg" alt="branding and marketing">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="bronze-title right">OUR PROMISE TO YOU</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">
                                            We’ll deliver beyond-the-box brand solutions that drive visibility, impact, and business: new ways of thinking that yield endless possibilities. We’ll outsmart versus outspend because we respect your bottom line. We’ll brainstorm without boundaries, awe you with options, and exceed expectations at every turn. Our passion for branding and commitment to the highest standards of excellence have earned W Brand Studio more than 100 major awards for branding, design, video production and marketing.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

	<!--<div id="promise" class="gray-bg-section wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms" >
		<div class="page-copy-section tk-futura-pt row" style="margin-bottom:60px;">
			<div class="col-sm-12 col-md-6" style="padding:0px!important; margin:0px!important;">
				<img class="wid-100 hidden-xs" src="/assets/images/wbrand-creative-brainstorm.jpg" alt="branding and marketing">
			</div>
			<div class="col-sm-12 col-md-6" style="padding:0px!important; margin:0px!important;">
				<div style="position: absolute;top:0;left:0;width:280px;" class="bg-bronze text-white text-center sm-text p-md">
					 OUR PROMISE TO YOU  
				</div>
				<div style="padding:30% 15%;">
					<p>
						We believe in exploring what's outside the box for the simple solutions that drive relevance, originality, and impact. When you partner with us, we guarantee that you'll be immersed with endless possibilities and discover a whole new way of thinking. They've come to realize that we outsmart instead of outspend and keep returning because we understand that their bottom line is our top priority. It's time to work with an agency that will immerse you with possibilities, brainstorm without restriction, work tirelessly beyond all expectation and make you more profitable.
					</p>
				</div>
			</div>
		</div>
	</div>-->

	<div id="recognition" class="page-copy-section text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp opacity-0 container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
	    <h2 class="page-title text-bronze" style="font-size: 3.6rem">
	        RECOGNITION 
	    </h2>
	    <div class="sm-underline">&nbsp;</div>
	    <p style="margin-bottom:15px;">We’re an independent shop of creative genius-hustlers making big-agency impact. W Brand Studio has taken home the AVA Digital Award, the dotCOMM Award, the American Advertising Award, the Hermes Creative Award, and the prestigious Cannes Lions. </p>
	    <p style="margin-bottom:15px;">We’re proud of these recognitions, because they’re the result of work that’s on par with that of large multi-million-dollar agencies. And the credit goes largely to the clients who’ve challenged and inspired us for the last 25 years. – Michael Wollner, W Brand Studio President/Creative Director </p>
	    <p style="margin-bottom:15px;">Based in Southern California, W Brand Studio’s brand work portfolio includes Honeybee Robotics/Blue Origin, Boeing, California Business Bank, Marriott Hotels & Resorts, Yamaha Pianos, Heidi’s Brooklyn Deli, Knott’s Berry Farm, Coastal Heart Medical Group, Pure Elements Water, Sentry Control/SKIDATA, Pacific Resource Recovery and Shigeru Kawai. </p>

	    
	</div>





	<div class="page-copy-section hidden-xs text-center pad-t-1 pad-b-1 tk-futura-pt wow fadeInUp container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
		<img class="wid-100" src="/assets/images/logos.png" alt="branding and marketing">
	</div>





</div>
@endsection