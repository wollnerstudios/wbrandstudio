@extends('layouts.default')

@section('title')
    Marketing And Branding Madmen @endsection

@section('description')
    We are an innovative and creative digital marketing agency in Orange County. @endsection

@section('keywords')
    Marketing agency Orange County, branding firm, branding firms, Strategic marketing Firm, Web Designers Orange County, Orange County Marketing Consulting, Search Engine Marketing Orange County, SEO Orange County, Orange County Web Site Design Experts, Orange County Web Design, Marketing, branding firm, agency,Irvine Marketing firm, OC website design, Orange County Marketing Firm California, social media marketing, Web design OC, irvine website development, los angeles marketing, seo, graphic design. @endsection

@section('abstract')
    Marketing agency, Marketing Firm, Marketing Company, web design, web design, Orange County website design, Orange County Branding, Orange County Website development, Los Angeles Marketing Company @endsection

@section('brandingHeader')
@section('brandTitle', 'DIGITAL MARKETING AGENCY')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-digital-marketing-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Marketing And Branding Madmen
                        </h1>
                        <p>
                            Big when we need to be, and small when we want to. The old ways of your typical advertising or marketing company is a thing of the past. We live in a world of constantly emerging technologies and new ways to search for information. Being able to differentiate yourself from the clutter and engage users, has become crucial to any marketing strategy. We know that branding is everything. It is the one thing that will not only differentiate you from your competition, it will also push forward your marketing efforts much more efficiently than any other approach. We are an award-winning, full-service agency and offer everything under one roof.
                        </p>
                        <br>
                        <p>
                            Come and work with one of the fastest growing branding and marketing companies in Orange County. We are well experienced in numerous industries from hospitality, medical and aerospace, to finance and retail. Clients like
                            <span style="color: #d06b29">Yamaha Corporation, Marriott, California Business Bank, Shigeru Kawai Pianos, Multi Medical Systems, Sentry Control (SKIDATA) and Shoring Engineers</span> have all trusted us with their marketing needs. So why shouldn't you? Call us at (657) 232-0110 or email us for more information.
                        </p>

                        <p>
                            <a href="http://www.wbrandstudio.com/contact" class="myButton org-btn">Let's talk</a>
                            <br><br>
                            <img class="wid-100 hidden-xs" src="{{url('/assets/images/marketing-header4.jpg')}}" alt="Branding for Menu">
                            <br><br>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text"></div>
                    </div>
                </div>
            </section>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/about-agency.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">MARKETING SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-left tk-futura-pt">
                                        <h2><strong>We think differently.</strong></h2>
                                        <p>
                                            We're a fast growing marketing and branding agency with insight, experience, and energy. If you want to be just like everyone else, we're NOT the agency for you. However, if you want to stand out in the crowd, call us. With a global background from successful ad agencies, we bring something new to the marketing mix. Let's help you with:
                                        </p>

                                        <ul>
                                            <li>Marketing Strategy</li>
                                            <li>Search Engine Marketing (SEO/ADWORDS)</li>
                                            <li>Social Media Marketing</li>
                                            <li>Advertising</li>
                                            <li>Web Development</li>
                                            <li>Logo Development</li>
                                            <li>Design Services and more...</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Everyone has a story.
                        </h2>
                        <p>We are storytellers. It's the stories that we share that make us unique and interesting individuals. Powerful experiences can effect perception, and more importantly change the way you do into others. Whether it's traditional advertising or digital marketing, We have decades of proven expertise both online and off-line. We create powerful messaging with good storytelling, engaging consumers and connect with them on an emotional level. From Fortune 500 companies to first time start-ups, our unique knowledge and flawless execution is second to none.
                            <br><br>
                            We not only care about brands, we care about people. We care about doing the right thing. We care about our community, being loyal to our clients, and being passionate about our work.
                            <br><br>
                        <h3><u><a href="contact">Let's set-up a meeting and find your story. </a></u></h3></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/digital-marketing-advertising.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">The heart of your business success lies in its marketing. </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">Most aspects of your business depend on successful marketing. The overall marketing umbrella covers advertising, public relations, promotions and sales. A marketing program that gives your company the best chance is a healthy mix of different forms of marketing, such as website development, public relations, print and broadcast advertising, design and printing for all print materials, trade shows and other special events. Call us and let's unleash your potential today.

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
