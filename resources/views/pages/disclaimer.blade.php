@extends('layouts.default')

@section('title')
W Brand Studio Terms of Use | Privacy Policy | Disclaimer @endsection

@section('description')
This page contains the W Brand Studio terms of use, privacy policy and disclaimer that applies to all website visitors. @endsection

@section('keywords')
branding, marketing @endsection
@section('abstract')
branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'Disclaimer')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-disclaimer">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Terms of Use, Privacy Policy and Disclaimer.
                        </h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 single-column-gray" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-12 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title normal">Disclaimer</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p>
                                            Consent: <br>
                                            PLEASE READ THESE TERMS OF USE ("TERMS OF USE") CAREFULLY BEFORE USING THIS WEB SITE (THIS "SITE"). BY ACCESSING THIS SITE, YOU AGREE TO BE BOUND BY THESE TERMS OF USE. IF YOU DO NOT AGREE TO THESE TERMS OF USE, DO NOT ACCESS THIS SITE. WBRANDSTUDIO.COM and W BRAND STUDIO, INC. RESERVES THE RIGHT TO CHANGE THESE TERMS OF USE AT ANY TIME, AT ITS OWN DISCRETION AND WITHOUT NOTICE TO YOU; HOWEVER, ALL SUCH CHANGES WILL BE POSTED TO THIS SITE. WBRANDSTUDIO.COM ENCOURAGES YOU TO REVIEW THE TERMS OF USE FROM TIME TO TIME SO THAT YOU ARE AWARE OF ANY SUCH CHANGES. THE TERMS OF USE THAT ARE APPLICABLE TO YOU AT ANY GIVEN TIME ARE THE THEN-CURRENT TERMS OF USE
                                        </p>
                                        <p>
                                            WBRANDSTUDIO.COM and W BRAND STUDIO, INC. RETAINS THE RIGHT TO DENY ACCESS TO THIS SITE TO ANYONE AT ITS COMPLETE DISCRETION FOR ANY REASON, INCLUDING FOR VIOLATION OF ANY OF THESE TERMS OF USE. THIS SITE PROVIDES LINKS TO VARIOUS OTHER WEB SITES, WHETHER AFFILIATED WITH WBRANDSTUDIO.COM and W BRAND STUDIO, INC. OR OTHERWISE. THESE OTHER WEB SITES MAY HAVE THEIR OWN TERMS OF USE POLICIES WHICH ARE DIFFERENT FROM, OR IN ADDITION TO, THE TERMS OF USE SPECIFIED HEREIN. BY ACCESSING THESE WEB SITES THROUGH LINKS PROVIDED ON THIS SITE, YOU AGREE TO ABIDE BY EACH APPLICABLE TERMS OF USE POLICY AS THEREIN SPECIFIED.
                                        </p>
                                        <p>
                                            Copyright, Trademarks & Intellectual Property Rights: <br>
                                            Copyright © 2014 W BRAND STUDIO, Inc. All rights reserved. All text, formatting (including without limitation the selection, coordination and arrangement of materials on the Site, and the "look and feel" of the Site), images, graphics, animation, videos, music, sounds, articles, copy, creative, trademarks, logos, and subsidiary, parent, or affiliate company names and other materials on this site are subject to the intellectual property rights of W BRAND STUDIO, Inc., its subsidiaries and affiliates, it's partners, it's clients and their respective licensors and licensees. These materials may not be copied for commercial use, reverse engineered, decompiled, or disassembled, nor may these materials be modified, re-posted to other web sites, framed, deep linked to, or otherwise redistributed in any form. Nothing in this site shall be construed as granting by implication, estoppel or otherwise, any license or right to make commercial use of WBRANDSTUDIO.COM or any intellectual property right or related materials without W BRAND STUDIO, INC's prior written permission. All third-party product and services referenced on the site are trademarks or service marks of their respective owners and cannot be used without the respective party's prior written permission. All intellectual property rights in all third party content used on the site shall belong to its respective owner. Any unauthorized use of WBRANDSTUDIO.COM and third party materials will violate such party's respective intellectual property rights and will subject to the appropriate legal rights and remedies.
                                        </p>
                                        <p>
                                            Limited Right to Use Site Content: <br>
                                            The viewing, printing or downloading of any content from the Site grants you only a limited, nonexclusive, revocable license for use solely by you for your own personal use and not for republication, distribution, assignment, sublicense, transfer, sale, preparation of derivative works or other non-personal use. No part of any content, graphic or document may be reproduced in any form or incorporated into any information retrieval system, electronic or mechanical, other than for your personal use.
                                        </p>
                                        <p>
                                            You may not create a Web page or site or computer application of any kind that deep links to or frames this Site, any page of this Site, or any graphics, trademark or other proprietary information of any kind located on this Site without W BRAND STUDIO, INC.'s express written permission. You may not use met a tags or any other type of hidden text utilizing W BRAND STUDIO, INC.'s or any of its affiliates' names, trademarks or intellectual property rights on a web site without W BRAND STUDIO, INC's express written permission. Except for the limited rights expressly granted herein, all right, title and interest in and to the Site and all materials contained therein are reserved by W BRAND STUDIO, INC. Your right to access this Site may be terminated at any time by W BRAND STUDIO, INC. without notice.
                                        </p>

                                        <p>
                                            Disclaimer <br>
                                            It is the policy of W BRAND STUDIO, Inc. that personal information such as your name, company, postal and email addresses, telephone numbers, and company information is private and confidential. Because your privacy is important to us, W BRAND STUDIO, Inc. maintains a website privacy policy to protect your personal information. By using this site, you consent to the terms of our privacy policy for the collection, use and disclosure of your personal information for the purposes set out below. We do not collect, use or disclose your personal information for any purpose other than those identified below, except with your consent as required by law.

                                        </p>
                                        <p>
                                            Site Activity <br>
                                            Each time a visitor comes to this website our web server collects and logs certain information. These access logs are kept for a reasonable period of time. These logs include, but are not restricted to your machine's TCP/IP address, your user name (if applicable), date, time, and files accessed. These logs are used solely for performance, site administration, and security reviews. They are not sold or shared in any way to third party organizations.

                                        </p>
                                        <p>
                                            Cookies <br>
                                            Portions of the W BRAND STUDIO, Inc. website may use "cookies" for various purposes, but mostly to enhance your visit to our website. Cookies can tell if you have visited our site before and help identify what aspects of the site you may be interested in. Cookies do not, however, obtain personal information about you or extract information from your computer.

                                        </p>
                                        <p>
                                            Personal Information <br>
                                            Personal information, such as your name, company, postal and email addresses, telephone numbers, and company information is collected only when you voluntarily provide it. Such information is received when you send an email to us or make some other specific contact, such as completing an online form. The personal information you provide to W BRAND STUDIO, Inc. is forwarded only to the person or department equipped to handle your request and is used only to respond to your inquiry. Personal information may be shared with W BRAND STUDIO, Inc.'s third party business partners, suppliers, vendors, distributors, manufacturers, and other agents only if that disclosure is needed to respond to your inquiry. However, personal information will not be sold to any third parties, nor will such information be added to bulk email lists. If you subscribe to any newsletters or other recurring sources of information emanating from this website, you will always have the ability to "unsubscribe" from those newsletters or information sources at any time.

                                        </p>
                                        <p>
                                            From time to time, W BRAND STUDIO, Inc. may conduct surveys or polls on our website. If you choose to participate in a survey or poll and provide personal information, your participation is entirely voluntary. Regardless, any personal information provided by you will be protected in accordance with our privacy policy. We will never request personal information from you, such as your social security number or account numbers via email. If you receive an email request for such information, it is most likely the result of a "phishing" effort, and you should not provide the information.

                                        </p>
                                        <p>
                                            Disclosures of Illegal Activities <br>
                                            If you engage in activities such as spoofing, spamming, the posting of libelous, slanderous, defamatory, or infringing materials, or other unlawful acts, W BRAND STUDIO, Inc. may be required to disclose your name and other identifying information to our web hosting provider or to governmental authorities.

                                        </p>
                                        <p>
                                            Policy Revisions <br>
                                            Any changes to W BRAND STUDIO, Inc's privacy policy will be promptly communicated on this website. Policy changes will not alter how we handle previously submitted personal information. W BRAND STUDIO, Inc. does not warrant or assume any legal liability from information that may be contained on this website and for which you may have taken any course of action, based solely on information provided on this website.

                                        </p>
                                        <p>
                                            Links <br>
                                            This website may provide links to other Internet sites as a service to its users. These include the websites of the various clients that we previously or currently represent. W BRAND STUDIO, Inc. is not responsible for the availability or content of these third-party sites. Further, W BRAND STUDIO, Inc. does not warrant or assume any legal liability or responsibility for the accuracy, completeness, usefulness, or content of any information, apparatus, product, or process disclosed on third-party websites.

                                        </p>


                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection