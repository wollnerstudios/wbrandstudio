@extends('layouts.default')

@section('title')
W Brand Studio: Award-winning Digital Marketing Agency | Newport Beach, Orange County @endsection

@section('description')
W Brand Studio is a leading, full-service digital marketing agency in Orange County. We specialize in customer engagement through storytelling, branding, visual identity, design and technoogy.@endsection

@section('keywords')
    digital marketing agency, digital marketing Irvine, marketing firm orange county, digital agency Irvine, bdigital marketing agency Irvine, marketing agency irvine, @endsection

@section('abstract')
Marketing agency, marketing firm, digital marketing agency, branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('brandingHeader')
@section('brandTitle', 'DIGITAL MARKETING AGENCY')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-marketing-firm">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            When you have clients that have been with you for up to 17 years, you've go to be doing something right.
                        </h1>
                        <p> Clients want better results, more leads and sales. Don't you? Of course you do. If you'd like to improve your digital marketing efforts, get more leads and sales, increase brand awareness, and make true, engaging connections with your customers, call us today! We have a proven, award-winning track record in marketing and branding.

Sure, you can find agencies that will offer you their services for less, but they can't deliver the results we do.  <br><br>We're honest and realistic. We'll never make promises we can't keep just to get you in the door, nor do we believe in nickel-and-diming our clients. We believe in simplicity and creating engaging content that can generate trust and results. If you agree with our philosophy, call us today. It's time for a partner that really is looking out for you in every way. 
We are a one stop shop for all your online and offline marketing needs.<br><br>
                            <br><br><img class="wid-100 hidden-xs" src="{{url('/assets/images/owl-kawai-edmon-createapp.jpg')}}" alt="Branding for Menu"><br><br>

                            <br><br>
                            <a href="http://www.wbrandstudio.com/contact"><img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;"></a>

                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        </div>
                    </div>
                </div>
            </section>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/marketing_banner.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">MARKETING SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>Put our creative forces to work and let's help you reach your business goals.</strong>
                                        </h2>
                                        <p class="text-left">
                                            - Marketing Audit<br>
                                            - Marketing Strategy and Implementation<br>
                                            - Design and Storytelling<br>
                                            - Website Development<br>
                                            - Analytics and Reporting<br>
                                            - Brand Positioning<br>
                                            - SEO, SEM & AdWords<br>
                                            - Content Marketing<br>
                                            - Social Media Marketing
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Everyone has a story.
                        </h2>
                        <p>We are storytellers. It's the stories that we share that make us unique and interesting individuals. Powerful experiences can effect perception, and more importantly change the way you do into others. Whether it's traditional advertising or digital marketing, We have decades of proven expertise both online and off-line. We create powerful messaging with good storytelling, engaging consumers and connect with them on an emotional level. From Fortune 500 companies to first time start-ups, our unique knowledge and flawless execution is second to none.
                            <br><br>
                            We not only care about brands, we care about people. We care about doing the right thing. We care about our community, being loyal to our clients, and being passionate about our work.
                            <br><br>
                        <h3><u><a href="contact">Let's set-up a meeting and find your story. </a></u></h3></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/digital-marketing-advertising.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">Our Triple D Threat approach to building successful brands. </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">When you break it down, our process is quite simple. Discover. Design. Deliver. Obviously each phase takes time to achieve perfection, but we're in the branding business and can accelerate the process to meet any deadline. Our team of strategists, storytellers, designers, and programmers are committed to your brand's success. Drop us a line and let us apply the Triple D Threat on your brand today.

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection