@extends('layouts.default')

@section('title')
    W Brand Studio, a total branding company in Orange County, CA @endsection

@section('description')
    We refresh and create brands that flow though your corporate veins like liquid to make you strong consistent.@endsection

@section('keywords')
    branding, branding agency, marketing agency @endsection
@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'BRANDING IS EVERYTHING')
@include('partials.branding-header')
@endsection



@section('content')
    <div class="page page-branding-philosophy">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            <span style="color: #178296">Walk on down the hall it gets even better.
                            </span>
                        </h1>
                        <p>Fall into the red room. It’s where our coders do code, where websites come to life. The space is refreshing and energizing with 11’ long, “starry night” overhead lights, made from two layers of metal sheets filled with different sized holes. Creative. Contemporary. Colorful. The desks are simple and plain white with vibrant red, modern leather chairs that match the pencil holders. And yes, the coffee cups are also red, right next to the Red Vines licorice.

                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 single-column-gray" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-12 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title normal" style="position: static;">BRANDING IS EVERYTHING</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p>
                                            Moving along, you’ll be green with envy as you enter the creative think tank. It’s where concept people are concepting, giving birth to new ideas and brands. The environment has an organic feel with raw wood doors, corrugated aluminum dividers, and lime green accent walls. From color psychology we know that green suggests something new and fresh, making it the perfect setting for the minds that work here.
                                        </p>

                                        <br>

                                        <img class="img-responsive" src="{{url('/assets/images/office-w-brand.jpg')}}" alt="">

                                        <br>

                                        <p>
                                            You’ll notice something different the moment you walk in our front door. Bold orange walls with large graphics, rich mahogany cabinetry, and raw concrete dividers create our entryway. Our offices define our brand and the people who work here. Branding is what we do and all we do. I’d challenge you to find a brand agency that dives this deep when creating a brand. From brand strategy, visual identity to interior and exterior design. Your on-hold music to how you answer the phone, vehicle wraps to employee uniforms—we don’t leave anything to chance. All well thought out, well planned, and well executed.
                                        </p>

                                        <br>

                                        <p>
                                            Come see my shoes and I’ll show you how it’s done.<br><br>

                                            - Michael K. Wollner, CEO.
                                        </p>

                                        <h3>Put some color in your brand’s life; call us at (657) 232-0110.</h3>

                                        <br>

                                        <p>
                                            <a href="about">RETURN TO 'ABOUT' &#187;</a><br><br><br><br>
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection