@extends('layouts.default')

@section('title')
    Brand Development | Branding, SEO and PPC @endsection

@section('description')
    GIve us 15 minutes and we will tell you how we can help you reach your online and offline goals.@endsection

@section('keywords')
    branding, branding agency, marketing agency @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'GOT IT')
@include('partials.branding-header')
@endsection



@section('content')

    <div class="page page-thank-you">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Thank you for your request.
                        </h1>
                        <p>We're eager to hear your story, whether it's just a project or a complete brand overhaul-we're here for you. If you can't wait another minute to get started, call (657) 232-0110 and ask for Michael.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 single-column-gray" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-12 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title normal" style="position: static;">What our brand is all about. </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">

                                        <p>

                                            W Brand Studio is in business do one thing; to be your branding consultant. From logo development and visual identity to sales and marketing campaigns, we do it all. We'll redefine your brand and reconnect with your customers on an emotional level, in a way that has never been done before. Your corporate identity will be as consistent and unique as you are. Even better, your customers will easily find you online with our expert SEO strategies and advice.
                                            <br><br>
                                            How can one company accomplish all of this? The answer is quite simple. We're not your typical design agency, Internet marketing company, or advertising firm. W Brand Studio is a unique, innovative digital branding and strategic marketing agency in Newport Beach, California, giving life to celebrated brands all over the country. And we're excited that you've chosen to contact us. We can't wait to hear your story!
                                            <br><br>
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <!-- Google Code for Sign Up Conversion Page -->
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 944395903;
            var google_conversion_label = "hIE5CIb48HYQ_6ypwgM";
            var google_remarketing_only = false;
            /* ]]> */
        </script>

        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/944395903/?label=hIE5CIb48HYQ_6ypwgM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>


@endsection