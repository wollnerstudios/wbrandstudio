@extends('layouts.default')

@section('title')
    Award Winning Website Design and Builders | Located in Orange County California
@endsection

@section('description')
    Companies that need expert website design turn to our team of talented website developers in Orange County Ca to help build their brand the right way.
@endsection

@section('keywords')
    website design, logo design
@endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company
@endsection

@section('brandingHeader')
@section('brandTitle', 'CUSTOM WEBSITE DESIGNERS')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-online-marketing-agency-orange-county">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h1 class="text-center">Website Design That Works</h1>

                        <p style="margin-bottom: 1rem;">When it comes to website design, most companies only focus on how it looks visually. While this is a very important aspect of the overall site design, a website should be designed to do more.</p>

                        <p style="margin-bottom: 1rem;">Many times, your website is the first piece of content your prospects see when they look for you online. It’s important to work with a <strong>website builder</strong> that can develop your online brand to showcase your expertise.</p>

                        <p style="margin-bottom: 1rem;">This includes adding content that demonstrates your expertise, using a message and images to show you’re an expertise in your specific field. As one of the premier award-winning <strong>Orange County</strong> website design companies on the west coast, we can guide you down the right path.</p>

                        <p style="margin-bottom: 1rem;">Our Orange County Ca website design team will work with you to create a site that not only looks good visually, but performs well when it comes to the overall user experience.</p>


                        <p style="margin-bottom: 1rem;"></p>

                        <h2 class="text-center">Call (657) 232-0110 today!</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6  display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/expert-orange-county-website-design-team.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/expert-orange-county-website-design-team.jpg')}}" alt="Branding for Menu">
                    </div>
                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Custom Web Solutions</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>Visitors in today’s market are looking for specific information, if your website design makes it difficult, they will simply go to your competitor for help.</p>
                        				<br>
                        				<p>As our website <strong>design team</strong> listens to your needs, and we’ll share strategies that have already worked well for other clients. It’s truly a collaborative effort in order to deliver a well-designed website with the right content and branding to drive results.</p>
                        				<br>
                        				<p>One of the key aspects of our work is being able to custom design your <strong>website</strong>, rather than simply using a theme or template that thousands of other companies have installed. This helps your website stand above the rest and become a memorable part of your marketing initiatives.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/california-website-deveopment-experts.jpg')}}" alt="Orange County Lead Generation">

                        <img class="wid-100 visible-xs" src="{{url('assets/images/california-website-deveopment-experts.jpg')}}" alt="Orange County Lead Generation">

                        <div class="subtext-img-cap hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title landing-orange-titles-margin">Your Single Design Source</h2>

                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>Since we’re a complete branding and digital agency in Orange County California, we can provide a full range of <strong>digital marketing</strong> help. </p>
                                        <br>

                                        <p>This means you can have a single partner for web design, graphics, online marketing and branding.</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left landing-orange-titles-margin">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray text-center tk-futura-pt">
                                            <p class="text-left bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                                <br><br>Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.

                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/custom-design-for-your-website-project-needs.jpg')}}" alt="Online Marketing Experts">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/custom-design-for-your-website-project-needs.jpg')}}" alt="Online Marketing Experts">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Ready to Discuss Your Website Content?</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>When you need website design help and want an award-winning team on your side, we’re here to help. </p>
                                        <br>
                                        <p>To discuss your next project, please <a href="/contact">contact our team</a> today and let’s start your journey together.</p>
                                    </div>

                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <div align="center">
            <img class="img-responsive" src="{{url('/assets/images/wbrand-orange-county-ca-client-logos.jpg')}}" alt="" style="PADDING-TOP: 20px; PADDING-BOTTOM: 20px;">
        </div>
    </div>
@endsection
