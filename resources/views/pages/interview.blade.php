@extends('layouts.default')

@section('title')
    Excellence in branding | W Brand Studio | Branding, SEO and PPC @endsection

@section('description')
    What every client needs to know about excellence in digital marketing and working with an agency.@endsection

@section('keywords')
    branding, branding agency, marketing agency @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('brandingHeader')
@section('brandTitle', 'INTERVIEW')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-interview">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Excellence - what every client needs to know.
                        </h1>
                        <p>There's the old saying that the reason God gave us two ears and one mouth is so that we will listen twice as much as we talk. And we really listen to our clients. We have ability and experience to create a strategy and concept that fits with their personality, but listening is an essential skill.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 single-column-gray" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-12 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title normal">Cliff Vegas meets Michael K. Wollner</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong>Define "EXCELLENCE" please...

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>
                                            Excellence is an important value goal and something definitely worth pursuing. And I’m not just talking about meeting deadlines and good grammar. I’m talking about excellence in the quality of creativity – the kind of excellence that transcends average and goes well above and beyond the norm.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> Doesn't that go without saying? Who wants average work?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>You’d be surprised. I can’t tell you how many times I’ve heard the phrase “quick and dirty” – from clients. By saying those words, of course, they’re setting the table on two fronts. First, of course, they’re saying, “the budget is small.” But they’re also telegraphing an expectation of mediocrity – average work – uninspired.

                                        </p>

                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> But poor work? As though an ounce of imagination might cost too much?

                                        </p>

                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Yes! Imagine if you asked a person to paint your house. To save money, you agree to standard colors and no special trim. But still, don’t you expect that the painter knows enough not to paint over the windows and the plants around the house? Do you have to say, “Hey, don’t paint the windows and the plants.”

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> That’s different though, isn’t it? You’re talking about craftsmanship, professionalism…

                                        </p>
                                        <p>

                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Yes and no. Yes, because we don’t expect to have to set down a bible of rules for the painter; we expect that common sense will guide him to avoid the windows and the plants. But does that change when we say or infer “quick dirty”? Although we want to control cost, do we not imagine something that will also make us happy?
                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> But these are nuances – isn’t “excellence” going above and beyond the call?

                                        </p>
                                        <aside class="pull-right quote">
                                            <em>
                                                "There’s the old saying that the reason God gave us two ears and one mouth is so that we will listen twice as much as we talk. And we really listen to our clients."
                                            </em>
                                        </aside>

                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> That’s my point. You see, while the client may want to control cost, the expectation is always something better than average, better than “dirty.”

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> Everybody wants “quick,” but nobody wants “dirty.”

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>Exactly. No matter what the client says – even if he or she insists – I meet my own expectations. I would want nothing but the very best, a fully optimized solution.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> But doesn’t that take more time; doesn’t that approach tend to break the budget?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Not necessarily. A small budget never is a hindrance to do excellent work. As for creative and optimized solutions, no matter how big the budget, the approach is the same; pursue excellence at every opportunity.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> But small budgets are a limitation, aren’t they?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Well, yes. A small budget may limit the final execution in some way or remove important media channels that might be ideal for the concept. But then that’s where the client has made a material decision on how and when and how often they want to deploy. But even then, a good creative agency is able to find ways to communicate within budget restraints; maximize the results as much as possible. When you have limitations, creativity really thrives.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> So what role does budget play in achieving excellence?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Of course it's important that the client and the agency have a realistic view of what can be achieved within the budget.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> What do you mean by “realistic”?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>Both sides must be honest with each other. The client should understand that they can't buy a new Mercedes at the price of a Vespa and be open about their resources. Then again, the agency shouldn’t pretend or suggest that the client is going to get something that they are not going to get.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> So there are limitations.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Of course there are. Having said that, there’s never a limitation on excellence – no budget can limit the opportunity to go beyond expectations. Achieving excellence is qualitative, not quantitative. You cannot reach a national audience in a matter of days with a low budget campaign. But you can lock-in the attention of a smaller audience with an excellent selling proposition or a well-planned message that has impact.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> So client should look for talent over bucks? Isn’t real talent expensive?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Talent is everywhere. But for whatever reason, both sides get lazy. On the agency side, people get lazy because they get complacent. On the client side, they stop expecting for an agency that stands out in the crowd. But there’s that other element – the personal relationship. Get up and meet with your agency in person. If you like each other, if you work together well as a team, then the opportunity for excellence will grow.
                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> The relationship between agency and client is that important?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Of course it is. Good relationships are honest. They provide the warmth that I believe is often absent in creative communications. Just look at the material around us – all of those impersonal “quick and dirty” projects that do nothing to capture attention – they are a waste of time and money because they were created to complete a task, to meet a job requirement.

                                        </p>
                                        <p>
                                            If you build a relationship, you build a unique opportunity that transcends the impersonal nature of communications. After all – we are trying to touch people at a distance. You need all of the personality and pathos you can get to bridge the gap of an ad or a brochure or a website and touch your target audience. That kind of work is best done in the warmth of a good relationship.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> What makes you so special?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> We're all unique, it's just that most of the time people don't dare go outside their safe little world. Most of the time, it takes one person to step out of the envelope of the typical agency-client relationship to be daring and unique. That’s what I do – not only do I dare to stand out, I also dare to be totally honest with the client from the very start.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> That follows your idea about excellence…

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>In a way it communicates how I am in my work. Look, if people don't notice you, you're not going to sell your ideas. It's very simple actually. If you are really serious about your business you need to take a step of faith, listen to the professionals and distinguish yourself from the rest. We can make that happen. I guess that's what makes us so special.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span> </strong>Are you expensive?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong>Everything is relative. I suppose there are some clients who think we're pretty "cheap" – given the quality of work we have provided and the effectiveness of their campaigns. That’s why honesty is so important in this business. If a client is honest about their expectations – then the work of getting to an excellent creative solution will be easy. People who are serious about their business are also serious about their expectations. It's important to remember that these things are all about attitude. If you want cheap flyers that anybody can do, then maybe you’ll have to adjust a little if you want somebody like me to work with you. “Quick and dirty” is a misconception – when people want things done cheaply, then they have cheapened their expectations. But if they want excellence and want to be satisfied, then isn’t it better to pay a little on the front end to ensure that you will achieve that satisfaction down the road?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> But do big budgets guarantee good results?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Absolutely not. My colleagues and I have had clients that spent a lot of time and money with the wrong people. They are completely honest with us – usually to the point where they can explain their budget and their expectations very clearly. That sets the table for good work because we have an open relationship – I’m open, they’re open, and everybody is looking for good solutions. What I’m trying to say is that big budgets and closed processes are not ideal. Really what you want is a cost-effective working relationship that optimizes the creative process. At some point, the size of the budget doesn’t matter at all. But if you don't have a big budget, you have to be smart, quick and you have to be willing to take risks.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> What kind of background or abilities do you and your colleages have that is important for a client?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> There’s the old saying that the reason God gave us two ears and one mouth is so that we will listen twice as much as we talk. And we really listen to our clients. We have ability and experience to create a strategy and concept that fits with their personality, but listening is an essential skill.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> I noticed that you have an impressive list of clients. But what about the entrepreneur on Main Street?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> The same principals apply to smaller companies. All of us have long experiences with small businesses and individual entrepreneurs. For us it's all about the most interesting projects and challenges – producing a truly qualitative result that ends with a happy client.

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #f48325"> Vegas:</span>
                                            </strong> One last question, if a client wants to work with your studio, what can he or she expect?

                                        </p>
                                        <p>
                                            <strong>
                                                <span style="color: #24bed6">Michael:</span>
                                            </strong> Excellence on all levels. That we will listen and do research. We will perform with the highest standards. We will be on time, on budget and come up with solutions that will set them apart from their competitors. That we will take branding to a whole new level that will leave a big impression.<br><br><br>

                                            <a href="/about">Return to ABOUT US.</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection