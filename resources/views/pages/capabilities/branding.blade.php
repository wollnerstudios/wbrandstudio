@extends('layouts.default')

@section('title')
    Professional Branding Services | Branding Agency
@endsection

@section('description')
    We are an innovative and creative brand development agency in Orange County, CA. We create value with brand strategy, positioning, and design.
@endsection

@section('keywords')
    branding agency, branding agency orange county
@endsection

@section('abstract')
    We are an innovative and creative brand development agency in Orange County, CA. We create value with brand strategy, positioning, and design
@endsection
@section('customHTMLClass')
    capabilities-page @endsection

@section('brandingHeader')
@section('brandTitle', 'Branding')

@section('brandHeaderImage','#')

@include('partials.branding-header-capabilities')
@endsection


@section('content')
    <div class="page page-capabilities-branding">
        <img class="wid-100 hidden-xs" src="/images/total-branding-wollnerstudios-2.jpg" alt="branding">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            ELEVATE YOUR BRAND WITH OUR FULL-SERVICE TOTAL BRANDING APPROACH
                        </h1>
                        <div class="sm-underline">&nbsp;</div>
                        <p>
                            Since 1997, we have been helping businesses build their brands and achieve success. Our award-winning team has the expertise and experience needed to work with both entrepreneurs and Fortune 500 companies. We understand the importance of branding and the impact it can have on a business's success. We work closely with our clients to create a comprehensive branding strategy that includes both digital and physical branding elements. We understand the importance of creating a consistent and cohesive brand identity, and we believe that bringing your brand into your office space is an essential element of achieving this goal. From designing your company logo and visual identity to creating custom office signage and interior design, we are committed to helping you build a strong and recognizable brand that reflects your business's unique identity.
                        </p><br>
                        <p style="font-weight: bold;">Our track record of success speaks for itself, and we look forward to working with you to achieve your branding goals.</p>
                        <br>
                        <p>
                            <a href="/contact" class="myButton org-btn">CONTACT US</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" style="background:#BFA179;font-size: 18px;">
            <div class="container text-white text-center">
                <i>“Wollner embraced the difficult challenge of re-branding Laguna Woods Village, a 40-year-old "active adult" community with over 18,000 residents. The Wollner team was professional and responsive to our desires, providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection”</i>
                <br><br>
                Wendy Bucknum, CMCA, AMS, CACM
                <br>Government & Public Affairs Manager, Professional Community Management, Inc.
                <br>Agent for Laguna Woods Village
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-bronze pad-b-1" style="font-size: 3.6rem">Ready to take your brand to the next level? Let's talk.</h2>
                        <div class="sm-underline">&nbsp;</div>

                        <p>Why chose WollnerStudios?  We are a 25 year old full-service agency with a proven “Total Branding” process, and strongly believe in the power of research and getting to know our clients before we begin any strategy or design work. We understand that every business has a unique story to tell, and it is our job to help them tell it in a way that resonates with your audience. We take the time to conduct thorough research into our clients' industries, target audiences, and competitors, in order to gain a deep understanding of their brand and their customers. This allows us to develop a tailored branding strategy that accurately reflects our clients' values and goals. Our approach ensures that every aspect of our branding work is grounded in research and is specifically tailored to our clients' needs, resulting in a truly unique and effective brand identity.</p>
                        <br>
                        <p>
                            <a href="/contact" class="myButton org-btn">CONTACT US</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-copy-section  pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2 class="page-title pad-b-1" style="font-size: 3.6rem">OUR BRANDING SERVICES INCLUDE:</h2>
                    </div>
                </div>
                <div class="row" style="font-weight: normal; font-size: 16px; max-width: 640px; margin: 0 auto;">
                    <div class="col-sm-12 col-md-6">
                        <ul>
                            <li>
                                Brand Audits and Analysis
                            </li>
                            <li>
                                Brand Strategy
                            </li>
                            <li>
                                Brand Messaging
                            </li>
                            <li>
                                Logo Design
                            </li>
                            <li>
                                Brand Guidelines
                            </li>
                            <li>
                                Visual IdentityBrand Audits and Analysis
                            </li>
                            <li>
                                Marketing Collateral
                            </li>
                            <li>
                                Website Design and Development
                            </li>
                        </ul>

                    </div>
                    <div class="col-sm-12 col-md-6">
                        <ul>
                            <li>
                                Social Media Strategy and Management
                            </li>
                            <li>
                                Copywriting and Content Creation
                            </li>
                            <li>
                                Advertising Campaigns
                            </li>
                            <li>
                                Public Relations and Media Outreach
                            </li>
                            <li>
                                Event Branding
                            </li>
                            <li>
                                Interior Design and Signage
                            </li>
                            <li>
                                Packaging Design
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="container">
                <div class="page-copy-section text-center pad-t-1 hidden-xs tk-futura-pt wow fadeInUp container text-center" data-wow-offset="100" data-wow-delay="100ms" style="margin-bottom:60px;">
                    <img class="wid-100 " src="/assets/images/clients-logos.jpg" alt="branding and marketing">
                </div>
            </div>
        </section>



        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/images/prr.jpeg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/images/Edmon-visual-identity-web.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/images/PRR-visual-identity-web.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/images/edmon.jpeg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        

        
    </div>
@endsection