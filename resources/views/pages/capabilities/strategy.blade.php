@extends('layouts.default')

@section('title')
    Award Winning Branding Agency | Creative Brand Strategies
@endsection

@section('description')
    Our branding touchpoint strategies will enable you to acquire and retain customers, fight attrition, and keep customers coming back for more.
@endsection

@section('keywords')
     branding agency, brand strategy @endsection

@section('abstract')
    Our branding touchpoint strategies will enable you to acquire and retain customers, fight attrition, and keep customers coming back for more.
@endsection

@section('customHTMLClass')
    capabilities-page @endsection


@section('brandingHeader')
@section('brandTitle', 'Brand Strategy')

@section('brandHeaderImage','http://wollnerstudios.wbrandstudio.com/assets/images/capabilities/')
@include('partials.branding-header-capabilities')
@endsection


@section('content')
    <div class="page page-capabilities-strategy">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                         <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">The right touchpoint strategy for your business, as only a real branding agency can do.</h1>
                        <div class="sm-underline">&nbsp;</div>

                        <p>As an experienced branding agency, we know how important touchpoints are when it comes to customer interaction. Every touchpoint is an opportunity to enhance your brand. These sensory magnets impact your audience's needs and perceptions. A touchpoint is every place a customer is in touch with you. Whether it is online, in your office, through brochures, business cards or ads, it's an opportunity to make your brand stronger and consistent. They're the point of contact that'll help you attract, acquire, and retain customers. It's our job to clearly communicate through the touchpoints. We'll unify the message about your company, define your positioning, differentiate your advantages, and offer up a unique value proposition. Then we reinforce the touchpoints to ensure that all components of your brand have a consistent message and that it continues to build upon your vision and company mission. </p>
                        
                        <br><br>
                    <div>
                            <img class="img-responsive" src="{{url('/assets/images/capabilities/touchpoints.jpg')}}" alt="">
                        </div>
                        </div>
                </div>
            </div>
        </section>
        <!-- <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/millennium-branding.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/lgarde-exterior.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/lgarde-lobby.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/sentry-interior.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <section class="page-copy-section text-center pad-t-1 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>The relationship between a brand and a customer is more complex than you may think. It's been proven that strong, sustainable branding is a direct result of identifying and emphasizing marketing touchpoints that impact all marketing efforts and operations. And well-planned touchpoint strategies must align with your overall core business strategy. This will help define your company's values and culture. Plus, it also gives you a unique perspective of every aspect of your marketing communications program; including public relations, advertising, promotions, and sales. These touchpoint strategies will enable you to acquire and retain customers, fight attrition, and keep customers coming back for more.</p>

                        <br><br>

                        
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
