@extends('layouts.default')

@section('title')
    PPC Advertising | Pay-Per-Click Management Services @endsection

@section('description')
    Our pay-per-click management services can help you reach your target audience with customized PPC advertising campaigns. Our experts will develop a strategy that fits your budget and goals
@endsection

@section('keywords')
    PPC, pay-per-click, branding agency, digital marketing agency, marketing agency orange county, branding agency orange county
@endsection

@section('abstract')
    At W Brand Studio we are the VIP team for companies who wants to grow. We are a full service branding agency in Orange County CA
@endsection
@section('customHTMLClass')
    capabilities-page @endsection


@section('brandingHeader')
@section('brandTitle', 'Pay-per-Click (PPC)')
@include('partials.branding-header')
@endsection

@include('partials.branding-modal')
@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }
            }
        });
    });
    </script>
    <div class="page page-capabilities-index">
        <section class="page-copy-section text-center pad-t-4 pad-b-1 tk-futura-pt opacity-0 wow fadeInUp" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>W Brand Studio is a Premier PPC Management Agency</h2>
                        <p>PPC, or Pay-Per-Clicks, is another way to market your company to your target audience through relevant Google, Facebook, and Yahoo/Bing advertisements. However, truly stand-out PPC management requires thorough research of target audiences and complimentary keywords between your PPC campaigns, landing pages, and websites. As a complete brand studio, we not only understand marketing cohesion of every variation, we employ our data research capabilities to increase leads and conversions.</p>
<h3><strong>PPC SERVICES</strong></h3>
                        <p>
PPC Ad & Landing Page Creation<br>
Selection of Relevant Keywords with Commercial Value<br>
Conversion Rate Optimization<br>
Monthly Campaign Management <br>
Comprehensive & Targeted Click Tracking<br>
Google Quality Score Improvement<br>
Algorithm Analysis for Company Visibility<br>
Audience Targeting & Remarketing<br>
A/B Testing for Ads & Landing Pages<br>
<br>
                            <a data-toggle="modal" href="#web-modal" class="myButton org-btn">Contact us for a free consultation</a><br><br>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<div class="g-partnersbadge" data-agency-id="5228729113"></div><br><br>
                </div>
            </div>
        </section>

        
        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <div class="contact-form-container">
                            {{--<div class="contact-form-close-button contact-form-close-button--webDesign">--}}

                            {{--<div>x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <section id="slick">
                                        <div class="feedback-form">
                                            <form action="/contact" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">name</div>
                                                                <div class="field">
                                                                    <input type="text" name="name" id="name" required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your name</span>
                                                                </div>
                                                                <div class="form-title">company</div>
                                                                <div class="field">
                                                                    <input type="text" name="company" id="company"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Company</span>
                                                                </div>
                                                                <div class="form-title">email</div>
                                                                <div class="field">
                                                                    <input type="email" name="email" id="email"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Email</span>
                                                                </div>
                                                                <div class="form-title">phone</div>
                                                                <div class="field">
                                                                    <input type="text" name="phone" id="phone"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                                </div>
                                                                <input type="hidden" name="recaptcha" id="recaptcha">
                                                                <input type="submit" value="Submit"
                                                                       class="send send--webDesign"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">tell us what you need</div>
                                                                <div class="field">
                                                                    <textarea name="message" placeholder="" id="message"
                                                                              class="message" required></textarea>
                                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                                    <span class="slick-tip left">Your message</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div><!--row-->
                        </div><!--container-->
                        {{--<img class="wid-100 hidden-xs" src="{{url('assets/images/capabilities-seo.jpg')}}" alt="L Garde Smart Space Technology Building">--}}
                        {{--<img class="wid-100 visible-xs" src="{{url('assets/images/mobile/seo-mobile.jpg')}}" alt="L Garde Smart Space Technology Building">--}}
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title right">SEM (SEO / PPC)</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">To keep your company at the top of organic searches, you need a powerful Search Engine Optimization (SEO) strategy. Relevant content. And you need to adhere to search engine guidelines. We work within best-practice Pay-Per-Click (PPC) methods that'll drive you up not only in the ranks, but also in the conversion rates.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection