@extends('layouts.default')

@section('title')
SEO & SEM Agency | Search Engine Marketing Orange County | Online Marketing Agency
@endsection

@section('description')
We are a digital marketing company with SEO, SEM, & Data Analytics Reporting services, customized to give each client a uniquely competitive edge.

@endsection

@section('keywords')
seo, pay per click, ppc, sem, marketing @endsection

@section('abstract')
    Increase your website rankings with search engine optimization & pay per click (PPC) advertising from the Orange County SEO experts at W Brand Studio today.
@endsection

@section('brandingHeader')
    @section('brandTitle', 'SEM - Online Marketing')
    @section('brandColor', '#0ebed8')
   
    @include('partials.branding-header')
@endsection
@section('content')
    <!-- <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script> -->

    <!-- <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
                if(token) {
                    console.log('Welcome to W Brand');
                    var elements = document.getElementsByClassName("recaptcha");
                    for(var x=0; x < elements.length; x++) {
                        elements[x].value = token;
                    }
                }
            });
        });
    </script> -->

    <!-- MODAL 
    <div id="web-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none;"> -->
        <!-- MODAL DIALOG 
        <div class="modal-dialog" role="document"> -->
            <!-- MODAL CONTENT 
            <div class="modal-content" style="background-color: #efefef; border-radius: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: -25px; top: -25px; background: black; color: white; opacity: 1; border-radius: 50%; width: 30px; height: 30px; display: flex; align-items: center; justify-content: center;">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="modal-body">
                    <form action="/website-contact-submit" method="post" style="margin-bottom: 50px;">
                        @csrf

                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="company" placeholder="Company">
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" required>
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="price" required>
                                <option>My budget is:</option>
                                <option>$5,000 - $10,000</option>
                                <option>$10,001 - $20,000</option>
                                <option>$20,001 - $30,000</option>
                                <option>$30,001 - $40,000</option>
                                <option>$40,001 - $50,000</option>
                                <option>$50,000 +</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>I am looking for:</label>

                            <br>

                            <div class="row" style="display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;">
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input type="checkbox" value="branding">Branding</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service1" type="checkbox" value="visual ID">Visual Identity</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service2" type="checkbox" value="logo design">Logo Design</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service3" type="checkbox" value="web dev">Website Development</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service4" type="checkbox" value="social media management">Social Media Management</label>
                                </div>
                                
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p"  style="margin-left: 10px;"><input name="service5" type="checkbox" value="seo">SEO</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service6" type="checkbox" value="ppc">PPC</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service7" type="checkbox" value="video prod">Video/Commercial Production</label>
                                    
                                    <label class="checkbox-inline w-20p" ><input name="service8" type="checkbox" value="photography">Photography</label>
                                </div>
                                
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input name="service9" type="checkbox" value="int/ext design">Interior/Exterior Design</label>
                                   
                                    <label class="checkbox-inline w-20p"><input name="service10" type="checkbox" value="print">Print Design (i.e. brochure)</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service11" type="checkbox" value="Naming">Naming</label>
                                    
                                    <label class="checkbox-inline w-20p"><input name="service12" type="checkbox" value="marketing">Marketing</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <input type="text" class="form-control" name="comments" placeholder="Comments:">
                        </div>
                        
                        <input type="hidden" name="recaptcha" class="recaptcha">
                        
                        <input type="hidden" name="page" value="seo-sem">
                        
                        <button type="submit" class="" style="margin-left: auto; position: absolute; bottom: 0; right: 0 margin-left: auto; position: absolute; bottom: 0; right: 0; background: orange; color: white; padding: 10px; font-weight: bold;">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div> -->

@section('content')
    <!-- todo Change image to space image -->
    <img class="wid-100 hidden-xs" src="/images/SEO-services-wollnerstudios.jpg" alt="SEO hero image">

    <div class="page page-capabilities-seo-sem">
        <section class="page-copy-section text-center pad-b-1 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            Making sure customers find you doesn’t take millions. It takes smarts.
                        </h1> -->
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            Accelerate Your Online Success WITH SEO Services Tailored for You
                        </h1>
                        <div class="sm-underline">&nbsp;</div>

                        <!-- <p>
                            W Brand Studio’s digital marketing gurus have years of experience building, scaling, and managing brands online. We use a shrewd mix of SEO, SEM, PPC and multiple analytics customized for your business, your target customers, and your goals. We make sure people find you before they find your competition. (We made sure you found us pretty quickly, right?)

                            <p style="color:#BFA179!important;">
                            <br><br><strong> Digital Marketing Services</strong><br>

                            <a href="seo-search-engine-optimization" style="color:#BFA179!important;font-weight:bold;">Search Engine Optimization (SEO)</a><br>
                            <a href="ppc-pay-per-click" style="color:#BFA179!important;font-weight:bold;">Pay-Per-Click Advertising (PPC)</a><br>
                            <a href="data-analytics-reporting" style="color:#BFA179!important;font-weight:bold;">Data Collection, Analytics & Reporting</a><br>
                            <a href="website-design" style="color:#BFA179!important;font-weight:bold;">Web Development</a><br>
                        </p> -->
                        <p class="mb-4">
                            WollnerStudios, an award-winning digital marketing agency, offers a comprehensive range of SEO services backed by data-driven strategies and a team of experts. Our approach is based on thorough research and scientifically-tested data, ensuring that decisions are made with solid evidence. We analyze every aspect of a client's business, website, and customer base to create custom SEO strategies that deliver optimal results. With a portfolio of hundreds of optimized websites, WollnerStudios excels in website design and development, crafting visually appealing sites that are optimized for search engine rankings. Our team consists of programmers, strategists, designers, and dedicated SEO specialists who work together to create high-performing websites and Search Optimization strategies. In addition to SEO, WollnerStudios provides local SEO services, email marketing, website design, and Google Ads management. We also offer long-term or monthly SEO services, allowing clients to choose the option that suits their needs. With our expertise, WollnerStudios helps businesses increase their online visibility, drive targeted traffic, and achieve their revenue goals through effective SEO strategies.
                        </p>

                        <div class="image-cap-section wow fadeInUp opacity-0 my-4">
                            <div class="container py-5 my-5" style="text-align:center!important;">
                                <a href="/contact" class="myButton org-btn">CONTACT US</a>
                            </div>
                        </div>

                        <p class="my-4" style="font-size:18px!important;">
                            <i>
                                "Since bringing the WollnerTeam onboard, we have not only began to realize ROI with additional revenue, but also a better understanding of the entire SEO & PPC process. We highly recommend them."
                            </i>
                        </p>

                        <p class="my-4 pb-5" style="font-size:18px!important;">
                            <strong>Stefanie Berumen, CPSM Director of Marketing, Shared Service Group</strong>
                        </p>

                        <p class="my-4 pt-5" style="font-size:18px!important;">
                            <i>
                                "These guys have been phenomenal! From the development of our website to SEO, PPC, Social Media and complete branding services. Give them a shout. I know you’ll be glad you did."
                            </i>
                        </p>

                        <p class="mb-4" style="font-size:18px!important;">
                            <strong>Rick Allen, Owner, Pure Elements Water</strong>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="image-cap-section wow fadeInUp opacity-0 mt-4">
            <img class="wid-100" src="/images/seo-ppc.jpg" alt="SEO & PPC collage">
        </section>
    
        <section class="page-copy-section text-center pad-t-1 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            Maximize Conversions and ROI with Results-Driven PPC Services
                        </h1>
                        <div class="sm-underline">&nbsp;</div>

                        <!-- <p>
                            We’re strategic about keeping your CPA (cost-per-acquisition) low and your CTR (click-through-rate) high using the latest tech and trends in key areas of digital marketing:
                        </p> -->
                        <p>
                            WollnerStudios provides a comprehensive range of PPC (Pay-Per-Click) services aimed at expanding businesses' reach and promoting their brand through Google Ads. PPC plays a vital role in effective search engine marketing campaigns, and we understand the dedication required to maintain a high level of performance. When partnering with WollnerStudios, you can expect a dedicated project manager who is committed to improving your search engine visibility and driving a consistent flow of potential customers to your website. Their team of professionals ensures that all PPC strategies are tailored to each client's specific needs. Through continuous testing and innovation, WollnerStudios focuses on enhancing campaign performance, with a keen emphasis on cost per acquisition (CPA) and the key metrics that matter most to your business. We offer a premium service, creating a comprehensive marketing environment designed for scalability. We ensure that your entire operation aligns with Google's best practices, a complex endeavor that sets the foundation for optimal success.
                        </p>

                        <div class="image-cap-section wow fadeInUp opacity-0 mt-4">
                            <div class="container py-5 my-5" style="text-align:center!important;">
                                <a href="/contact" class="myButton org-btn">CONTACT US</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="image-cap-section wow fadeInUp opacity-0 mt-4">
            <img class="wid-100" src="/images/search-engine-optimization.jpg" alt="SEO collage">
        </section>
    
        <!-- <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keywords.png')}}" alt="">
                                <h2>Keywords</h2>
                                <p style="text-align:left!important;">
                                    Which words and phrases will land your business on Google’s front page? We’ll find out.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keyword-search.png')}}" alt="">
                                <h2>Optimization</h2>
                                <p style="text-align:left!important;">
                                    Populating and updating your website with the right keywords makes sure your customers find you – for free. We know SEO.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/seo-icon.png')}}" alt="">
                                <h2>Analytics</h2>
                                <p style="text-align:left!important;">
                                    How are people interacting with your website? Let’s measure that. When they do, are they becoming customers? If not, let’s fix it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/conversion-icon.png')}}" alt="">
                                <h2>Conversion</h2>
                                <p style="text-align:left!important;">
                                    The holy grail of search analytics. Keyword-rich content and pay-per-click (PPC) ads help people land on your site and stay there. Site visitors are good; site customers are even better.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

        <!-- <section class="page-copy-section text-center pad-t-1 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container" style="color:#BFA179!important;">
                <a href="/contact" class="myButton org-btn">Sign up for a free SEM/SEO consultation</a><br>
            or call us at (657) 232-0110.
            </div>
        </section> -->
    </div>
@endsection