@extends('layouts.default')

@section('title')
    Interior Design Services | WollnerStudios
@endsection

@section('description')
    WollnerStudios offers interior design services that transform commercial spaces into beautiful, branded and functional environments that inspire creativity and productivity.
@endsection

@section('keywords')
    branding agency, interior design, exterior design, marketing agency, branding orange county, marketing orange county
@endsection

@section('abstract')
    We are an award-winning branding & marketing agency in Orange County. We believe in simplicity, relevance, originality & creating engaging brands.
@endsection

@section('customHTMLClass')
    capabilities-page
@endsection

@section('brandingHeader')
@section('brandTitle', 'Web Design')
@section('brandColor', '#a3db59')
@section('brandHeaderImage','http://wollnerstudios.wbrandstudio.com/assets/images/capabilities/')
@include('partials.branding-header-capabilities')
@endsection


@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }
            }
        });
    });
    </script>
    <div id="web-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background-color: #efefef; border-radius: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;
    right: -25px;
    top: -25px;
    background: black;
    color: white;
    opacity: 1;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body">
                    <form action="/website-contact-submit" method="post" style="margin-bottom: 50px;">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="company" placeholder="Company">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="price" required>
                                <option>My budget is:</option>
                                <option>$5,000 - $10,000</option>
                                <option>$10,001 - $20,000</option>
                                <option>$20,001 - $30,000</option>
                                <option>$30,001 - $40,000</option>
                                <option>$40,001 - $50,000</option>
                                <option>$50,000 +</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>I am looking for:</label>
                            <br>
                            <div class="row" style="display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;">
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input type="checkbox" value="branding">Branding</label>
                                    <label class="checkbox-inline w-20p"><input name="service1" type="checkbox" value="visual ID">Visual Identity</label>
                                    <label class="checkbox-inline w-20p"><input name="service2" type="checkbox" value="logo design">Logo Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service3" type="checkbox" value="web dev">Website Development</label>
                                    <label class="checkbox-inline w-20p"><input name="service4" type="checkbox" value="social media management">Social Media Management</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p"  style="margin-left: 10px;"><input name="service5" type="checkbox" value="seo">SEO</label>
                                    <label class="checkbox-inline w-20p"><input name="service6" type="checkbox" value="ppc">PPC</label>
                                    <label class="checkbox-inline w-20p"><input name="service7" type="checkbox" value="video prod">Video/Commercial Production</label>
                                    <label class="checkbox-inline w-20p" ><input name="service8" type="checkbox" value="photography">Photography</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input name="service9" type="checkbox" value="int/ext design">Interior/Exterior Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service10" type="checkbox" value="print">Print Design (i.e. brochure)</label>
                                    <label class="checkbox-inline w-20p"><input name="service11" type="checkbox" value="Naming">Naming</label>
                                    <label class="checkbox-inline w-20p"><input name="service12" type="checkbox" value="marketing">Marketing</label>
                                </div>


                            </div>

                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="comments" placeholder="Comments:">
                        </div>
                        <input type="hidden" name="recaptcha" class="recaptcha">
                        <input type="hidden" name="page" value="interior-design">
                        <button type="submit" class="" style="margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;    margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;
    background: orange;
    color: white;
    padding: 10px;
    font-weight: bold;">SUBMIT</button>
                    </form>

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="page page-capabilities-website-design">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Award-winning website development with clean and simple expression that drives more conversions.
                        </h1>

                        <p>
                            We are constantly pushing the aesthetic, technological and functional boundaries of website development and online marketing. With our roots in Scandinavian craftsmanship and efficient use of limited material resources, our work is characterized by simplicity, minimalism and functionality. The Scandinavian graphic design may be your best bet when you want to catch audiences, increasing performance and conversions in the world in which human attention span stands at just a few seconds.
                            <br><br><strong>Below are some examples of award-winning websites we have developed for our clients.</strong><br><br>
                            <em>
                                "What was hot yesterday may be pass&egrave; today.
                                Some companies like W Brand Studio not only keep up with the evolution of website design but set the standard."</em><br>

                            <span style="font-size: 11pt"><strong>Association of Marketing and Communication Professionals</strong></span>
                        </p>     <p>

                            <br><a href="/contact" class="myButton org-btn">LET'S TALK</a><br><br>
                        </p>
                        <h2>Call (657) 232-0110, or contact us at
                            <a href=mailto:hello@wbrandstudio.com>hello@wbrandstudio.com</a> now for a no-obligation, no cost quote.
                        </h2>

                   


                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0 background-c-shade-2 padding-t-5 padding-b-5">
            <div class="container ">
                <div class="row card-holder" style="display: flex; column-count: 3; flex-wrap: wrap;">
                    @foreach($projects as $project)
                        <div class="col-md-4 ">
                            <div class="card bg-white" style="min-height: 550px; position: relative;">
                                <img class="card-img-top" src="/storage/{{$project->image}}" alt="Card image" style="width:100%">
                                <div class="card-body">
                                    <h2 class="card-title font-f-1" style="line-height: 1; margin-bottom: 0; font-size: 22px;">{{$project->name}}</h2>
                                    <h4 class="font-f-1" style="margin-bottom: 30px; margin-top: 0; font-size: 14px;">{{$project->type}}</h4>
                                    <h4 class="font-f-1" style="margin-bottom: 30px; font-size: 14px;">{{$project->description}}</h4>
                                    <div class="card-bottom-row" style="display: flex; align-items: flex-end; justify-content: space-between; position: absolute; bottom: 10px; right: 10px; left: 10px;">
                                        <div class="awards">
                                            @if($project->platinum_marcom)
                                                <img src="/images/platinum-marcom.jpg" alt="">
                                            @endif
                                            @if($project->gold_marcom)
                                                <img src="/images/gold-marcom.jpg" alt="">
                                            @endif
                                            @if($project->gold_hermes)
                                                <img src="/images/hermes-gold.jpg" alt="">
                                            @endif


                                        </div>
                                        <div class="link">
                                            <a href="{{$project->link}}" target="_blank">
                                                <img src="/images/world-url-icon.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </section>
        {{--<section class="image-cap-section wow fadeInUp opacity-0">--}}
        {{--<div class="container-fluid ">--}}
        {{--<div class="row">--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Renew.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_CHMG_Website_430x305_Mock.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Shotcrete.gif')}}" alt="">--}}
        {{--</div>--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/prr-desktop-and-mobile-.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Edmon_Website_Mock.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</section>--}}

        <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/cms-icon.png')}}" alt="">
                                <h2>CMS</h2>
                                <p>Content Management Systems should be simple and intuitive, not complex and confusing. We custom build our solutions for each client, so there's no need for tutorials or manuals. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/ecommerce-icon.png')}}" alt="">
                                <h2>eCommerce</h2>
                                <p>All commerce-enabled websites are different. That's why we customize each site to the particular business and market. We'll select the right shopping cart system that makes the most sense for your business, whether that's Magento, eCart, or some other popular platform.

                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/mobile-icon-web.png')}}" alt="">
                                <h2>Mobile First</h2>
                                <p>In 2018, 58% of site visits were from mobile devices. A mobile first strategy, facilitates connections with customers, as smartphones are always close at hand. We focus on well-designed websites, since they are considered more trustworthy, more memorable, and easier to use. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/seo-icon.png')}}" alt="">
                                <h2>SEO</h2>
                                <p>With millions of websites online and more popping up all the time, you need optimization if you want to be found at all. Search Engine Optimization (SEO) and Pay-Per-Click (PPC) are the only ways to ensure your customers will find you online. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/baci-website.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/marriott-web-1.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/marriott-web-2.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/sentry-web-1.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection