@extends('layouts.default')

@section('title')
    Website Analytics Services |Web Traffic Reporting Services @endsection

@section('description')
    Smarter Website Analytics Services company that can improve your online business performance with meaningful insights. 
@endsection

@section('keywords')
    branding agency, marketing agency, marketing agency orange county, branding agency orange county
@endsection

@section('abstract')
    At W Brand Studio we refresh, reimagine, reposition, and create brands through any means possible. We are a full service branding agency in Orange County CA
@endsection
@section('customHTMLClass')
    capabilities-page @endsection


@section('brandingHeader')
@section('brandTitle', 'Data Analytics Reporting')
@include('partials.branding-header')
@endsection

@include('partials.branding-modal')
@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                document.getElementById('recaptcha').value = token;
            }
        });
    });
    </script>
    <div class="page page-capabilities-index">
        <section class="page-copy-section text-center pad-t-4 pad-b-1 tk-futura-pt opacity-0 wow fadeInUp" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Better Data Analytics for Better Business</h2>
                        <p>W Brand Studio collects and analyze more data for SEO purposes than most agencies. Because we are attentive to your brand in its entirety, we examine every detail to improve the traffic and visitor conversions. At W Brand Studio, we build automated data-driven systems based on your customers' online behaviors. Our unique data presentation approach helps identify your KPIs, target audiences, and potential ways to increase sales leads & revenue across platforms.</p>
                       <h3><strong>DATA & ANALYTICS SERVICES</strong></h3>
                        <p>
Clean Data Collection through Google Tag Manager for Google Analytics, Google Ads, Bing Ads, Facebook, & more<br>
Data Silo Maintenance<br>
Data Filtering & Stitching from Web Analytics, Mobile App Analytics, Online Advertising Platforms, & CRMs<br>
Multi-platform Data Consolidation<br>
Dynamic, Custom Data Visualization Creation<br>
Tailored Reports for Specific Business Insight Needs<br>
Business Intelligence Reports<br>
Custom Visualizations & Controls for Organizational Roles using Google Data Studio<br>
Social Media Activity Reports<br>
Custom Data Gathering & Presentation Strategies<br>
Custom Web Reporting<br>
Web Traffic Reports Across Platforms<br>
Monthly Web Traffic Analysis and customized expert reviews<br><br>
                            <a data-toggle="modal" href="#web-modal" class="myButton org-btn">Contact us for a free consultation</a><br><br>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<div class="g-partnersbadge" data-agency-id="5228729113"></div>
</p>
                    </div>
                </div>
            </div>
        </section>

        
        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <div class="contact-form-container">
                            {{--<div class="contact-form-close-button contact-form-close-button--webDesign">--}}

                            {{--<div>x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <section id="slick">
                                        <div class="feedback-form">
                                            <form action="/contact" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">name</div>
                                                                <div class="field">
                                                                    <input type="text" name="name" id="name" required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your name</span>
                                                                </div>
                                                                <div class="form-title">company</div>
                                                                <div class="field">
                                                                    <input type="text" name="company" id="company"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Company</span>
                                                                </div>
                                                                <div class="form-title">email</div>
                                                                <div class="field">
                                                                    <input type="email" name="email" id="email"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Email</span>
                                                                </div>
                                                                <div class="form-title">phone</div>
                                                                <div class="field">
                                                                    <input type="text" name="phone" id="phone"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                                </div>
                                                                <input type="hidden" name="recaptcha" id="recaptcha">
                                                                <input type="submit" value="Submit"
                                                                       class="send send--webDesign"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">tell us what you need</div>
                                                                <div class="field">
                                                                    <textarea name="message" placeholder="" id="message"
                                                                              class="message" required></textarea>
                                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                                    <span class="slick-tip left">Your message</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div><!--row-->
                        </div><!--container-->
                        {{--<img class="wid-100 hidden-xs" src="{{url('assets/images/capabilities-seo.jpg')}}" alt="L Garde Smart Space Technology Building">--}}
                        {{--<img class="wid-100 visible-xs" src="{{url('assets/images/mobile/seo-mobile.jpg')}}" alt="L Garde Smart Space Technology Building">--}}
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title right">SEM (SEO / PPC)</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">To keep your company at the top of organic searches, you need a powerful Search Engine Optimization (SEO) strategy. Relevant content. And you need to adhere to search engine guidelines. We work within best-practice Pay-Per-Click (PPC) methods that'll drive you up not only in the ranks, but also in the conversion rates.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection