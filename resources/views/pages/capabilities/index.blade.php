@extends('layouts.default')

@section('title')
    Full Service Branding Agency | Award Winning | Orange County, CA @endsection

@section('description')
    At W Brand Studio we refresh, reimagine, reposition, and create brands through any means possible. We are a full service branding agency in Orange County CA
@endsection

@section('keywords')
    branding agency, marketing agency, marketing agency orange county, branding agency orange county
@endsection

@section('abstract')
    At W Brand Studio we refresh, reimagine, reposition, and create brands through any means possible. We are a full service branding agency in Orange County CA
@endsection
@section('customHTMLClass')
    capabilities-page @endsection


@section('brandingHeader')
@section('brandTitle', 'Capabilities')
@include('partials.branding-header')
@endsection


@section('content')
    <div class="page page-capabilities-index">
        <section class="page-copy-section text-center pad-t-4 pad-b-1 tk-futura-pt opacity-0 wow fadeInUp" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">We're a full service branding agency - and more.</h1>
                        <div class="sm-underline">&nbsp;</div>
                        <p>At W Brand Studio we refresh, reimagine, reposition, and create brands through any means possible. We work strategically with our clients to help them define their brand vision, the brand promise, messaging, and brand positioning. Whether you need help with complete personal branding, business marketing, advertising, brand identity, brand design, website development, SEO/PPC, or interior and exterior brand design, we can help.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="cap-logo-section page-copy-section pad-b-2 tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/strategy-icon-branding.png')}}" alt="">
                                <h2>Strategy</h2>
                                <p>Naming<br>Branding<br>Positioning<br>Messaging</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/design-icon-branding.png')}}" alt="">
                                <h2>Creative</h2>
                                <p>Art Direction<br>Copywriting<br>Graphic Design<br>Interior & Exterior Design<br></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/execution-icon-capabilities.png')}}" alt="">
                                <h2>Execution</h2>
                                <p>Web<br>Mobile<br>Social<br>Print</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/marketing-icon-capabilities.png')}}" alt="">
                                <h2>Marketing</h2>
                                <p>Research<br>Digital (SEO/PPC)<br>Traditional<br>Advertising
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100  hidden-xs" src="{{url('assets/images/capabilities-branding.jpg')}}" alt="L Garde Smart Space Technology Building">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/lgarde-what-we-do.jpg')}}" alt="L Garde Smart Space Technology Building">
                    </div>
                    <div class="col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title left">Totally Branding</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">To do branding right, you need to start at the top. From there it should trickle down into the spirit of your employees, streamlining their uniforms, and embodying how they answer the phone. Your brand should brighten the interior and exterior of your building, transforming company cars into rolling billboards, while living online for the world to experience it. People will feel your brand come to life on business cards, letters, mailers, and packaging. Your brand should make them sit up and notice through print ads and commercials. Every single touch point is a new opportunity to build a connection with your customers. And that's what where here for, to help you strengthen that bond.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>

                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100  hidden-xs" src="{{url('assets/images/capabilities-Laguna-woods.jpg')}}" alt="L Garde Smart Space Technology Building">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/LWV-what-we-do.jpg')}}" alt="L Garde Smart Space Technology Building">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title right">Visual Identity</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">You don't get a second chance to make a first impression, so get it right from the get-go. And a clear, consistent visual identity is the key to maintaining a strong brand image. Your logo, typography, photographs, graphics, and colors all reinforce the image of your company. It should reflect your brand values, projecting a perception of quality, relevance, originality and impact. </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/capabilities-website.jpg')}}" alt="L Garde Smart Space Technology Building">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/sentry-mobile.jpg')}}" alt="L Garde Smart Space Technology Building">
                    </div>
                    <div class="col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title left">Website Development</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">Your website is probably the single most important tool in your branding arsenal. We're experts at creating unique user experiences and developing effective strategies for your online presence that'll work across all mobile, tablet, and desktop devices.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/capabilities-seo.jpg')}}" alt="L Garde Smart Space Technology Building">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/seo-mobile.jpg')}}" alt="L Garde Smart Space Technology Building">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text">
                        <article>
                            <h2 class="orange-title right">SEM (SEO / PPC)</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">To keep your company at the top of organic searches, you need a powerful Search Engine Optimization (SEO) strategy. Relevant content. And you need to adhere to search engine guidelines. We work within best-practice Pay-Per-Click (PPC) methods that'll drive you up not only in the ranks, but also in the conversion rates.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection