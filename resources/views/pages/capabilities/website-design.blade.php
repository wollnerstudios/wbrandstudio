@extends('layouts.default')

@section('title')
    Custom Website Development Services | Wollner Studios
@endsection

@section('description')
    Wollner studios offers professional website design services that are tailored to your business needs. Our team of experts will create a visually appealing and user-friendly website that will help you stand out in the digital world.
@endsection

@section('keywords')
    website design, Web apps, web development, website design orange county, website development orange county, web development, orange county
@endsection

@section('abstract')
    Looking for a website design that's effective & unique? Our award-winning website design and development team is here to help.
@endsection

@section('customHTMLClass')
    capabilities-page
@endsection

@section('brandingHeader')
@section('brandTitle', 'Web Design')

@section('brandHeaderImage','http://wollnerstudios.wbrandstudio.com/assets/images/capabilities/')
@include('partials.branding-header-capabilities')
@endsection


@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }
            }
        });
    });
    </script>
    <div id="web-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background-color: #efefef; border-radius: 0;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;
    right: -25px;
    top: -25px;
    background: black;
    color: white;
    opacity: 1;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body">
                    <form action="/website-contact-submit" method="post" style="margin-bottom: 50px;">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="company" placeholder="Company">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="price" required>
                                <option>My budget is:</option>
                                <option>$5,000 - $10,000</option>
                                <option>$10,001 - $20,000</option>
                                <option>$20,001 - $30,000</option>
                                <option>$30,001 - $40,000</option>
                                <option>$40,001 - $50,000</option>
                                <option>$50,000 +</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>I am looking for:</label>
                            <br>
                            <div class="row" style="display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;">
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">
                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input type="checkbox" value="branding">Branding</label>
                                    <label class="checkbox-inline w-20p"><input name="service1" type="checkbox" value="visual ID">Visual Identity</label>
                                    <label class="checkbox-inline w-20p"><input name="service2" type="checkbox" value="logo design">Logo Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service3" type="checkbox" value="web dev">Website Development</label>
                                    <label class="checkbox-inline w-20p"><input name="service4" type="checkbox" value="social media management">Social Media Management</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p"  style="margin-left: 10px;"><input name="service5" type="checkbox" value="seo">SEO</label>
                                    <label class="checkbox-inline w-20p"><input name="service6" type="checkbox" value="ppc">PPC</label>
                                    <label class="checkbox-inline w-20p"><input name="service7" type="checkbox" value="video prod">Video/Commercial Production</label>
                                    <label class="checkbox-inline w-20p" ><input name="service8" type="checkbox" value="photography">Photography</label>
                                </div>
                                <div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">

                                    <label class="checkbox-inline w-20p" style="margin-left: 10px;"><input name="service9" type="checkbox" value="int/ext design">Interior/Exterior Design</label>
                                    <label class="checkbox-inline w-20p"><input name="service10" type="checkbox" value="print">Print Design (i.e. brochure)</label>
                                    <label class="checkbox-inline w-20p"><input name="service11" type="checkbox" value="Naming">Naming</label>
                                    <label class="checkbox-inline w-20p"><input name="service12" type="checkbox" value="marketing">Marketing</label>
                                </div>


                            </div>

                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="comments" placeholder="Comments:">
                        </div>
                        <input type="hidden" name="recaptcha" class="recaptcha">
                        <input type="hidden" name="page" value="website-design">
                        <button type="submit" class="" style="margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;    margin-left: auto;
    position: absolute;
    bottom: 0;
    right: 0;
    background: orange;
    color: white;
    padding: 10px;
    font-weight: bold;">SUBMIT</button>
                    </form>

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="page page-capabilities-website-design">
        <img class="wid-100 hidden-xs" src="/images/web.jpg" alt="website design">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            TAILORED WEBSITE DEVELOPMENT: CUSTOM-BUILT SOLUTIONS FOR YOUR BUSINESS
                        </h1><div class="sm-underline">&nbsp;</div>


                        <p><br>
                            Custom website development based in branding is a highly effective way to make your website stand out from the crowd. By leveraging custom photography and unique content development based on your messaging and strategy, you can create a website that truly reflects your brand identity and resonates with your target audience.
                        </p>
                        <BR>
                        <p>
                            One of the key benefits of custom website development is the ability to create a unique visual identity that sets you apart from your competitors. By using custom photography and imagery, you can create a website that is both visually stunning and highly memorable. This can help to establish a strong brand identity and increase brand recognition over time.
                        </p>
                        <BR>
                        <p>
                            Our sites come with a custom CMS which allows us to tailor your website's backend to your specific needs, without being bogged down by unnecessary features and functionalities that you don't need. This can lead to a faster, more efficient website that is easier to manage and update. By focusing on your specific requirements, a custom CMS can help to streamline your website management processes, reduce costs, and increase overall website performance.
                        </p>
                        <br><a href="/contact" class="myButton org-btn">CONTACT US</a><br><br>

                    </div>
                </div>
            </div>
        </section>
        <section class="pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" style="background:#BFA179;font-size: 18px;">
            <div class="container text-white text-center">
                <i>Websites follow certain patterns and trends from year to year. What was hot yesterday may be passé today. Some companies not only keep up with the evolution of website design but set the standard. WollnerStudios’ client list includes a who's who of the Fortune 500. WollnerStudios’ website design for RENEW is simple yet elegant. The copy is sparse but meaningful without all the showy verbiage. And, if you are looking for all the menu buttons in all the usual places, you won't find them here. The navigation is novel.” </i>               <br><br>

                Hermes Creative Awards<br>
                Association of Marketing and Communication Professionals.
            </div>
        </section>
        <section class="page-copy-section text-center pad-t-4 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-bronze" style="font-size: 3.6rem">
                            BUILDING FOR THE FUTURE: CUSTOM SITE DEVELOPMENT WITH SEO IN MIND                        </h2>
                        <div class="sm-underline">&nbsp;</div>
                        <p>By prioritizing fast loading speeds, streamlined code, and optimized content, custom websites can achieve higher search engine rankings and increased visibility. This is achieved through careful attention to website structure, metadata, and content optimization, as well as advanced technical optimization techniques such as caching, compression, and minification. By prioritizing both user experience and search engine optimization, custom websites can achieve high levels of performance, leading to increased engagement, higher conversion rates, and ultimately, greater business success. Overall, custom website development that is optimized for search engines can be a highly effective way to establish a strong online presence and gain a competitive edge in your industry</p>
                        <br>
                        <p style="font-weight: bold;">
                            Are you tired of compromising your brand to boost your SEO? Look no further. At WollnerStudios, we prioritize both the integrity of your brand and driving relevant traffic to your site. With a proven track record and a focus on quality service, we only take on a select few clients to ensure their success. Don't settle for subpar SEO tactics that harm your brand image. Choose a branding agency for SEO that elevates your brand and drives results. Contact us today to learn more.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        {{--<section class="image-cap-section wow fadeInUp opacity-0">--}}
        {{--<div class="container-fluid ">--}}
        {{--<div class="row">--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Renew.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_CHMG_Website_430x305_Mock.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Shotcrete.gif')}}" alt="">--}}
        {{--</div>--}}
        {{--<div class="ar-img-small">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/prr-desktop-and-mobile-.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-sm-4 padding-l-0 padding-r-0">--}}
        {{--<div class="ar-img">--}}
        {{--<img class="width-100" src="{{url('/assets/images/capabilities/_Edmon_Website_Mock.jpg')}}" alt="">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</section>--}}


        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/baci-website.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/marriott-web-1.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/marriott-web-2.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/sentry-web-1.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection