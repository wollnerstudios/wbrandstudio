@extends('layouts.default')

@section('title')
    Visual Identity Design | WollnerStudios
@endsection

@section('description')
    WollnerStudios is a top-rated design agency that specializes in visual identity design, including logos, typography, and color schemes
@endsection

@section('keywords')
    branding agency, visual identity, marketing agency
@endsection

@section('abstract')
    Your visual identity is the foundation for which a broader brand experience is built, put our experience to work for your brand today.
@endsection

@section('customHTMLClass')
capabilities-page
@endsection

@section('brandingHeader')
    @section('brandTitle', 'Visual Identity')

    @section('brandHeaderImage','http://wollnerstudios.wbrandstudio.com/assets/images/')
    @include('partials.branding-header-capabilities')
@endsection

@section('content')
    <div class="page page-capabilities-visual-identity">
        <img class="wid-100 hidden-xs" src="/images/Visual-ideneity-wollnerstudios.jpg" alt="visual identity">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            VISUAL IDENTITY: A KEY COMPONENT OF EFFECTIVE BRANDING
                        </h1>
                        <div class="sm-underline">&nbsp;</div>

                        <p>
                            WollnerStudios is a leading design agency that specializes in creating strong visual identities for brands. With a team of experienced designers, we have helped numerous businesses to stand out in their respective industries through effective visual branding. We work closely with our clients to understand their values, goals, and messaging, and use this information to develop a unique and cohesive visual identity that accurately represents their brand.
                        </p>

                        <br>

                        <p>
                            From logo design to color palette selection, typography to imagery, WollnerStudios’ expertise in visual identity design ensures that our clients' brands are well-positioned for success in today's competitive marketplace. Our track record of success and their commitment to delivering exceptional design solutions make us a top choice for businesses seeking to enhance their visual branding.
                        </p>
                        <br>
                        <p>
                            <a href="/contact" class="myButton org-btn">CONTACT US</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" style="background:#BFA179;font-size: 18px;">
            <div class="container text-white text-center">
                “Wollner also re-designed our logo and developed new branding – these were critical as we have been growing and acquiring new companies. WollnerStudios bears my highest recommendation."
                <br><br>
                Kris Zacny, Ph. D.<br>
                VP, exploration technologies, HONEYBEE ROBOTICS
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-bronze" style="font-size: 3.6rem">
                            MAXIMIZE YOUR BRAND’S POTENTIAL WITH A STRONG VISUAL IDENTITY
                        </h2>
                         <div class="sm-underline">&nbsp;</div>
                        <p>No matter what type of business you have, you will benefit from creating a powerful visual identity. W Brand Studio can take you through the entire process from conception to implementation and evaluation. We’ll create a consistent look and feel, including establishing your brand color palette, corporate fonts, and image application treatments. Your visual identity is the foundation for which a broader brand experience is built.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/logo-design-icon-visual-identity.png')}}" alt="">
                                <h2>Naming & Logo Design</h2>
                                <p>
                                    We will create a unique and memorable brand name that reflects the brand’s personality and values. Once chosen, we will create a logo that visually represents the brand.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/images/brand-guidelines.png')}}" alt="">
                                <h2>Brand Guidelines</h2>
                                <p>We will develop a set of guidelines that outline how the visual identity elements should be used across various media and platforms.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/brochure-icon-branding.png')}}" alt="">
                                <h2>Brand Collateral</h2>
                                <p>We will design and produce branded materials such as business cards, brochures, and packaging.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/images/images-tone-of-voice.png')}}" alt="">
                                <h2>Imagery & Voice</h2>
                                <p>We will select or create images that reinforce the brand's message and evoke the desired emotions, as well as establishing a consistent tone and voice for the brand's communications.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/images/prr.jpeg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/images/Edmon-visual-identity-web.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/images/PRR-visual-identity-web.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/images/edmon.jpeg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection