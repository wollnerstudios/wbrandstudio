@extends('layouts.default')

@section('title')
    Award Winning Branding & Marketing Agency | Orange County, CA
@endsection

@section('description')
    We are an innovative & creative digital branding agency in Orange County, CA.  A brand agency with insight, experience, and energy.
@endsection

@section('keywords')
    branding agency, brand agency orange county
@endsection

@section('abstract')
    We are an innovative & creative digital branding agency in Orange County, CA.  A brand agency with insight, experience, and energy.
@endsection

@section('brandingHeader')
@section('brandTitle', 'BRAND NAMING')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-capabilities-brand-naming">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                         <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            What you may need the most is nomenclature.
                        </h1>
                        <div class="sm-underline">&nbsp;</div>

                        <p>
                            What's in a name? Well, quite frankly everything. Nobody remembers A-1 Dry Cleaning or Joe's Pizza. But when look at some of the biggest brands; Apple, Google, Facebook, and Microsoft, they all have named their brand something totally unique. Ownable. Memorable. And each has it's own story behind the name. That's how a successful brand is built.
                        </p>

                        <p>
                            W Brand Studio has developed distinctive nomenclature for boutique hotels, magazines, fashion brands, consumer products, and IT deployment agencies. Due to confidentiality agreements, we cannot list them online, but we'd be happy to tell you about them in person.
                        </p>

                        <p>
                            We're experts at brand development strategy, so the first item on our agenda is to take a close look at your name. What does it mean? Does it have a story to back it up? Is it unique? Whether you need a new name, a name audit, or a name refresh, we can help. By combining our knowledge of languages and linguistics we can create a name that will be chosen based on a sound strategy.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/naming.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray tk-futura-pt">
                                            <p class="text-left bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                            </p>

                                            <p>
                                                Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.
                                            </p>

                                            <p>
                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>

                                            <h3>
                                                <u>
                                                    <a href="/contact">Ready to get started? Let's talk.</a>
                                                </u>
                                            </h3>

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/naming.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <h2>
                                            <strong>We practice what we preach. Walk the walk and talk the talk. </strong>
                                        </h2>

                                        <p>
                                            Since 1997, we've made a living out of giving compelling life to the most celebrated companies, or even the most mundane products. We're a brand agency with insight, experience, and energy. Our offices are located in Newport Beach, California, one of the most creative landscapes in all of Orange County, it's where we provide our clients with essential services:
                                        </p>

                                        <ul>
                                            <li>Branding Design</li>
                                            <li>Brand Advertising</li>
                                            <li>Marketing and Branding</li>
                                            <li>Visual Identity</li>
                                            <li>Graphic Design (Magazines, Brochures, Annual Reports)</li>
                                            <li>Interior & Exterior Design</li>
                                            <li>Website Development</li>
                                            <li>Search Engine Optimization (SEO)</li>
                                            <li>Pay-Per-Click Strategy & Management (PPC)</li>
                                            <li>Social Media Strategy & Maintenance
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection