@extends('layouts.default')

@section('title')
    Social Media Marketing & Services | Orange County | Los Angeles
@endsection

@section('description')
    We provide expert advice and Social Media Services for those who do not have the time nor the resources to maintain their social media or blog updates.
@endsection

@section('keywords')
social media marketing, marketing orange county, marketing los angeles @endsection

@section('abstract')
    We provide expert advice and Social Media Services for those who do not have the time nor the resources to maintain their social media or blog updates.
@endsection

@section('customHTMLClass')
    capabilities-page
@endsection

@section('brandingHeader')
@section('brandTitle', 'SOCIAL MEDIA')

@section('brandHeaderImage',' ')
@include('partials.branding-header-capabilities')
@endsection


@section('content')
    <div class="page page-capabilities-social-media">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            Engagement is essential.
                        </h1>
                        <div class="sm-underline">&nbsp;</div>
                        <p>
                            If social media isn’t part of your brand strategy, you don’t have a brand strategy. We’ll keep you on top of your social game with a strategy and content calendar designed to reach, engage, and grow your target audience with a strong, consistent presence. And our tracking and management systems will make sure it’s all resonating.
                        </p>

                        <!--<br>

                        <p><a href="/contact">Like? Call us and we'll talk about your strategy today.</a></p>-->
                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/social-media-L.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/social-media-top.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/social-media-bottom.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/social-media-R.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-copy-section text-center pad-t-4 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-bronze" style="font-size: 3.6rem">We’re the best at best social practices.</h2>
                        <div class="sm-underline">&nbsp;</div>
                        <p>Like every part of your brand, your social media should be you – genuine, truthful, and interested in your followers. Leave the advertising for actual ads (we do those too, btw).
                        </p>
                        <!--<p>
                            <strong style="color: #0ebed8"><a href="../contact">+ Add friend. Then us call to discuss in detail. </a></strong>
                        </p>-->
                    </div>
                </div>
            </div>
        </section>
        <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keywords.png')}}" alt="">
                                <h2>Social Strategy</h2>
                                <p>Before you start liking, DMing, and posting, let’s make a plan. We’ll audit your current accounts, define their purpose, goals, and target audience, then land on the best sites and schedule for your brand.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keyword-search.png')}}" alt="">
                                <h2>Content Calendar</h2>
                                <p>Once your strategy’s in place, we'll create a schedule of major events and regular posts that fit your brand's personality.  </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/conversion-icon.png')}}" alt="">
                                <h2>Lead Generation</h2>
                                <p>We use tools like gated-content landing pages, lead generation cards and engagement ads to generate and track social media leads.  </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/seo-icon.png')}}" alt="">
                                <h2>Reporting</h2>
                                <p>We use Google Analytics as well as individual social platform campaign tracking to determine which ad content is the most effective. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="cap-logo-section page-copy-section mb-4 tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <strong>Like, friend, DM – or hey, just call us – so we can talk social strategy today. (657) 232-0110</strong>
                <br><BR><a href="/contact" class="myButton org-btn">CONTACT US</a><br><br>
            </div>
        </section>

        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/montagna-branding.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/people-branding.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/room-branding.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/sky-branding.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection