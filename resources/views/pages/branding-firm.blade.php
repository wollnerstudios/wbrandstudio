@extends('layouts.default')

@section('title')
    Brand Development | Branding Pros | Branding Firm | @endsection

@section('description')
    We are an innovative brand firm in Orange County, CA.  Marketing and Branding Agency in Newport Beach, CA. @endsection

@section('keywords')
    branding, graphic design, logo design @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'BRANDING FIRM')
@include('partials.branding-header')
@endsection



@section('content')
    <div class="page page-branding-firm">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            We're One of the Top Branding Firms in Orange County.
                        </h1>
                        <p>We have successfully told our client's stories in simple, repeatable and memorable ways with unique design for over 20 years. Simplicity is the secret to a successful brand, and to deliver top-notch brand experiences. With the right brand strategy, brand positioning, brand messaging and brand promise, we can make you stand out in the crowd and drive business results. We can help you with brand strategy, brand positioning, brand promises, brand messaging, visual identity and more. We are a one-stop creative branding agency located in Newport Beach with a distinct Scandinavia flair from our Norwegian founder.
                            <br><br>
                            <a href="http://www.wbrandstudio.com/contact"><img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;"></a>

                            <br><br><img class="wid-100 hidden-xs" src="{{url('/assets/images/branding-logo-design-website.jpg')}}" alt="Branding for Menu"><br><br>

                            <small>
                                <em>"Dear Michael, we would like to thank you for the excellent work you have done creating our company's brand, our new website, logo, and improving our overall corporate image. We recognized that we were in very capable hands the first time we met with you. You and your team made a considerable effort to understand all aspects of our business upfront, and combined with your understanding of our needs, you have produced an image that fits our company's personality."</em>
                                <br><Strong>Tim Flanagan, Sentry Control Systems - SKIDATA.</strong></small>
                            <br><br>Scroll down for a listing of our services and more.
                        </p>
                        <h2>Call us today at (657) 232-0110.</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/about-office.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>We practice what we preach. We walk the walk, not just talk the talk. </strong>
                                        </h2>
                                        <p class="text-left">We're one of the fastest growing branding agencies in Orange County, with insight, experience and energy. Our offices are located in Newport Beach, it's where we provide our clients with these essential services:
                                            <br><br>
                                            - Brand Audit<br>
                                            - Brand Strategy<br>
                                            - Brand Positioning and Promise<br>
                                            - Brand Voice<br>
                                            - Brand Implementation, incl. Interior Design<br>
                                            - Naming<br>
                                            - Visual Identity<br>
                                            - Logo Development<br>
                                            - Website Development<br>
                                            - Online Marketing incl. PPC, SEO and Social Media<br>
                                        </p>


                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>


        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Here's why you should choose us.
                        </h2>
                        <p>We are a passionate and dedicated group of professional madmen, celebrating 20 years in business in 2017. We are uniquely interested in listening to our clients so we can really get to know them before we create a single dot. We are proactive, accessible, collaborative and will always make sure you are happy with the results. If you are looking for someone who can do it all in-house, then you've come to right place. We are a full-service agency and can help you with everything from total branding to online marketing (SEM) and website development, to your logo re-design, visual identity and advertising needs.
                            <br><br><em>"W Brand Studio embraced the difficult challenge of re-branding a 40-year-old "active adult" community with over 18,000 residents, and produced a product that is fresh, current, attractive and appropriate for the client. The new Laguna Woods Village logo, an elegant stylized green leaf, reflects the vitality and strength of the community. The W Brand Studio team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection, and the presentation was outstanding. Thank you, W Brand Studio!"</em>
                            Wendy Bucknum, CMCA, AMS, CACM.
                            <br><br>Government & Public Affairs Manager. Professional Community Management, Inc. - Agent for Laguna Woods Village<br><br><a href="testimonials">Click here to read more testimonials, or simply click below for a no-obligation quote.</a>
                            <br><br><a href="http://www.wbrandstudio.com/contact"><img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;"></a>


                        </p>
                    </div>
                </div>
            </div>
        </section>


        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/work-sq.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">How do you brand yourself? </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">The core essence of a brand starts from within. It's a vision from the top management that needs to flow all the way down to the person that sits at the front desk. Your visual identity, the interior and exterior of your building, and all communication, whether online or offline, need to reflect that vision. Allow W Brand Studio to unlock the true power, full potential, and exponential value of your company branding.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection