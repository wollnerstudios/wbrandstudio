@extends('layouts.default')

@section('title')
    Search Engine Optimization Services | Expert Orange County SEO Companies
@endsection

@section('description')
    Generate more visibility with the right search engine optimization strategy. Work with a local SEO company in Orange County Ca and see the difference.
@endsection

@section('keywords')
    seo, sem, marketing
@endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company
@endsection

@section('brandingHeader')
@section('brandTitle', 'SEO SOLUTIONS')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-online-marketing-agency-orange-county">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h1 class="text-center">Search Engine Experts</h1>

                        <p style="margin-bottom: 1rem;">If you want to grow your business, getting more clients is key. No other strategy can get your brand in front of new prospects like organic search engine optimization.</p>

                        <p style="margin-bottom: 1rem;">Also known as “SEO”, this strategy helps optimize your website content so that you show up in Google and other search engines when interested prospects look online. When done right, this is a very effective marketing method that, can drive more leads than ever before.</p>

                        <p style="margin-bottom: 1rem;">When you're ready to partner with an expert Orange County search engine optimization team, we’re here to help.</p>

                        <h2 class="text-center">Call (657) 232-0110 today!</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6  display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/offering-expert-seo-service-in-orange-county-ca.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/offering-expert-seo-service-in-orange-county-ca.jpg')}}" alt="Branding for Menu">
                    </div>
                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Search Engine Strategy</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>As a marketing and branding organization, our goal is to help your brand from a holistic approach. This means that we work with you to research the exact keywords that should be focused our SEO services on.</p>
                                        <br>
                                        <p>We have a wide range of tools and techniques to help us stay laser targeted on your best prospects searches. As an experienced Orange County SEO company, we take great pride is providing clients with a roadmap to online marketing success.</p>
                                        <br>
                                        <p>Not only are there specific attributes that help craft an optimized page, once visitors view the page you need to have compelling content and call to actions in order to get them taking that next step.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/our-seo-company-drives-results-for-clients.jpg')}}" alt="Orange County Lead Generation">

                        <img class="wid-100 visible-xs" src="{{url('assets/images/our-seo-company-drives-results-for-clients.jpg')}}" alt="Orange County Lead Generation">

                        <div class="subtext-img-cap hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title landing-orange-titles-margin">Your Local SEO Partner</h2>

                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>From website design, graphics and content writers on our team, we have all the ingredients to help your SEO and marketing initiatives thrive like never before. Keep in mind that the right SEO services should include both strategy and hands on implementation in order to achieve the very best results.</p>
                                        <br>
                                        <p>Although our office is located in beautiful Orange County California, we service a wide range of clients throughout the United States, and even some key customers located overseas. Don’t allow your marketing to be limited by physical location, your prospects are counting on you.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left landing-orange-titles-margin">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray text-center tk-futura-pt">
                                            <p class="text-left bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                                <br><br>Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.

                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/partner-with-our-orange-county-team.jpg')}}" alt="Online Marketing Experts">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/partner-with-our-orange-county-team.jpg')}}" alt="Online Marketing Experts">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Ready To Talk?</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>When you’re ready to put an effective strategy together with a leading SEO services company, please <a href="/contact">contact our team</a>. We look forward to learning more about your needs and sharing how we’ve already helped others.</p>
                                    </div>

                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <div align="center">
            <img class="img-responsive" src="{{url('/assets/images/our-search-engine-optimization-clients-get-results.jpg')}}" alt="" style="PADDING-TOP: 20px; PADDING-BOTTOM: 20px;">
        </div>
    </div>
@endsection
