@extends('layouts.default')

@section('title')
    Award Winning Web Designs | Serving Orange County & Los Angeles
@endsection

@section('description')
    We are an innovative and creative branding and marketing agency in Orange County with insight, experience and energy. Unique web designs since 1997.
@endsection

@section('keywords')
    web design, web design orange county, web design los angeles
@endsection

@section('abstract')
    We are an innovative and creative branding and marketing agency in Orange County with insight, experience and energy. Unique web designs since 1997.
@endsection

@section ('subject')
    We are an innovative and creative branding and marketing agency in Orange County with insight, experience and energy. Unique web designs since 1997.
@endsection

@section('brandingHeader')
@section('brandTitle', 'Award Winning Website Development')
@include('partials.branding-header')
@endsection



@section('content')
<img class="wid-100 hidden-xs" src="{{url('storage/wb_images/awards-wbrand-success.jpg')}}" alt="wbrand website awards">

<p>
<div class="page page-capabilities-website-design">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Websites built for visibility with SEO and PPC in mind.
                        </h1>

                        <p><br>
                            Unlike most web developers, we take everything into consideration when we build websites. Every site is built to accomodate advanced SEO tactics, simple but effective navigation and effective landing pages for PPC (pay-per-click advertising). We always start with a strategic look at who you are, what your goals are, your messaging, the site structure and navigation. Most companies will buy a template and force you into something that really does not fit your company. It may look nice, but it is fundamentally wrong from a strategic and search perspective. You might as well throw the money out the window. Our clients really get a return on their investment with our methodology and creative twist. No wonder many have been with us for up to 17 years. </strong><br><br>
                            
                        </p>     <p>

                            <br><a href="/contact" class="myButton org-btn">LET'S TALK</a><br><br>
                        </p>
                        <h3>What's in your future? Call us at (657) 232-0110 or send us an email. We will blow you away with what we can do.
                        </h3>

                   


                    </div>
                </div>
            </div>
        </section>
        
    <div class="page page-branding-agency">
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-flex display-t-r-sm">
                    <div class="col-sm-6 display-flex display-t-c-sm pad-l-0 pad-r-0 wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        {{--added new image here--}}
                        {{--<img class="wid-100 pos-abs hidden-xs" src="{{url('/assets/images/web-banner-2.jpg')}}" alt="Branding for Menu">--}}
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/web-banner-2.jpg')}}" alt="Branding for Menu">

                        <div class="wid-100 hei-100 hidden-xs pos-abs collageImage--webDesign"></div>
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                             <div class="row">
                                <div class="col-xs-12 col-md-12 col-lg-12 orange-text-container copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>
                                            Custom Web Development with Technical Knowhow, Marketing Expertise and Award Winning Design.</strong>
                                        </h2>
                                        <p class="text-left">We are located in Newport Beach where we provide our clients with these essential services:
                                        </p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul>
                                                    <li>Front-end Design (UX/UI)</li>
                                                    <li>Advanced Database Development</li>
                                                    <li>Easy to Use Content Management Systems (CMS)</li>
                                                    <li>Custom & Secure Website Development</li>
                                                    <li>Mobile & Tablet Friendly Design</li>
                                                    <li>E-commerce Solutions</li>
                                                </ul>
                                           
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <div class="contact-form-container">
                            {{--<div class="contact-form-close-button contact-form-close-button--webDesign">--}}

                                {{--<div>x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-xs-12 py-5">
                                    <h2 class="text-center">Contact Us</h2>
                                    <div class="contact-block text-center">
                                        <a href="/contact">
                                            <i class="fa fa-envelope fa-4x" style="font-size: 4em !important;"></i>

                                        </a>
                                    </div>
                                    {{--<section id="slick">--}}
                                        {{--<div class="feedback-form">--}}
                                            {{--<form action="/contact" method="post">--}}
                                                {{--{{ csrf_field() }}--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div class="row">--}}
                                                            {{--<div class="col-sm-10 col-sm-offset-1 ">--}}
                                                                {{--<div class="form-title">name</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<input type="text" name="name" id="name" required/>--}}
                                                                    {{--<!-- <span class="entypo-user icon"></span> -->--}}
                                                                    {{--<span class="slick-tip left">Enter your name</span>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="form-title">company</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<input type="text" name="company" id="company" required/>--}}
                                                                    {{--<!-- <span class="entypo-user icon"></span> -->--}}
                                                                    {{--<span class="slick-tip left">Enter your Company</span>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="form-title">email</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<input type="email" name="email" id="email" required/>--}}
                                                                    {{--<!-- <span class="entypo-user icon"></span> -->--}}
                                                                    {{--<span class="slick-tip left">Enter your Email</span>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="form-title">phone</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<input type="text" name="phone" id="phone" required/>--}}
                                                                    {{--<!-- <span class="entypo-user icon"></span> -->--}}
                                                                    {{--<span class="slick-tip left">Enter your Phone Number</span>--}}
                                                                {{--</div>--}}
                                                                {{--<input type="submit" value="Submit" class="send send--webDesign"/>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-sm-6">--}}
                                                        {{--<div class="row">--}}
                                                            {{--<div class="col-sm-10 col-sm-offset-1 ">--}}
                                                                {{--<div class="form-title">tell us what you need</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<textarea name="message" placeholder="" id="message" class="message" required></textarea>--}}
                                                                    {{--<!-- <span class="entypo-comment icon"></span> -->--}}
                                                                    {{--<span class="slick-tip left">Your message</span>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="field">--}}
                                                                    {{--<div class="g-recaptcha" data-sitekey="6Ld1tDQUAAAAAOWIoCiML4SjfxaxI6DpGG-92VVe"></div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</section>--}}
                                </div>
                            </div><!--row-->
                        </div><!--container-->
                    </div>
                </div>
            </div>
        </section>
 
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Here's why you should choose us.
                        </h2>
                        <p>We are more than a web developer. We are marketing and branding experts. It means that our clients are getting more than a web site, they are getting results. Our websites have wowed our peers and award show judges from all over the world. Just in the last 18 months, we have received over 50 major awards for our website development, design, marketing and branding projects. We are proactive, collaborative and will always make sure you are happy with the results. If you are looking for someone who can make you stand out in the crowd, make your site engaging and produce results, and can do it all in-house, then you've come to right place. We are local and accessible.
                            <br><br><strong>"What was hot yesterday may be pass&eacute; today. 
Some companies like W Brand Studio not only keep up with the evolution of website design but set the standard."</strong>


<br><br>
                            Association of Marketing and Communication Professionals - 2017
                            



                        </p>
                    </div>
                </div>
            </div>
        </section>



    </div>
@endsection