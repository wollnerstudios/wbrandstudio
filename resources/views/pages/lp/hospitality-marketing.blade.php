@extends('layouts.default')

@section('title')
    Put more heads in beds with smart hospitality marketing. @endsection

@section('description')
    We are an innovative and creative digital branding agency in Newport Beach, CA.  The agency for total branding and design services to companies all over the U.S. @endsection

@section('keywords')
    branding, branding agency, marketing agency @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('brandingHeader')
@section('brandTitle', 'HOSPITALITY MARKETING')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-hospitality-marketing">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Get in bed with us to raise occupancy rates.
                        </h1>
                        <p>The key to hotel marketing is having a website that truly reflects your brand with engaging content and ownable SEO keywords. When you have room for growth, let W Brand Studio create a business strategy that'll open the doors to new audiences like millennials. We'll rebrand your website with eye-catching visuals and captivating content that resonates to create unique user-experiences. It's extremely important that site visitors understand exactly what your hotel stands for, what guests can expect while staying the night, and what you represent as a company. </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray text-left tk-futura-pt">
                                            <p class="bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                            </p>
                                            <p>Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.

                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/about-office.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-left tk-futura-pt">
                                        <h2>
                                            <strong>We practice what we preach. Walk the walk and talk the talk. </strong>
                                        </h2>
                                        <p>
                                            Since 1997, we've made a living out of giving compelling life to the most celebrated companies, or even the most mundane products. We're a brand agency with insight, experience, and energy. Our offices are located in Newport Beach, California, one of the most the most creative landscapes in all of Orange County, it's where we provide our clients with essential services:
                                        </p>
                                        <ul>
                                            <li>Branding Design</li>
                                            <li>Brand Advertising</li>
                                            <li>Marketing and Branding</li>
                                            <li>Visual Identity</li>
                                            <li>Graphic Design (Magazines, Brochures, Annual Reports)</li>
                                            <li>Interior & Exterior Design</li>
                                            <li>Website Development</li>
                                            <li>Search Engine Optimization (SEO)</li>
                                            <li>Pay-Per-Click Strategy & Management (PPC)</li>
                                            <li>Social Media Strategy & Maintenance</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <p>
                            <em>
                                "Wollner and his team were able to take a struggling product, and within a very short period of time analyze and create a program that truly sent the message to our customers we wanted, but managed to boost the spirit of the entire team in the process. We saw not only direct increases in trackable profits, but were recognized by our competitive set for capitalizing on our uniqueness that Wollner identified for our resort. Thank you for your energy, efforts and results!"
                            </em>
                        </p>

                        <p>Steve Tremewan, General Manager, CalNeva Resort Lake Tahoe</p>

                        <h3><u><a href="/contact">Request a meeting in person or via Skype.</a></u></h3>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/apollo.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">TRENDS</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-left tk-futura-pt">
                                        <p>
                                            Mobile trend is soaring. 50% of mobile travelers book last minute stays. Are you mobile friendly? Mobile booking is extremely important to your continued growth and success.
                                        </p>
                                        <p>
                                            Rebranding. 2014 and 2015 may well go down as the years of rebranding. This is in response to the intense competition and the need to cater to a new audience: the millennials or generation Y. Let us dress you up for a new generation of guests in 2016.
                                        </p>
                                        <p>
                                            <strong>Ring our bell for customer service at <br>714-754-5475</strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection