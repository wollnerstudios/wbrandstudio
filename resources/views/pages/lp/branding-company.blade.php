@extends('layouts.default')

@section('title')
    Top Tier Branding Company | Orange County, CA 
@endsection

@section('description')
    We are a one-stop full-service branding agency in Orange County. Irvine, Newport Beach, Costa Mesa. Trusted for over 22 years by long-term clients.
@endsection

@section('keywords')
    branding agency, branding company orange county, orange county branding agency, branding firm
@endsection

@section('abstract')
    We are a one-stop creative branding agency in Orange County with unique designs. Trusted for over 22 years by long-term clients.
@endsection

@section ('subject')
    We are a one-stop creative branding agency in Orange County with unique designs. Trusted for over 22 years by long-term clients.
@endsection

@section('brandingHeader')
@section('brandTitle', 'BRANDING COMPANY')
@include('partials.branding-header')
@endsection






@section('content')

    @include('partials.branding-modal')


@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }

            }
        });
    });
    </script>
    <img class="wid-100 hidden-xs" src="storage/wb_images/branding-visual-identity_2.jpg" alt="branding and marketing">
    <br><br>
    <div class="page page-branding-agency">
        <section class="page-copy-section text-center pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="paddedtop">
                            A Real Branding Company Covering All Touchpoints.</h1><br>
                        <p> Many agencies say they are a "branding company", when they in reality are marketing companies, web developers, freelancers or advertising agencies. We are a 22 year old pure branding agency at the core. 

Our clients are more famous than we are, but our people and experience come from the largest agencies on the planet. We have worked with Fortune 500 Companies and startups, domestically and internationally. 



 <br><br>We believe ideas can come from anywhere, so we are not focused on a specific industry. Our niche is branding. With a diverse background and expertise from hundreds of different companies and industries, we have more to offer than most agencies. As a porfessional Branding Agency, we know how to make you stand out from the competition. We have won some of the top awards in the world from Cannes, France to Los Angeles, so there is no lack of creativity or accolades from our peers in the industry.

<br><br>Why W Brand? You will work with the best. We have no big overheads, so we are very price competitive, and when it comes to creativity, we are second to none. When you work with W Brand, you always work with an A-team, not some junior first-timers because you are not important enough. Every client we take on is treated as if they are our most important client. Being a "Total Brand Agency" means we look at your company as a whole from your logo, website, printed materials and marketing materials, to interior design, uniforms and vehicle wraps. 
</p><br> 

 <img class="wid-100 hidden-xs" src="storage/wb_images/clients-logos.jpg" alt="branding and viual identity"><br><br>
                        <p><br><br><a href="/contact" class="myButton org-btn">Free Consultation <br><small>Contact us today</small></a><br><br>
                        </p><br><br>
                           <p class="text-left"> <strong>If you are serious about your brand, these are the steps we follow:</strong><br><br>

1. Research. Before we start, we'd love to get to know you, your goals and your industry.<br>
2. Secondly,we will take a good look at your competitors<br>
3. We perform a brand audit to see how your brand is perceived internally and in the marketplace.<br>
4. SWOT Analysis<br>
5. Interview management, staff and customers (current and past)<br>
6. Our strategists look for trends in the marketplace<br>
7. A Brand Alignment Strategy is developed based on the Brand Audit, interviews and the SWOT analysis.<br>
8. We will develop a brand strategy, which include positioning in the marketplace, messaging, brand architecture, brand promise, elevate brand storytelling and experiences, staying consistent through touchpoints, employee engagement, and more.<br>
9. Create a Visual Identity or Brand Identity, including Logo design, Corporate colors, Corporate fonts, Tone-of-voice, and Imaging<br>
10. Implementation of the Brand Identity, Interior design, Exterior design, Uniforms and vehicles, Stationary, Sales Sheets, Presentation Materials, and Brochures<br>

</p>
                           <br><br><a data-toggle="modal" href="#web-modal" class="myButton org-btn">Let Us Amaze You <br><small>Call today</small></a><br><br><br>
                      
                        <img class="wid-100 hidden-xs" src="storage/wb_images/branding-visual-identity_1.jpg" alt="branding and marketing"><br><br>

                       


                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-flex display-t-r-sm">
                    <div class="col-sm-6 display-flex display-t-c-sm pad-l-0 pad-r-0 wow rotateInUpRight fadeIn"
                         data-wow-offset="100" data-wow-delay="100ms">
                        {{--added new image here--}}
                        <img class="wid-100 visible-xs" src=Z"storage/wb_images/branding_banner_1-new.jpg"
                             alt="Visual Identity">
                        <div class="wid-100 hei-100 hidden-xs pos-abs collageImage--brandingAgency"></div>
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-lg-12 orange-text-container copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>We're a different kind of branding agency in Orange County,
                                                with insight and international experience. </strong>
                                        </h2>
                                        <p class="text-left">Our offices are located in Newport Beach, where we
                                            provide our clients with these essential services:
                                        </p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <ul>
                                                    <li>Branding</li>
                                                    <li>Visual Identity</li>
                                                    <li>Naming</li>
                                                    <li>Website Design</li>
                                                    <li>Spaces, Interior</li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>


                        <div class="contact-form-container">
                            {{--<div class="contact-form-close-button contact-form-close-button--webDesign">--}}

                            {{--<div>x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <section id="slick">
                                        <div class="feedback-form">
                                            <form action="/contact" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">name</div>
                                                                <div class="field">
                                                                    <input type="text" name="name" id="name" required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your name</span>
                                                                </div>
                                                                <div class="form-title">company</div>
                                                                <div class="field">
                                                                    <input type="text" name="company" id="company"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Company</span>
                                                                </div>
                                                                <div class="form-title">email</div>
                                                                <div class="field">
                                                                    <input type="email" name="email" id="email"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Email</span>
                                                                </div>
                                                                <div class="form-title">phone</div>
                                                                <div class="field">
                                                                    <input type="text" name="phone" id="phone"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                                </div>
                                                                <input type="hidden" name="recaptcha" class="recaptcha">
                                                                <input type="submit" value="Submit"
                                                                       class="send send--webDesign"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">tell us what you need</div>
                                                                <div class="field">
                                                                    <textarea name="message" placeholder="" id="message"
                                                                              class="message" required></textarea>
                                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                                    <span class="slick-tip left">Your message</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div><!--row-->
                        </div><!--container-->
                    </div>
                </div>
            </div>
        </section>


        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn"
                         data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/branding_banner_2.jpg')}}"
                             alt="Branding works">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/branding_banner_2.jpg')}}"
                             alt="Branding works">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">How do you brand yourself?</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">
                                            The core essence of a brand starts from within. It's a vision from the top
                                            management that needs to flow all the way down to the person that sits at
                                            the front
                                            desk. Your visual identity, the interior and exterior of your building, and
                                            all communication, whether online or offline, need to reflect that vision.
                                            Allow W Brand Studio to unlock the true power, full potential, and
                                            exponential value of your company branding.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection