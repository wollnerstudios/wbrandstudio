@extends('layouts.default')

@section('title')
    Award-winning Digital Marketing Agency | Orange County @endsection

@section('description')
    W Brand Studio is a leading, full-service digital marketing agency in Orange County. We specialize in customer engagement through storytelling, branding, visual identity, design and technoogy.@endsection

@section('keywords')
    digital marketing agency, digital marketing Irvine, marketing firm orange county, digital agency Irvine, bdigital marketing agency Irvine, marketing agency irvine, @endsection

@section('abstract')
    Marketing agency, marketing firm, digital marketing agency, branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('brandingHeader')
@section('brandTitle', 'DIGITAL MARKETING AGENCY')
@include('partials.branding-header')
@endsection

@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }
            }
        });
    });
    </script>
    {{--<div id="web-modal" class="modal fade" tabindex="-1" role="dialog">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content" style="background-color: #efefef; border-radius: 0;">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;--}}
    {{--right: -25px;--}}
    {{--top: -25px;--}}
    {{--background: black;--}}
    {{--color: white;--}}
    {{--opacity: 1;--}}
    {{--border-radius: 50%;--}}
    {{--width: 30px;--}}
    {{--height: 30px;--}}
    {{--display: flex;--}}
    {{--align-items: center;--}}
    {{--justify-content: center;"><span aria-hidden="true">&times;</span></button>--}}
                {{--<div class="modal-body">--}}
                    {{--<form action="/website-contact-submit" method="post" style="margin-bottom: 50px;">--}}
                        {{--@csrf--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" class="form-control" name="name" placeholder="Name" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" class="form-control" name="company" placeholder="Company">--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="email" class="form-control" name="email" placeholder="Email" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" class="form-control" name="phone" placeholder="Phone" required>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<select class="form-control" name="price" required>--}}
                                {{--<option>My budget is:</option>--}}
                                {{--<option>$5,000 - $10,000</option>--}}
                                {{--<option>$10,001 - $20,000</option>--}}
                                {{--<option>$20,001 - $30,000</option>--}}
                                {{--<option>$30,001 - $40,000</option>--}}
                                {{--<option>$40,001 - $50,000</option>--}}
                                {{--<option>$50,000 +</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label>I am looking for:</label>--}}
                            {{--<br>--}}
                            {{--<div class="row" style="display: flex;flex-wrap: wrap;align-items: center;justify-content: flex-start;">--}}
                                {{--<div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">--}}
                                    {{--<label class="checkbox-inline w-20p" style="margin-left: 10px;"><input type="checkbox" value="branding">Branding</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service1" type="checkbox" value="visual ID">Visual Identity</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service2" type="checkbox" value="logo design">Logo Design</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service3" type="checkbox" value="web dev">Website Development</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service4" type="checkbox" value="social media management">Social Media Management</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">--}}

                                    {{--<label class="checkbox-inline w-20p"  style="margin-left: 10px;"><input name="service5" type="checkbox" value="seo">SEO</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service6" type="checkbox" value="ppc">PPC</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service7" type="checkbox" value="video prod">Video/Commercial Production</label>--}}
                                    {{--<label class="checkbox-inline w-20p" ><input name="service8" type="checkbox" value="photography">Photography</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4" style="display: flex; flex-direction: column; align-items: flex-start;">--}}

                                    {{--<label class="checkbox-inline w-20p" style="margin-left: 10px;"><input name="service9" type="checkbox" value="int/ext design">Interior/Exterior Design</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service10" type="checkbox" value="print">Print Design (i.e. brochure)</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service11" type="checkbox" value="Naming">Naming</label>--}}
                                    {{--<label class="checkbox-inline w-20p"><input name="service12" type="checkbox" value="marketing">Marketing</label>--}}
                                {{--</div>--}}


                            {{--</div>--}}

                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" class="form-control" name="comments" placeholder="Comments:">--}}
                        {{--</div>--}}
                        {{--<input type="hidden" name="recaptcha" class="recaptcha">--}}
                        {{--<input type="hidden" name="page" value="marketing-agency">--}}
                        {{--<button type="submit" class="" style="margin-left: auto;--}}
    {{--position: absolute;--}}
    {{--bottom: 0;--}}
    {{--right: 0;    margin-left: auto;--}}
    {{--position: absolute;--}}
    {{--bottom: 0;--}}
    {{--right: 0;--}}
    {{--background: orange;--}}
    {{--color: white;--}}
    {{--padding: 10px;--}}
    {{--font-weight: bold;">SUBMIT</button>--}}
                    {{--</form>--}}

                {{--</div>--}}

            {{--</div><!-- /.modal-content -->--}}
        {{--</div><!-- /.modal-dialog -->--}}
    {{--</div><!-- /.modal -->--}}



@section('content')
    <div class="page page-marketing-firm">
    <div class="page page-about">
        <section class="image-cap-section wow fadeInUp opacity-0">
           
                            <img class="wid-100 hidden-xs" src="{{url('storage/wb_images/digital-marketing.jpg ')}}" alt="branding and marketing">
                        </div>

        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0"
                 data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            We are a trusted, award-winning digital marketing agency with a proven track record.
                        </h1>
                        <p> Digital marketing, also known as online marketing, is the promotion of brands, products and/or services online.
                        We are focused on results-based marketing in the digital world. Measurable marketing and ROI is the name of the game.
                        Our services cover all SEM services including SEO, PPC (Google and Bing Ads), Content Marketing, Website Analytics and Social Media Marketing.
                            We deliver a personal, passionate & tailored service to each and every one of our clients, big or small, based in Orange County or across the country. 
                            <br><br>If you'd like to improve your digital marketing efforts, get more leads and sales, increase
                            brand awareness, and make true, engaging connections with your customers, call us today!
                                 <p>

                            <br><br><a href="/contact" class="myButton org-btn">LET'S TALK</a><br><br>
                        </p>

   <br>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<div class="g-partnersbadge" data-agency-id="5228729113"></div>
                    </div>
                </div>
            </div>
        </section>
        
            


    </div>
@endsection