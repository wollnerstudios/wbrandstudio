@extends('layouts.default')

@section('title')
    Advertising Agency | Orange County | Advertising Firm | OC
@endsection

@section('description')
    We are a different and creative advertising agency in Orange County, CA.  Awardwinning Advertising Firm with international experience.
@endsection

@section('keywords')
advertising agency, marketing, branding
@endsection

@section('abstract')
    advertising agency, advertising firm, orange county advertising, orange county, los angeles advertising company
@endsection

@section('brandingHeader')
@section('brandTitle', 'ADVERTISING AGENCY')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-advertising-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            As an advertising agency, we are branders and storytellers.
                        </h1>

                        <p>
                            Everyone has a story to tell. If anyone can tell your story, we can. We are an independent, creatively driven branding and advertising agency that is made up of artists, strategists, technologists, writers and doers. We connect brands with people and create ideas that resonate with people by putting a brand's purpose at the center of communication.
                        </p>

                        <p>
                            <a href="http://www.wbrandstudio.com/contact">
                                <img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;">
                            </a>

                        </p>

                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/advertising-agency.jpg')}}" alt="Branding for Menu">

                        <p>
                            Think different - the best thinkers often do. We believe that your company, products or services have to have a unique selling proposition - a public personality that defines who you are and what you do. Think Different and contact us today for a no-obligation quote.
                        </p>

                        <p>
                            Scroll down for a listing of our services and more.
                        </p>

                        <h2>Call us today at (657) 232-0110</h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/about-agency.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>We practice what we preach. We walk the walk, not just talk the talk. </strong>
                                        </h2>
                                        <p class="text-left">We're an advertising agency rooted in branding. Our offices are located in Newport Beach, it's where we provide our clients with these essential services:
                                        </p>
                                        <ul class="list-dashed">
                                            <li>Branding</li>
                                            <li>Advertising</li>
                                            <li>Marketing</li>
                                            <li>Website Development</li>
                                            <li>Visual Identity</li>
                                            <li>SEO/PPC</li>
                                            <li>Video</li>
                                            <li>Social Media</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>


        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Here's why you should choose us.
                        </h2>
                        <p>
                            We are a passionate and dedicated group of professionals who have been in this industry for 35 years. We are uniquely interested in listening to our clients so we can really get to know them before we create a single dot. We are proactive, accessible, collaborative and will always make sure you are happy with the results. If you are looking for someone who can do it all in-house, then you've come to right place. We are a full-service agency and can help you with everything from total branding to online marketing (SEM) and website development, to your logo re-design, visual identity and advertising needs.
                        </p>
                        <p>
                            <em>
                                <small>
                                    "Michael Wollner and his team were able to take a struggling product, and within a very short period of time analyze and create a program that truly sent the message to our customers we wanted, but managed to boost the spirit of the entire team in the process. We saw not only direct increases in trackable profits, but were recognized by our competitive set for capitalizing on our uniqueness that W Brand Studio identified for our resort. Thank you for your energy, efforts and results!"
                                    <br><br>
                                    Steve Tremewan, General Manager, CalNeva Resort Lake Tahoe.
                                </small>
                            </em>
                        </p>
                        <p>
                            <a href="testimonials">Click here to read more testimonials, or simply click below for a no-obligation quote.</a>
                        </p>
                        <p>
                            <a href="http://www.wbrandstudio.com/contact"><img src="assets/images/get-a-quote.jpg" alt="Easy Quote" style="width:183px;height:45px;"></a>
                        </p>
                    </div>
                </div>
            </div>
        </section>


        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/work-sq.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">How do you brand yourself? </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">The core essence of a brand starts from within. It's a vision from the top management that needs to flow all the way down to the person that sits at the front desk. Your visual identity, the interior and exterior of your building, and all communication, whether online or offline, need to reflect that vision. Allow W Brand Studio to unlock the true power, full potential, and exponential value of your company branding.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection