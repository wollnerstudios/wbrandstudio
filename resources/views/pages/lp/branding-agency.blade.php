@extends('layouts.default')

@section('title')
    Top Branding Agency | Orange County, CA | Best Marketing Agency in OC
@endsection

@section('description')
    We are a one-stop creative branding agency in Orange County. Trusted for over 22 years by long-term clients.
@endsection

@section('keywords')
    branding agency, branding agency orange county, orange county branding agency, branding firm
@endsection

@section('abstract')
    We are a one-stop creative branding agency in Orange County with unique designs. Trusted for over 22 years by long-term clients.
@endsection

@section ('subject')
    We are a one-stop creative branding agency in Orange County with unique designs. Trusted for over 22 years by long-term clients.
@endsection

@section('brandingHeader')
@section('brandTitle', 'BRANDING AGENCY')
@include('partials.branding-header')
@endsection






@section('content')

    @include('partials.branding-modal')


@section('content')
    <script src="https://www.google.com/recaptcha/api.js?render=6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc"></script>
    <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcftLUUAAAAAJEN9dTxYD4Xer61N1UAWlsqO2Hc', {action: 'contact'}).then(function(token) {
            if(token) {
                console.log('Welcome to W Brand');
                var elements = document.getElementsByClassName("recaptcha");
                for(var x=0; x < elements.length; x++)
                {
                    elements[x].value = token;
                }

            }
        });
    });
    </script>
    <img class="wid-100 hidden-xs" src="/images/branding-visual-identity_2.jpg" alt="branding and marketing">
    <br><br>
    <div class="page page-branding-agency">
        <section class="page-copy-section text-center pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-title text-bronze" style="margin-bottom:45px; font-size: 4.8rem">
                            A top-tier agency developing top-tier brands for 25 years
                        </h1>
                        <div class="sm-underline">&nbsp;</div>
                        <p class="mb-5">
                            Branding: It’s as crucial to your company’s success as the products and services you sell. W Brand Studio is a comprehensive branding and marketing agency with decades of experience branding some of the world’s most noted companies locally and worldwide.
                        </p>
                        <p>
                            We believe simplicity, consistency and disciplined strategic differentiation create maximum brand impact that builds awareness, attracts customers, and earns their loyalty. We also believe:
                        </p>
                      
                        <div style="color:#BFA179 !important;font-weight:bold;font-size: 2.8rem;" class="mt-5 mb-5">CHANGING PERCEPTION CHANGES RESULTS</div>
                        <div style="color:#BFA179 !important;font-weight:bold;font-size: 2.8rem;" class="mb-5">BIG IDEAS AREN’T JUST FOR BIG COMPANIES</div>
                        <div style="color:#BFA179 !important;font-weight:bold;font-size: 2.8rem;" class="mb-5">DRIVING AWARENESS DRIVES BUSINESS</div>
                        <div style="color:#BFA179 !important;font-weight:bold;font-size: 2.8rem;" class="mb-5">OUTSMARTING BEATS OUTSPENDING EVERY TIME</div>


<p class="mt-5 mb-5" style="font-weight:bold;">Let us unleash the full potential of your brand with our full range of services.</p>


<p class="mb-5">Naming · Logo Development · Brand Strategy · Brand Positioning · Competitor Research · Brand Promise · User Experience · Brand Architecture · Brand Messaging · Brand Identity · Interior/ Exterior Design · More</p>
                    <a href="/contact" class="myButton org-btn mb-2">Sign up for a free consultation</a>
                    <div style="color:#BFA179 !important;font-weight:bold;font-size: 2.8rem;">or call us at (657) 232-0110</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="hidden-xs page-copy-section text-center pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div style="background:#ebe3d8;display:flex;">
                    <div style="flex-grow: 0;flex-shrink: 0;width:237px;">
                        <img src="/images/laguna-woods-branding.jpg" alt="Laguna Woods Branding" width="100%" />
                    </div>
                    <div>
                        <p class="p-4"><i>
                            “W Brand Studio embraced the difficult challenge of re-branding a 40-year-old "active adult" community, delivering results that are fresh, current, attractive, and appropriate for our client Laguna Woods Village. Michael’s team was professional and responsive to our core brand and clients in providing a comprehensive, strategic re-branding plan. The final product was nothing short of perfection, and the presentation was outstanding. Thank you, W Brand Studio!"
                        </i></p>
                        <p style="font-weight:bold;" class="p-4">-- Wendy Bucknum, CMCA, AMS, CACM</p>
                        <p class="p-4">Government & Public Affairs Manager Professional Community Management, Inc., Agent for Laguna Woods Village</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-flex display-t-r-sm">
                    <div class="col-sm-6 display-flex display-t-c-sm pad-l-0 pad-r-0 wow rotateInUpRight fadeIn"
                         data-wow-offset="100" data-wow-delay="100ms">
                        {{--added new image here--}}
                        <img class="wid-100 visible-xs" src="{{url('/images/branding_banner_1-new.jpg')}}"
                             alt="Branding for Menu">
                        <div class="wid-100 hei-100 hidden-xs pos-abs collageImage--brandingAgency"></div>
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-lg-12 orange-text-container copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <h2 class="text-left">
                                            <strong>We're a different kind of branding agency in Orange County, with insight and international experience.</strong>
                                        </h2>
                                        <p class="text-left">Our offices are located in Costa Mesa, where we provide our clients with these essential services:
                                        </p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <ul>
                                                    <li>Branding</li>
                                                    <li>Visual Identity</li>
                                                    <li>Naming</li>
                                                    <li>Website Design</li>
                                                    <li>Spaces, Interior</li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>


                        <div class="contact-form-container">
                            {{--<div class="contact-form-close-button contact-form-close-button--webDesign">--}}

                            {{--<div>x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <section id="slick">
                                        <div class="feedback-form">
                                            <form action="/contact" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">name</div>
                                                                <div class="field">
                                                                    <input type="text" name="name" id="name" required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your name</span>
                                                                </div>
                                                                <div class="form-title">company</div>
                                                                <div class="field">
                                                                    <input type="text" name="company" id="company"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Company</span>
                                                                </div>
                                                                <div class="form-title">email</div>
                                                                <div class="field">
                                                                    <input type="email" name="email" id="email"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Email</span>
                                                                </div>
                                                                <div class="form-title">phone</div>
                                                                <div class="field">
                                                                    <input type="text" name="phone" id="phone"
                                                                           required/>
                                                                    <!-- <span class="entypo-user icon"></span> -->
                                                                    <span class="slick-tip left">Enter your Phone Number</span>
                                                                </div>
                                                                <input type="hidden" name="recaptcha" class="recaptcha">
                                                                <input type="submit" value="Submit"
                                                                       class="send send--webDesign"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-10 col-sm-offset-1 ">
                                                                <div class="form-title">tell us what you need</div>
                                                                <div class="field">
                                                                    <textarea name="message" placeholder="" id="message"
                                                                              class="message" required></textarea>
                                                                    <!-- <span class="entypo-comment icon"></span> -->
                                                                    <span class="slick-tip left">Your message</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div><!--row-->
                        </div><!--container-->
                    </div>
                </div>
            </div>
        </section>


    </div>
@endsection