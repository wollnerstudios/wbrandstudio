@extends('layouts.default')

@section('title')
    Client Testimonials | WollnerStudios Clients and Projects @endsection

@section('description')
    WollnerStudios has a proven track record in generating success for our clients. Hear what our amazing clients say and the fantastic results they've experienced. @endsection

@section('keywords')
    branding agency, marketing agency @endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection


@section('brandingHeader')
@section('brandTitle', 'Testimonials')
@include('partials.branding-header')
@endsection


@section('content')
    <div class="page page-testimonials">
        <section class="copy-block-gray testimonial-gray-block testimonial-interactive-block wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="testimonial-close-button hidden-xs">
                <div>x</div>
            </div>
            <div class="container-fluid-table display-t-sm width-100 opacity-0">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 testimonial-interactive-block-image">
                        <img class="wid-100 opacity-0 hidden-xs" src="{{url('/images/HBR.jpg')}}" alt="HONEYBEE ROBOTICS">
                        <img class="wid-100 opacity-0 visible-xs" src="{{url('/images/HBR.jpg')}}" alt="HONEYBEE ROBOTICS">
                        <div class="testimonial-client-information">
                            <div>Kris Zacny, Ph.D.</div>
                            <div>VP, exploration technologies. HONEYBEE ROBOTICS</div>
                        </div>
                    </div>
                    <div class="col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm testimonial-interactive-block-copy">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 testimonial-interactive-block-p">
                                <article class="copy-block-gray text-center tk-futura-pt">
                                    <h1>HONEYBEE ROBOTICS</h1>
                                    <hr>
                                    <p class="testimonial-scrollbar">“I had the pleasure of working with Michael and his team at WollnerStudios during the design, construction and commissioning of our new location in Altadena, CA. Michael and his team have done a superb job trying to meet our requirements while keeping the cost within our allocated budget. They understand where we should and should not spend the money while trying to keep the overall theme ‘robotics/aerospace/high tech’ intact. Our new facilities look amazing and yet are very functional, which is critical to winning new contracts and staying under budget. WollnerStudios bears my highest recommendations. Michael has also re-designed our logo and developed new branding – these were critical as we have been growing and acquiring new companies.”</p>
                                </article>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-copy-section text-center tk-futura-pt testimonial-copy wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container padding-t-4 padding-b-4 position-r">
                <div class="row">
                    <div class="col-sm-12">
                        "2017 Hermes Creative Awards Spotlight: WollnerStudios. Websites follow certain patterns and trends from year to year. What was hot yesterday may be passé today. Some companies not only keep up with the evolution of website design but set the standard. WollnerStudios is a branding agency out of Newport Beach, California. Its client list includes a who's who of the Fortune 500. They were asked to create a high-end brand for a Long Beach landscaping service. WollnerStudios's website design for RENEW is simple yet elegant. The copy is sparse but meaningful without all the showy verbiage. And, if you are looking for all the menu buttons in all the usual places, you won't find them here. The navigation is novel."
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-left position-r testimonial-part">
                        <small class="display-b-xs">Association of Marketing and Communication Professionals.</small>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-block-gray testimonial-gray-block testimonial-interactive-block wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="testimonial-close-button hidden-xs">
                <div>x</div>
            </div>
            <div class="container-fluid-table display-t-sm width-100 opacity-0">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 testimonial-interactive-block-image">
                        <img class="wid-100 opacity-0 hidden-xs" src="{{url('/assets/images/Mike-Bowermaster 2.jpg')}}" alt="Mike Bowermaster">
                        <img class="wid-100 opacity-0 visible-xs" src="{{url('/assets/images/mobile/bowermaster-mobile.jpg')}}" alt="Mike Bowermaster">
                        <div class="testimonial-client-information">
                            <div>Mike Bowermaster</div>
                            <div>Bowermaster and Associates</div>
                            <div>(888) 825.4332</div>
                        </div>
                    </div>
                    <div class="col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm testimonial-interactive-block-copy">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 testimonial-interactive-block-p">
                                <article class="copy-block-gray text-center tk-futura-pt">
                                    <h1>BowerMaster and Associates</h1>
                                    <hr>
                                    <p class="testimonial-scrollbar">"Michael and his team spent time understanding the culture of Bowermaster and where we have been and where we wanted to go. He was able to get us to think out of the insurance box, which is not easy to do. He gained our confidence through incisive questioning conversation, and observation, which eased us into a decision making process that, took all the factors we were concerned with in effect. We are extremely excited about our new direction and excited with what we have created with Michael's assistance and insight."</p>
                                </article>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center tk-futura-pt testimonial-copy wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container padding-t-4 padding-b-4 position-r">
                <div class="row">
                    <div class="col-sm-12">
                        "WollnerStudios embraced the difficult challenge of re-branding a 40-year-old "active adult" community with over 18,000 residents, and produced a product that is fresh, current, attractive and appropriate for the client. The new Laguna Woods Village logo, an elegant stylized green leaf, reflects the vitality and strength of the community. The Wollner team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection, and the presentation was outstanding. Thank you, WollnerStudios!"
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-left position-r testimonial-part">
                        <small class="display-b-xs">Wendy Bucknum</small>
                        <small class="display-b-xs">CMCA, AMS, CACM</small>
                        <small class="display-b-xs">Government & Public Affairs Manager</small>
                        <small class="display-b-xs">Professional Community Management, Inc. - Agent for Laguna Woods Village</small>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray testimonial-gray-block testimonial-interactive-block wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="testimonial-close-button hidden-xs">

                <div>x</div>
            </div>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm testimonial-interactive-block-image">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/Tim-Flanagan 2.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/tim-flanagan-mobile.jpg')}}" alt="Branding for Menu">
                        <div class="testimonial-client-information">
                            <div>Timothy Flanagan</div>
                            <div>Sentry Control Systems - SKIDATA Group</div>
                            <div>(800) 246.6662</div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm testimonial-interactive-block-copy">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 testimonial-interactive-block-p">
                                <article class="copy-block-gray text-center tk-futura-pt">
                                    <h2>Sentry Control Systems</h2>
                                    <hr>
                                    <p class="testimonial-scrollbar">
                                        "Dear Michael, we would like to thank you for the excellent work WollnerStudios has done creating our company's new website, logo, and improving our overall corporate image. We recognized that we were in very capable hands the first time we met with you. You and your team made a considerable effort to understand all aspects of our business upfront, and combined with your understanding of our needs, you have produced an image that fits our company's personality." </p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center tk-futura-pt testimonial-copy wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container padding-t-4 padding-b-4 position-r">
                <div class="row">
                    <div class="col-sm-12">
                        "As a Marketing Manager for several of Yamaha's musical instrument product lines, I have had the opportunity to work with Michael and his team on a variety of "before" and "after-the sale" projects. They has always delivered above and beyond my expectations because of their unique eye and talent for marketing and design. I would highly recommend using WollnerStudios to get the expertise, professionalism and value that is so essential in today's challenging business environment."
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-left position-r testimonial-part">
                        <small class="display-b-xs">Jim Levesque</small>
                        <small class="display-b-xs">Marketing Manager, Yamaha Keyboard Division,
                            Yamaha Corporation of America
                        </small>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonial-list-section background-c-shade-1 padding-t-4 padding-b-4 wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>

                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/tui-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "WollnerStudios was assigned to redesign TUI University's website on a short timeline and aggressive budget. They have performed to our outmost satisfaction reducing our bounce rate by 25% and increasing the amount of time visitors spend on our site. I am thankful to Michael and his team for delivering a product that exceeded the value of the contract. Thank you."
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Tim Finaly <br>
                                            VP of Administration, TUI University
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/calneva-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "Michael Wollner and his team were able to take a struggling product, and within a very short period of time analyze and create a program that truly sent the message to our customers we wanted, but managed to boost the spirit of the entire team in the process. We saw not only direct increases in trackable profits, but were recognized by our competitive set for capitalizing on our uniqueness that Wollner Studios identified for our resort. Thank you for your energy, efforts and results!"
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Steve Tremewan <br>
                                            General Manager, CalNeva Resort Lake Tahoe
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/habitat-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "Michael and his team have the very special ability to work from the inside out to articulate the essence of who you really are as a company, what you are passionate about, what your competitive edge is and what it is you want to achieve. This perspecitve as opposed to imposing a set formula or signature, is an exceedingly valuable ability."
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Phil Roth <br>
                                            Habitat Hospitality Design
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/buncher-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "In reviewing portfolios on line I ran across WollnerStudios and was intrigued by the work which was edgy but still professional. Upon our initial consultation I was impressed at the time spent with us in an effort to know us as individuals as well as learn more about our vision for the future of our company. We at The Buncher Law Corporation cannot thank WollnerStudios enough for their incredibly creative and professional approach to the branding of our business."
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Kerrie Murphy<br>
                                            The Buncher Law Firm
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/lwv-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "WollnerStudios embraced the difficult challenge of re-branding a 40-year-old "active adult" community, and produced a product that is fresh, current, attractive and appropriate for the client. The new Laguna Woods Village logo, an elegant stylized green leaf, reflects the vitality and strength of the community. The Wollner team was professional and responsive to the client's desires in providing a comprehensive re-branding strategic plan. The final product was nothing short of perfection, and the presentation was outstanding."
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Wendy Bucknum <br>
                                            CMCA, AMS, CACM
                                            Governmental & Public Affairs Manager
                                            Professional Community Management, Inc - Agent for Laguna Woods Village
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/nasa-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "On behalf of the entire McDonnell Douglas Aerospace X-33/RLV proposal team, I would like to thank Michael Wollner for his role in the development of the MDC proposal submission for the NASA X-33 Phase II program. Your leadership and tremendous experience helped to focus and streamline the creation of our proposal CD to make the best possible presentation."
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Paul L. Klevatt<br>
                                            RLV Program Manager<br>

                                            Steve Cook<br>
                                            X-33 Dep. Prog. Manager NASA-MSFC
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-push-10">
                                        <img src="{{url('/assets/images/keylink-icon.jpg')}}" alt="" class="img-responsive testimonial-list-image">
                                    </div>
                                    <div class="col-sm-10 col-sm-pull-2">
                                        <p>
                                            "I have relied on WollnerStudios creative touch for not only our Web site but strategic marketing campaigns for large accounts over the years & he has always been instrumental in helping us standout among competitors and improving our image with clients. I highly recommend using what I call, The Wollner Touch"
                                        </p>
                                        <div class="testimonail-list-section-name">
                                            Robert Basulto<br>
                                            President & CEO of Keylink Solutions
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonial-bottom-contact wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 text-center padding-t-4 padding-b-4 background-c-shade-4 color-white">
                        For more testimonials, please <a style="color:#fff" href="/contact">contact us</a>.
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection