@extends('layouts.default')

@if(!$singlePage)
    @section('customHTMLClass') gallery-open work-page @endsection
    @section('title') WollnerStudios Portfolio | Creative Design Examples  @endsection
    @section('description') Check out WollnerStudios' portfolio for examples of our creative branding and design projects, including logos, websites, and marketing materials @endsection
    @section('keywords') marketing agency, branding agency @endsection
    @section('abstract') See work where strategic brand development is combined with break-through creativity to deliver measurable results & discover unique branding & marketing. @endsection
@else
    @section('customHTMLClass') work-page @endsection
    @section('description') {{ $work['seoDescription'] ?? 'See our work where strategic brand development is combined with break-through creativity to deliver measurable results.'}} @endsection
    @section('title') {{ $work['seoTitle'] ?? 'Total Branding | W Brand Studio | Digital Marketing Newport Beach'}} @endsection
    @section('keywords') {{ $work['seoKeywords'] ?? 'seo, marketing, branding agency'}} @endsection
@endif

@section('brandingHeader')
@section('brandTitle', 'WORK')
@include('partials.branding-header-work')
@endsection

@section('content')
    <div class="page page-work">
        <section class="hidden-xs">
            <div class="gallery-work" id="slick-gallery-work">
                @if($singlePage)
                    @foreach($work['mainImageFiles'] as $image)
                        <div><img class=" width-100" src="{{$image[0]}}" alt="{{$image[1]}}"></div>
                    @endforeach
                @else
                    <div>
                        <img class="width-100" src="{{url('assets/images/work/Large_Photos/buncher.jpg')}}" alt="">
                    </div>
                @endif
            </div>
        </section>

        <section class="position-r hidden-xs ">
            <div class="container-fluid work-gallery-section background-c-shade-3">
                <div class="row padding-b-6">
                    <div class="col-xs-12">
                        {{--<ul class="gallery-menu">--}}
                            {{--<li data-filter=".category-all" class="display-i-b-xs filter waves-effect cat-all-filter active">All</li>--}}
                            {{--<li data-filter=".category-1" class="display-i-b-xs filter waves-effect cat-1-filter">branding</li>--}}
                            {{--<li data-filter=".category-2" class="display-i-b-xs filter waves-effect cat-2-filter">visual identity</li>--}}
                            {{--<li data-filter=".category-3" class="display-i-b-xs filter waves-effect cat-3-filter">Web site</li>--}}
                            {{--<li data-filter=".category-4" class="display-i-b-xs filter waves-effect cat-4-filter">Logo Design</li>--}}
                            {{--<li data-filter=".category-5" class="display-i-b-xs filter waves-effect cat-5-filter">Graphic Design</li>--}}
                            {{--<li data-filter=".category-6" class="display-i-b-xs filter waves-effect cat-6-filter">Photography</li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 padding-l-0 padding-r-0 position-a bot-0 slick-gallery-thumb" id="gallery-container">
                        @foreach($allWork->chunk(8) as $key => $slide)
                            <div class="slide-{{$key}}">
                                @foreach($slide as $key => $image)
                                    @foreach($image['mainImageFiles'] as $source)
                                        <?php $mainImageData[] = $source[0]; ?>
                                    @endforeach

                                    <div class="gallery-thumb-image mix {{implode(" ", $image['categories'])}} hover-image position-r" data-myorder="{{$key}}">
                                        <img src="{{url($image['thumbImageFile'][0])}}" alt="{{$image['thumbImageFile'][1]}}" data-main-description="{{$image['mainDescription']}}" data-main-img="{{implode(',',$mainImageData)}}">
                                        <div class="hover-effect-text">
                                            <a href="{{url('work')}}/{{$image['slug']}}" class="take-a-look display-i-b-xs">Take a look</a>
                                            <div class="description">{{$image['hover-effect-description']}}</div>
                                            <div class="client-name">{{$image['hover-effect-client-name']}}</div>
                                        </div>
                                        <div class="image-hover-effect"></div>
                                    </div>
                                    <?php $mainImageData = null ?>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="container-fluid-table display-t-sm width-100 hide-this-box-for-gallery">
                <div class="row-table display-t-r-sm ">
                    <div class="col-sm-6 display-t-c-sm work-gallery-copy font-f-1 text-center vertical-a-m-sm">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 work-description-text">
                                @if($singlePage)
                                    @if(isset($work['mainDescription']))
                                        {!!$work['mainDescription']!!}
                                    @else
                                        <p>
                                            Every project we do is crafted from the ideas and strategy behind your brand.
                                        </p>
                                    @endif

                                @else
                                    <p>
                                        Every project we do is crafted from the ideas and strategy behind your brand.
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 display-t-c-sm padding-l-0 padding-r-0">
                        <img class="placeholder" style="width:99%" src="{{url('assets/images/placeholder-left-white.jpg')}}" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="background-c-shade-2 position-r z-i-2 text-center work-text-description font-f-2 padding-t-4 padding-b-4 hidden-xs">

            <div class="container-fluid hidden-xs position-a width-100 top-0 left-0 right-0 work-button-section">
                <div class="row">
                    <div class="col-xs-6 padding-l-0 padding-r-0 background-c-shade-3 work-section-left">
                        <span class="invisible">work</span></div>
                    <div class="col-xs-6 padding-l-0 padding-r-0 background-c-shade-4 color-white work-button-button waves-effect waves-light">
                        <img src="{{url('assets/images/gallery-icon-white.png')}}" alt=""> <span>WORK</span></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1 padding-t-2">
                        <p>
                            Through brand strategy, identity development, website development and product design, we help our clients shape what's next. W Brand Studio has a proven track record over two decades in generating success for our clients. Whether it's increasing awareness, driving sales, or moving product off shelves-the, we do it all. Take a look at some of the work we've done where well-thought-out brand development strategies are combined with break-through creativity to deliver measurable results.
                        </p>
                    </div>
                </div>
            </div>

        </section>

        <section class="visible-xs background-c-shade-3 text-center front-page-service-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        Selected Works
                    </div>
                </div>
            </div>
        </section>
        <section class="visible-xs">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 padding-l-0 padding-r-0">
                        <div id="slick-slider-work-mobile">
                            @foreach ($mobileImages as $key=>$mobileImage)
                                <div class="slick-work-mobile-image">
                                    <img class="width-100" src="{{url($mobileImage['imageFile'][0])}}" alt="{{$mobileImage['imageFile'][1]}}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="background-c-shade-4 see-our-desktop-version visible-xs text-center">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        TO SEE MORE, PLEASE VISIT OUR DESKTOP VERSION
                    </div>
                </div>
            </div>
        </section>
        <section class="visible-xs mobile-copy-block padding-t-2 padding-b-2">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        Every project we do is crafted from the ideas and strategy behind your brand.
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
