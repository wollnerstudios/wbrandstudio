@extends('layouts.default')

@section('title')
    Branding and Marketing Experts | Newport Beach, CA @endsection

@section('description')
    Branding is everything. We know how to make you stand out from your competition and get you noticed. @endsection

@section('keywords')
    branding agency, marketing agency, orange county branding @endsection

@section('abstract')
    Branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection



@section('brandingHeader')
@section('brandTitle', 'MARKETING PROS')
@include('partials.branding-header')
@endsection



@section('content')
    <div class="page page-branding-agency page-brandMarket">
        <section class="page-copy-section mar-b-2 mar-t-2 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100"
                 data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="paddedtop">
                            Do you need more customers?
                            <br/>
                            Then get noticed!
                        </h1>
                        <p>And to get noticed, you have to differentiate yourself from your competitors. “Who are you
                            and why should I trust you? Why should I do business with you? What’s in it for me?” These
                            are some of the questions potential customers ask all the time, and we know how to answer them.
                            We are branding and marketing experts with over 20 years of experience, helping small and large businesses in Orange
                            County and nationwide. We can provide you with a better online presence that actually
                            sells, and show you how to get measurable results and increased ROI whether it’s in print or
                            online. Your future depends on it.<br><br>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-container--brandMarket paddedtop">
                            <h2>Schedule A FREE Consultation</h2>
                            <form action="/consultation" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="field">
                                            <input class="form__input" type="text" name="name"
                                                   id="name"
                                                   placeholder="Name"
                                                   required/>
                                        </div>
                                        <div class="field">
                                            <input class="form__input" type="email" name="email" id="email"
                                                   placeholder="Email"
                                                   required/>
                                        </div>
                                        <div class="field">
                                            <input class="form__input" type="text" name="phone" id="phone"
                                                   placeholder="Phone"
                                                   required/>
                                        </div>
                                        <div class="field">
                                            <textarea name="message" placeholder="Message" id="message"
                                                      class="message form__input form__input--textArea"
                                            ></textarea>
                                        </div>
                                        <div class="field">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6Ld1tDQUAAAAAOWIoCiML4SjfxaxI6DpGG-92VVe"></div>
                                        </div>
                                        <input type="submit" value="CONTACT US!"
                                               class="send send--webDesign form__input form__input--btn"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection