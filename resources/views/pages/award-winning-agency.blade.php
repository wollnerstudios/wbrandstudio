@extends('layouts.default')

@section('title')
    Digital Marketing & Advertising Agency| Award Winning Services
@endsection

@section('description')
    We are one of the fastest growing branding and marketing agencies in Orange County with unique insight, experience, and energy.
@endsection

@section('keywords')
    ad agency orange county, branding, branding agency, marketing agency
@endsection

@section('abstract')
    We are one of the fastest growing branding and marketing agencies in Orange County with unique insight, experience, and energy.
@endsection

@section('brandingHeader')
@section('brandTitle', 'PRESS RELEASES')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-award-winning-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            WollnerStudios, Inc., (dba W Brand Studio) Wins MarCom Gold for Boston Hotel Website
                        </h1>

                        <h3>FOR IMMEDIATE RELEASE</h3><br>


                        <p>
                            <strong>Costa Mesa, CA, January 18, 2023 -</strong>
                            Three months after signing on as the digital marketing agency for Newbury Guest House last year, WollnerStudios launched a new website for the three-star Boston boutique hotel and took home Gold at the MarCom Awards for their work.
                        </p>

                        <br>

                        <p>
                            Since its inception in 2004, MarCom has become one of the largest and most respected international competitions for excellence in marketing and communications, receiving 6,500 entries from around the world every year. The awards honor the creativity, hard work and generosity of industry professionals.
                        </p>

                        <br>

                        <p>
                            WollnerStudios continues to develop and execute an integrated campaign highlighting the hotel’s prime location and uniquely Boston aesthetic through strategic brand positioning, media relations, social media and influencers, as well as brand partnerships.
                        </p>

                        <br>

                        <p>
                            “Newbury Guest House defines the authentic spirit, experience and architecture of Boston’s Back Bay in a way that few hotels do,” said David Garabedian, Newbury Guest House owner. “People who stay with us once get that, and we knew that WollnerStudios got it, too – they understood why we spent nearly $1 million on renovations but made sure the stairs still creak.”
                        </p>

                        <br>

                        <p>
                            “We do business with several large corporate clients but love the opportunity to work with independent and genuinely unique partners like Newbury Guest House,” said WollnerStudios President/Creative Director Michael Wollner. “It gives us the chance to dig in and build a campaign that tells a consistent, compelling brand story across multiple channels – one that results in discovery for new customers and increased revenues for our partners.”
                        </p>
                        <br>

                        <p>
                            WollnerStudios has more than 40 years of combined experience serving clients in the travel, hospitality, lifestyle, and food and beverage sectors. The agency will be responsible for delivering a multi-pronged marketing communications program to increase visibility and awareness, drive direct and organic engagement, and highlight how Newbury Guest House defines the authentic Boston boutique hotel experience.
                        </p>
                        <br>
                        <p>
                            Located in Costa Mesa, CA, WollnerStudios offers complete branding, marketing, website and content development, photography, design, consulting, research, analytics, SEO, and PPC services to corporations and a wide range of local independent businesses. For more information, visit https://www.WollnerStudios.com, or follow WollnerStudios on Twitter and Instagram at @wbrandstudio and on Facebook.com/wbrandstudio.
                        </p>
                    </div>
                </div>
            </div>
        </section>


    <div class="page page-award-winning-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Small Agency Wins Big In Branding
                        </h1>

                        <h3>FOR IMMEDIATE RELEASE</h3><br>


                        <p>
                            <strong>Costa Mesa, CA, September 27, 2022 -</strong>
                            In competition with some of the world’s largest agencies, WollnerStudios continues to take home Platinum, Gold, and Silver prizes from the industry’s most prestigious awards events.
                        </p>

                        <br>

                        <p>
                            Over the last several years, the Orange County, CA-based agency has won nearly 20 top honors for branding, logo design, website development, and digital communication at the American Advertising Federation Awards, the Hermes Creative Awards, and the prestigious Cannes Lions Awards for Creative Excellence. WollnerStudios also won recognition for its web creativity and digital communication work at the international dotComm Awards.
                        </p>

                        <br>
                        <p>
                            "These awards are important to us because they honor both the work of our passionate and talented team, and the trust our clients place in us. They also are proof that a small but fiercely agile and creative agency can produce work at the caliber of a multi-million one,” said Michael K. Wollner, WollnerStudios CEO.
                        </p>

                        <br>

                        <p>
                            Helping build great brands has also built the agency’s new business significantly, with clients including Blue Origin, Honeybee Robotics, Boston’s Newbury Guest House, Cerona Therapeutics, RMA Companies, Intera Oncology, and Heidi's Brooklyn Deli, a nationwide restaurant chain headquartered in Denver, CO.
                        </p>

                        <br>

                        <p>
                            These new clients join a roster that illustrates WollnerStudios Studio’s flexibility and scope in serving clients from a sweeping range of market sectors: Pure Elements Water, Little Owl School, Pacific Resource Recovery, Rejuvenation Wellness, Bottega Angelina, DuToit Realty, Sara, Inc. Defense Systems, Jacobs Music, Structural Shotcrete, YNG Yoga Studios, Renew Landscapes, and Cutting Edge Aerospace.
                        </p>

                        <br>

                        <p>
                            “WollnerStudios is built on 25 years of passion for brands – for helping our clients discover what their brand truly is, what it has the potential to become, and seeing it succeed,” says Wollner. “We are able to serve such a diverse range of clients, not because we know hamburgers, defense systems or resort hotels, but because we know – and love – branding.”
                        </p>
                        <br>

                        <p>
                            WollnerStudios is a full-service marketing and branding agency based in Orange County, CA, serving clients nationwide. For more information, visit https://www.WollnerStudios.com, or follow WollnerStudios on Twitter and Instagram at @wbrandstudio and on Facebook.com/wbrandstudio.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    <div class="page page-award-winning-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Fast-Casual Chain Heidi’s Brooklyn Deli Taps WollnerStudios for Rebrand
                        </h1>

                        <h3>FOR IMMEDIATE RELEASE</h3><br>


                        <p>
                            <strong>Costa Mesa, CA, February 22, 2022 -</strong>
                            Orange County-based agency WollnerStudios ended 2021 with the addition of another client to its growing roster. The shop, co-founded in 1997 by former DDB and Ogilvy creative Michael K. Wollner, has been named the new agency for fast- casual chain Heidi’s Brooklyn Deli, headquartered in Denver, Colorado. The agency will lead the chain’s rebranding efforts as it expands its retail footprint.
                        </p>

                        <br>

                        <p>
                            “We’re excited by the challenge of helping Heidi’s evolve its brand, continue its growth, and achieve its mission to bring delicious, authentic deli food to communities around the country,” said Wollner. We believe our in-depth understanding of Heidi’s business, extensive research capabilities, and highly creative approach make WollnerStudios uniquely suited as their branding and marketing partner.”
                        </p>

                        <br>
                        <p>
                            “Heidi’s Brooklyn Deli presents exactly the kind of branding challenge we love,” added Wollner. “They have an exceptional product with a great story that we can help tell in a way that positions Heidi’s for wider awareness and future growth.”
                        </p>

                        <br>

                        <p>
                            Heidi’s Brooklyn Deli is one of the newest clients to sign on with WollnerStudios Studio’s fast-growing branding and marketing practice. Other recent additions to the agency’s roster include Boston’s renowned Newbury Guest Hous.
                        </p>

                        <br>

                        <p>
                            Located in Costa Mesa, CA, WollnerStudios offers complete branding, marketing, website and content development, photography, design, consulting, research, analytics, SEO, and PPC services to corporations and a wide range of local independent businesses. For more information, visit https://www.WollnerStudios.com, or follow WollnerStudios on Twitter and Instagram at @wbrandstudio and on Facebook.com/wbrandstudio.

                        </p>
                    </div>
                </div>
            </div>
        </section>
    <div class="page page-award-winning-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            SAATCHI & SAATCHI, RAZORFISH AND WOLLNERSTUDIOS HONORED AT GLOBAL DOTCOMM AWARDS
                        </h1>

                        <h3>FOR IMMEDIATE RELEASE</h3><br>


                        <p>
                            <strong>Newport Beach, CA, September 19, 2017 -</strong>
                            WollnerStudios was honored along with some of the world’s largest agencies for excellence in web creativity and digital communication, receiving five Platinum and two Gold Awards for their work for clients including Shigeru Kawai, The Edmon in Hollywood, CreateApp/Weyland Inc. in Singapore, and Shoring Engineers. Judged by the Association of Marketing and Communication Professionals, he dotCOMM Awards are one of the most highly regarded international evaluators of creative work, recognizing the best web and digital work in the world.
                        </p>

                        <br>

                        <p>
                            "We feel blessed to be honored with several dotCOMM awards, as well as nineteen recent awards from respected industry groups including the American Advertising Federation and the Hermes Creative Award,” " said Michael Wollner, WollnerStudios CEO. "It recognizes the hard and innovative work of our creative team, our outstanding clients, and one of the fastest growing branding agencies in Southern California."
                        </p>

                        <br>
                        <p>
                            Along with awards, the agency has picked up new business from clients including SARA, SKIDATA in Dallas, Hanover Pacific, and Apex Energetics.
                        </p>
                        <br>
                        <p>
                            WollnerStudios is a full-service branding and marketing agency located in Newport Beach, CA with capabilities including branding and identity, marketing, website development, design, consulting, research, analytics, SEO, and PPC. WollnerStudios serves mid-size businesses and major corporations. Visit: wbrandstudio.com Follow: @wbrandstudio.
                        </p>

                        <br>
                        <p>
                            What was hot yesterday may be out of style today. Some companies not only keep up with the evolution of website design but set the standard. – Association of Marketing and Communication Professionals on WollnerStudios Studio
                        </p>
                        <br>
                        <p>
                            WollnerStudios has been winning a lot of new business as well as awards. Recently, they picked up SARA, SKIDATA in Dallas, Hanover Pacific and Apex Energetics.
                        </p>

                        <br>

                        <p>
                            WollnerStudios is a branding agency located in Newport Beach, CA. The company offers many services, such as: branding and identity, marketing, website development, design, consulting, research, analytics, SEO, and PPC. WollnerStudios services major corporations as well as local businesses. For more information, visit http://www.WollnerStudios.com, or follow WollnerStudios on Twitter and Instagram at @wbrandstudio and on Facebook.com/wbrandstudio.
                        </p>

                    </div>
                </div>
            </div>
        </section>





    <div class="page page-award-winning-agency">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            WollnerStudios Studio, The Orange County Underdog Agency Wins Again With Platinum, Gold, Silver And Bronze Awards In First Quarter 2017
                        </h2>

                        <h3>FOR IMMEDIATE RELEASE</h3><br>

                        <p>
                            <strong>Newport Beach, CA, April 21, 2018 -</strong>
                            Size DOESN'T matter. It's how well you use it. The small, creative shop WollnerStudios has been making big ad agency impact after only a year and a half. The agency has produced some outstanding work that has raised judge eyebrows at award shows nationwide. WollnerStudios is run by seasoned professionals who have won the coveted Lion in Cannes, received awards from the New York Festivals, judged CLIO Awards, and taught design at The American Film Institute.
                        </p>

                        <br>

                        <p>
                            Hermes Creative Awards, a part of the Association of Marketing and Communication Professionals, writes on their site about WollnerStudios Studio, "What was hot yesterday may be out-of-style today. Some companies not only keep up with the evolution of website design but set the standard."
                        </p>

                        <br>
                        <p>
                            "Awards are important and we are excited about how many we've won so far," says Michael K. Wollner, CEO. "It shows that we are at the same level as the big multi-million dollar agencies. Every project we have submitted has won something. That's more than I can say as a Creative Director at any of the big agencies for which I've worked. Kudos to our fantastic clients who believed in us."
                        </p>

                        <br>

                        <p>
                            During the first year in Newport Beach, the fairly unknown, surprising, ninja-like agency has won the trust and business of numerous companies such as California Business Bank, Coastal Heart Medical Group, CarPark, The Edmon Restaurant in Hollywood, Renew Landscape Management in Bixby Hills, Pure Elements Water, Sentry Control / SKIDATA, Pacific Resource Recovery, and Shigeru Kawai. Call us at (657) 232-0110 or email us for more information.
                        </p>
                        <br>

                        <p>
                            During the first year in Newport Beach, the fairly unknown, surprising, ninja-like agency has won the trust and business of numerous companies such as California Business Bank, Coastal Heart Medical Group, CarPark, The Edmon Restaurant in Hollywood, Renew Landscape Management in Bixby Hills, Pure Elements Water, Sentry Control / SKIDATA, Pacific Resource Recovery, and Shigeru Kawai.
                            Call us at (657) 232-0110 or email us for more information.
                        </p>

                        <br>

                        <!--<p>
                            <a href="http://www.wbrandstudio.com/contact" class="myButton org-btn">Let's talk</a>

                        </p>-->
                    </div>
                </div>
            </div>
        </section>

        <!--<section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        </div>
                    </div>
                </div>
            </section>
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/2017-awards.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/mobile/mw-mobile.jpg')}}" alt="Branding for Menu">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left">MARKETING SERVICES</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">

                                        <h2><strong>We think differently.</strong></h2>

                                        <p>
                                            We're one of the fastest growing branding and marketing agencies with unique insight, experience, and energy. If you want to be just like everyone else, we're NOT the agency for you. However, if you want to STAND OUT in the crowd, call us. With a global background from successful ad agencies, we bring something new to the marketing mix. Let's help you with:
                                        </p>

                                        <ul class="list-dashed">
                                            <li>Branding and Visual Identity</li>
                                            <li>Search Engine Marketing (SEO/ADWORDS)</li>
                                            <li>Social Media Marketing</li>
                                            <li>Marketing and Advertising</li>
                                            <li>Website Development</li>
                                            <li>Logo Development</li>
                                            <li>Design Services and more...</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            Should winning awards matter to clients?
                        </h2>

                        <p>
                            Since awards are important to the careers of professional creative people, agencies that consistently win awards have an advantage in recruiting and retaining the best people. The clients who work with an award-winning agency benefit by having generally more capable and more driven people working on their business. So, if you want to stand out in the crowd, we're your agency. We understand the importance of reaching users on all platforms, whether it is mobile, tablet or desktop. Most of all, we understand the importance of branding. We guarantee that when we build your brand, it'll be as original as you are.
                        </p>

                        <h3><u><a href="contact">Let's set-up a meeting and make you successful too. </a></u></h3>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/digital-marketing-advertising.jpg')}}" alt="Results">
                        <img class="wid-100 visible-xs" src="{{url('assets/images/mobile/rsque-mobile.jpg')}}" alt="Smarter Branding">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title ">The heart of your business success lies in its marketing. </h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray text-center tk-futura-pt">
                                        <p class="text-left">
                                            Most aspects of your business depend on successful marketing. The overall marketing umbrella covers advertising, public relations, promotions and sales. A marketing program that gives your company the best chance is a healthy mix of different forms of marketing, such as website development, public relations, print and broadcast advertising, design and printing for all print materials, trade shows and other special events. Call us and let's unleash your potential today.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>-->
    </div>
@endsection