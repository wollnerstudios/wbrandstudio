@extends('layouts.default')

@section('title')
    How To Get More Traffic To Your Website @endsection

@section('description')
    We provide effective online marketing services, If you have a website, but very few visitors or no sales, we can help.. @endsection

@section('keywords')
sem, seo, marketing @endsection
@section('abstract')
    website traffic, convert visitors to customers, search engine optimization. SEO, PPC, pay-per-click, AdWords, branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company @endsection

@section('customHTMLClass')
    capabilities-page @endsection

@section('brandingHeader')
@section('brandTitle', 'SEO & PPC')
@section('brandColor', '#0ebed8')
@section('brandHeaderImage',' ')
@include('layout._brandingHeaderCapabilities')
@endsection

@section('content')
    <div class="page page-increase-website-traffic">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>
                            Increase Traffic to Your Website.
                        </h1>
                        <p>Grow your market share, increase e-commerce conversion, increase the conversion rates, increase lead volume and quality or reduce the cost per conversion. Whatever your goal is, we can help. We are one of the fastest growing marketing and branding companies in Orange County. We are big on keyword research and doing things differently than anyone else. For SEO (Search Engine Optimization), we will make sure you are found for the right keywords. And for PPC (AdWords), we will make sure you pay less for more. To avoid wasting your time and money on AdWords, we pay attention to the factors that determine long-term success. As smart PPC marketers we get the best results by creating well written ads, plus the right strategic plan, relevant keywords and a strong relevant landing page with a call to action, is just part of what we do really well.
                            <br><br>
                        <h2>Call us today at (657) 232-0110 and see why so many are turning to W Brand Studio for their online marketing needs.</h2></em>
                        <br><br>

                        </p>


                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/hhyc-web.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/seo-web.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/sem-google.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/crowd.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-copy-section text-center pad-t-4 pad-b-2 tk-futura-pt wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            <span style="color: #0ebed8">Get the most out of Goggle AdWords and PPC best practices. </span>
                        </h2>
                        <p>W Brand Studio is always seeking to measurably increase your Pay-Per-Click (PPC) and Search Engine Optimization (SEO) results. These are vital components of a digital marketing strategy and we're constantly observing the latest trends and best practices.
                            <br><br>
                            <strong><span style="color: #0ebed8"><a href="../contact">Call (657) 232-0110, or click for a free consultation. </a></span><strong>
                        </p></div>
                </div>
            </div>
        </section>
        <section class="cap-logo-section page-copy-section tk-futura-pt text-center wow fadeInUp opacity-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInLeft">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keywords.png')}}" alt="">
                                <h2>Keywords</h2>
                                <p>Which phrases will best describe your company to place you on the first page of Goggle? Let's find out. It all starts with keyword research. Some words get only a few clicks a day, while others may get thousands. Our knowledge will help you to make the right choices. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="200ms" id="contact-contact-block">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/keyword-search.png')}}" alt="">
                                <h2>Optimization</h2>
                                <p>Search Engine Optimization (SEO) is where you populate your website with right keywords in order to get free, organic, editorial, or natural search results. This needs to be monitored monthly by our SEO experts to see what your customers are actually searching for. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInUp" data-wow-delay="400ms">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/capabilities/seo-icon.png')}}" alt="">
                                <h2>Analytics</h2>
                                <p>Web analytics is a great way to measure how people interact with your site and to see much traffic the search engines are sending you. It's also the best way to track how organic search traffic plays a role in influencing conversion, the holy grail of search analytics.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact-block  wow fadeInRight">
                            <div>
                                <img class="img-iconi" src="{{url('/assets/images/conversion-icon.png')}}" alt="">
                                <h2>Conversion</h2>
                                <p>Site traffic is great, but if you can't convert them to paying customers what's the point? It starts with creating keyword rich content and optimized Pay-Per-Click (PPC) ads, and then we develop landing pages with a call-to-action to convert as many people as possible. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="image-cap-section wow fadeInUp opacity-0">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-4 padding-l-0 padding-r-0 ">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/montagna-branding.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/people-branding.jpg')}}" alt="">
                        </div>
                        <div class="ar-img-small">
                            <img class="width-100" src="{{url('/assets/images/capabilities/room-branding.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-4 padding-l-0 padding-r-0">
                        <div class="ar-img">
                            <img class="width-100" src="{{url('/assets/images/capabilities/sky-branding.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection