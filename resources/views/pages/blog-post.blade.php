@extends('layouts.default')
@section('brandingHeader')
@section('brandTitle', 'W PRO BLOG Post')
@include('partials.branding-header')
@endsection
@section('content')
    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb" style="margin-bottom: 0;">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/blog">Blog</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$post->title}}</li>
        </ol>
    </nav>
    @if($post->image)
    <div class="blog-feature-image" style="background: url('/storage/{{$post->image}}'); background-size: cover; background-repeat: no-repeat; height: 70vh;"></div>
        @else
            <div class="blog-feature-image" style="background: url('/assets/images/w-brand-studio-offices.jpg'); background-size: cover; background-repeat: no-repeat; height: 70vh;"></div>

        @endif




    <div class="container post-container" >

        <div class="row text-center">
            <div class="col-md-12 p-3">
                <h1 class="my-25">{{$post->title}}</h1>
                <div style="font-size: 16px;">
                    {!! $post->body !!}
                </div>
            </div>

        </div>

        <p class="my-25">
            <a href="/blog" style="padding: 10px; border: 1px solid black; color: black;">Back to Blog</a>

        </p>


    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/holder.min.js"></script>
    <script>
    $(function () {
        Holder.addTheme("thumb", {background: "#55595c", foreground: "#eceeef", text: "Thumbnail"});
    });
    </script>


@endsection