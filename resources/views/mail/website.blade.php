<html>
    <body>
        <div style="background-color: #f68308;max-width:600px;position:relative;padding-top:5px;padding-bottom:5px;text-align:center;color:#fff;">
            <img src="https://wbrandstudio.com/assets/images/WBRAND-logo-noshade.png" style="max-width:50px;"/>
        </div>
        <div class="email-table-wrapper" style="background-color: #f68308;max-width: 600px;position: relative;padding-top: 2px;padding-bottom: 2px;">
            <table class="email-table" style="width: 100%;padding: 30px;background-color: #efefef;color: #5d5c5c;text-align: left;">
                <tr><td class="email-column-titles" style="padding: 5px;">From Page:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['page']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Name:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['name']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Email:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['email']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Phone:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['phone']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Company:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['company']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Message:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['comments']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Price:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['price']}}.</td></tr>
                <tr><td class="email-column-titles" style="padding: 5px;">Services:</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service1']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service2']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service3']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service4']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service5']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service6']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service7']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service8']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service9']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service10']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service11']}}.</td></tr>
                <tr><td class="email-column" style="background-color: #fff;color: #000;padding: 15px;">{{$content['service12']}}.</td></tr>
            </table>.
            <div class="email-table-wrapper right" style="background-color: #f68308;max-width: 600px;position: relative;text-align: right;padding: 5px;color: #fff;">W BRAND STUDIO 2019</div>.
        </div>
    </body>
</html>