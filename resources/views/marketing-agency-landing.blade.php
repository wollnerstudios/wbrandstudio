@extends('layouts.default')

@section('title')
    Full Service Marketing Agency | Creative Orange County Marketing Firm
@endsection

@section('description')
    As a creative full service marketing firm in Orange County Ca, our agency delivers creative marketing results for clients that want to make an impact.
@endsection

@section('keywords')
    marketing, agency, marketing company, media, graphic design, branding, orange county ca, Newport beach, website design
@endsection

@section('abstract')
    branding agency, web design, web design, orange county website design, orange county branding, orange county website development, los angeles marketing company
@endsection

@section('brandingHeader')
@section('brandTitle', 'Full Service Agency')
@include('partials.branding-header')
@endsection

@section('content')
    <div class="page page-online-marketing-agency-orange-county">
        <section class="page-copy-section text-center pad-t-4 pad-b-4 tk-futura-pt wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h1 class="text-center">Full Service Agency</h1>

                        <p style="margin-bottom: 1rem;">Having an amazing product or service is important, but when your key prospects don’t know about you, sales suffer. Partnering with the right marketing agency makes all the difference when it comes to gaining positive exposure and branding for your organization.</p>

                        <p style="margin-bottom: 1rem;">As a marketing agency in Orange County Ca, we can help you with an overall marketing strategy, design and the actual creative delivery. No matter if you have a simply marketing project or a complete corporate rebranding initiative, we’re here to help.</p>

                        <h2 class="text-center">Call (657) 232-0110 today!</h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6  display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/offering-expert-seo-service-in-orange-county-ca.jpg')}}" alt="Branding for Menu">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/offering-expert-seo-service-in-orange-county-ca.jpg')}}" alt="Branding for Menu">
                    </div>
                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Why Work With An Agency?</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>Our marketing team offers a full scope of capabilities, including online marketing and visual identity. Although clients may not know what they need during their planning stage, it’s not an issue for our team. We’re ready to assist our clients by offering a full scope of services.</p>
                                        <br>
                                        <p>Why do companies turn to a marketing firm or agency for help? The answer is simple, it’s expertise. You’re busy running and growing your own company and like most, marketing may not be the focus of your day to day operations.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0" data-wow-offset="100" data-wow-delay="100ms">
            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 col-sm-push-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpLeft fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('assets/images/our-seo-company-drives-results-for-clients.jpg')}}" alt="Orange County Lead Generation">

                        <img class="wid-100 visible-xs" src="{{url('assets/images/our-seo-company-drives-results-for-clients.jpg')}}" alt="Orange County Lead Generation">

                        <div class="subtext-img-cap hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title landing-orange-titles-margin">Your Single Marketing Source</h2>

                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>Clients today are looking for a single source for their design needs, and as an Orange County full service marketing agency, we have the skills and experience to deliver everything from graphic design changes to a completely new website. And it’s what we focus on every single day.</p>
                                        <br>
                                        <p>Put our expertise to work for your brand and see what a difference it makes. And although our office is located in sunny Orange County Ca, we’re used to helping clients throughout the country and even a few international brands.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="copy-block-gray wow fadeInUp opacity-0 position-r" data-wow-offset="100" data-wow-delay="100ms">
            <section class="copy-block-gray animation hidden-xs invis-0" id="slide-in-about">
                <div class="contact-form-close-button" id="about-close">

                    <div>x</div>
                </div>
                <div class="container-fluid-table display-t-sm width-100">
                    <div class="row-table display-t-r-sm">
                        <div class="col-sm-3 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                            <img class="wid-100 " src="{{url('/assets/images/slideleft.jpg')}}" alt="Branding for Menu">
                        </div>

                        <div class="col-sm-9 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                            <article>
                                <h2 class="orange-title left landing-orange-titles-margin">Wollner Bio</h2>
                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                        <div class="copy-block-gray text-center tk-futura-pt">
                                            <p class="text-left bio-michael">Michael K. Wollner is a master architect of strategies, simple designs and experiences. Over the past three decades, Michael has worked as an art director, creative director and branding expert with high profile agencies such as Ogilvy and DDB Needham. His extensive client roster included major corporations such as American Express, McDonnell Douglas/Boeing, Epson, Mitsubishi, TRW, Castle Rock Entertainment, Marriott, Nutrasweet, government giants such as NASA, and charities such as Southern California Special Olympics.
                                                <br><br>Michael has served on several judging panels for international advertising award shows, including the prestigious Clio Awards. He has received honors from the International Advertising Festival (IAF) for outstanding achievement in print and interactive design, the New York Festivals international competition for outstanding achievement in interactive multimedia design and other design awards from Europe and the U.S.

                                                Michael is a past instructor of Interface design and illustration at the American Film Institute and has served as a panelist and speaker during such events as the Hollywood Film Festival, and the premier Hotel Lodging Conference. He was also a key player in the development of the first ever digital proposal for the new X-33 reusable launch rocket program at McDonnell Douglas.
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container-fluid-table display-t-sm width-100">
                <div class="row-table display-t-r-sm">
                    <div class="col-sm-6 display-t-c-sm pad-l-0 pad-r-0 vertical-a-b-sm wow rotateInUpRight fadeIn" data-wow-offset="100" data-wow-delay="100ms">
                        <img class="wid-100 hidden-xs" src="{{url('/assets/images/partner-with-our-orange-county-team.jpg')}}" alt="Online Marketing Experts">
                        <img class="wid-100 visible-xs" src="{{url('/assets/images/partner-with-our-orange-county-team.jpg')}}" alt="Online Marketing Experts">
                    </div>

                    <div class="col-sm-6 col-sm-6 display-t-c-sm background-c-shade-1 vertical-a-m-sm copy-block-gray-text ">
                        <article>
                            <h2 class="orange-title left landing-orange-titles-margin">Connect With Our Agency</h2>
                            <div class="row">
                                <div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 copy-block-gray-col orange-offset">
                                    <div class="copy-block-gray tk-futura-pt">
                                        <p>We would love to hear more about your marketing challenges, and share how we’ve helped other companies in similar situation. To start the conversation, please <a href="/contact">contact us</a> and begin the marketing journey together.
                                    </div>

                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <div align="center">
            <img class="img-responsive" src="{{url('/assets/images/our-search-engine-optimization-clients-get-results.jpg')}}" alt="" style="PADDING-TOP: 20px; PADDING-BOTTOM: 20px;">
        </div>
    </div>
@endsection
