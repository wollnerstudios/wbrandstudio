@if(app()->isLocal())
	<script src="{{ asset('js/app.js') }}"></script>
@else
	<script src="/js/app.js"></script>
@endif

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->

<script>(function(document, tag) { var script = document.createElement(tag); var element = document.getElementsByTagName('body')[0]; script.src = 'https://acsbap.com/api/v1/assets/js/acsb.js'; script.async = true; script.defer = true; (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script); script.onload = function() { AccessiBe.init({ statementLink : '', feedbackLink : '', showAllActions : false, keyNavStrong : false, hideMobile : false, hideTrigger : false, wlbl : 'accessiBe', language : 'en', focusInnerColor : '#146FF8', focusOuterColor : '#ff7216', leadColor : '#146FF8', triggerColor : '#146FF8', size : 'big', position : 'right', triggerRadius : '50%', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerIcon : 'default', triggerSize : 'medium', triggerOffsetX : 20, triggerOffsetY : 20, usefulLinks : { }, mobile : { triggerSize : 'small', triggerPositionX : 'right', triggerPositionY : 'bottom', triggerOffsetX : 0, triggerOffsetY : 0, triggerRadius : '0' } }); };}(document, 'script'));</script>

<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->