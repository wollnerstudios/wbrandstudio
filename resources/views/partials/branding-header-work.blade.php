<section class="branding-section position-r">
	<div class="background-c-shade-2 hidden-xs slide-in-gallery-box"></div>		
	<div class="container-fluid-table display-t-xs width-100">
		<div class="row-table display-t-r-xs">
			<a href="/" class="col-sm-10-percent z-i-2 wave branding-wave brand-logo-box col-xs-4 waves-effect display-t-c-xs text-center padding-t-1 padding-b-1 background-c-accent">
				<img class=" display-i-b-xs wollner-branding-logo width-100" src="/images/wollner-logo-2023-w.svg" alt="">
			</a>
			<div  class="col-sm-10-percent col-sm-4 hidden-xs background-c-shade-4 waves-effect email-box vertical-a-m-xs text-center display-t-c-xs">
				<i class="fa fa-envelope"></i>
			</div>
			<div  class="col-sm-10-percent col-xs-4  hidden-sm hidden-md hidden-lg background-c-shade-4 waves-effect vertical-a-m-xs text-center display-t-c-xs">
				<div class="menu-button dis-i-b-xs">
					<i class="fa fa-bars fa-3x"></i>
				</div>
			</div>
			<div class="page-title-box z-i-2  col-sm-30-percent col-xs-4 background-c-shade-2 display-t-c-xs hidden-xs vertical-a-m-xs text-center">
				<h1 class="hidden-xs">@yield('brandTitle')</h1>
			</div>
			<div class="col-sm-6 background-c-white long-header-style display-t-c-xs hidden-xs vertical-a-m-xs padding-l-0 padding-r-0 ">
			
			</div>	
			<div class="col-xs-4 background-c-white display-t-c-xs visible-xs-tc text-center">
				<a class="phone-number visible-xs" href="tel:6572320110"><i class="fa fa-phone"></i></a>
			</div>
		</div>
	</div>
</section>
