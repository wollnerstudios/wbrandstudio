<section class="branding-section">
    <div class="container-fluid-table display-t-xs width-100">
        <div class="row-table display-t-r-xs">
            <a href="/" class="col-sm-10-percent col-xs-4 waves-effect brand-logo-box display-t-c-xs text-center padding-t-1 padding-b-1 background-c-accent" style="background-color:@yield('brandColor');">
                <img itemprop="image" class="img-responsive display-i-b-xs wollner-branding-logo" src="/images/wollner-logo-2023-w.svg" alt="">
            </a>
            <div  class="col-sm-10-percent col-sm-4 hidden-xs background-c-shade-4 waves-effect email-box vertical-a-m-xs text-center display-t-c-xs">
                <i class="fa fa-envelope"></i>
            </div>
            <div  class="col-sm-10-percent col-xs-4  hidden-sm hidden-md hidden-lg background-c-shade-4 waves-effect vertical-a-m-xs text-center display-t-c-xs">
                <div class="menu-button dis-i-b-xs">
                    <i class="fa fa-bars fa-4x" style="color: white;"></i>
                </div>
            </div>
            <div class="page-title-box col-sm-30-percent col-xs-4 background-c-shade-2 display-t-c-xs hidden-xs vertical-a-m-xs text-center">
                <h1 class="hidden-xs">@yield('brandTitle')</h1>
            </div>
            <div class="col-sm-6 background-c-white display-t-c-xs hidden-xs vertical-a-m-xs long-header-style capabilities-branding-image"
                 style="background:url('@yield('brandHeaderImage')');">

            </div>
            <div class="col-xs-4 background-c-white display-t-c-xs visible-xs-tc text-center vertical-a-m-xs">
                <a class="phone-number visible-xs" href="tel:+1-714-754-5475"><i class="fa fa-phone"></i></a>
            </div>
        </div>
    </div>
</section>




