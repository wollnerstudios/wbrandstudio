<header class="main-header pad-t-1 pad-b-1 hidden-xs" style="visibility:hidden;position:static !important;">
    <div class="container-fluid">
        <div class="col-lg-6 col-md-5 col-sm-4 pad-l-0">
            <div class="menu-button dis-i-b-xs">
                <i class="fa fa-bars"></i>
                <span class="menu-text">Menu</span>
            </div>
        </div>
    </div>
    <script src="https://use.typekit.net/vkv5awm.js"></script>
    <script>
    try {
        Typekit.load({async: true});
    } catch (e) { }
    </script>
</header>

<header class="main-header pad-t-1 pad-b-1 hidden-xs">
    <div class="container-fluid">
        <div class="col-lg-6 col-md-5 col-sm-4 pad-l-0">
            <div class="menu-button dis-i-b-xs">
                <i class="fa fa-bars"></i>
                <span class="menu-text">Menu</span>
            </div>
        </div>
        <div class="col-lg-6 col-md-7 col-sm-8 hidden-xs pad-r-0">
            <div class="header-item pull-left">
                <span class="slogan-text">
                    <span itemprop="name">WOLLNERSTUDIOS</span>- totally branding. Since 1997</span>
            </div>
            <div class="header-item pull-right">
                <a  class="phone-number" href="tel:+1-657-232-0110">
                    <span itemprop="telephone">657 . 232 . 0110</span></a>
            </div>
        </div>
    </div>
</header>