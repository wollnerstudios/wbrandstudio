<footer class="main-footer">

    <div class="container visible-xs text-center mobile-footer-section">
        <div class="row">
            <div class="col-xs-12 padding-t-2 padding-b-1">
                <a class="footer-email" href="mailto:hello@wbrandstudio.com">hello@wbrandstudio.com</a>
            </div>
            <!--<div class="col-xs-12 padding-t-1 padding-b-2">
                W BRAND STUDIO - TOTAL BRANDING<br>
                BASED IN COSTA MESA, CALIFORNIA<br>
                DOING BUSINESS IN EVERY STATE
            </div>-->
            <div class="col-xs-12" style="text-transform: none!important;">
                WollnerStudios, Inc. dba W Brand Studio.
            </div>
            <div class="col-xs-12 padding-b-2" style="text-transform: none!important;">
                Full-service Branding and Marketing Agency in Orange County and Los Angeles.
            </div>
            <div class="col-xs-12 padding-b-2" style="text-transform: none!important;">
                <span>&copy; {{date('Y')}} WollnerStudios, Inc.</span> <br>
                <span><a style="color:#fff" href="{{url('disclaimer')}}">Terms of Use and Disclaimer </a></span> |
                <span><a style="color:#fff" href="/privacy"> Privacy Policy</a></span>
            </div>
        </div>
    </div>
    <div class="container-fluid dis-t-sm hidden-xs">
        <div class="row dis-t-r-sm">
            <div class="col-sm-5 left-footer-side dis-t-c-sm ver-a-m-sm pull-none">
                <span>Branding
                    <img src="{{url('/assets/images/branding-since.png')}}" alt="">
                    SEO & PPC
                </span>
            </div>
            <div class="col-sm-7 right-footer-side dis-t-c-sm ver-a-m-sm pull-none text-right"  style="text-transform: none!important;">
                <span>WollnerStudios, Inc. dba W Brand Studio.</span> <br>
                <span>Full-service Branding and Marketing Agency in Orange County and Los Angeles.</span> <br>
                <span>&copy; {{date('Y')}} WollnerStudios, Inc.</span> <br>
                <span><a style="color:#fff" href="{{url('disclaimer')}}">Terms of Use and Disclaimer </a></span> | 
                <span><a style="color:#fff" href="/privacy"> Privacy Policy</a></span>
            </div>
        </div>
    </div>

</footer>


<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>
    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <meta itemprop="streetAddress" content="20250 SW Acacia St #130">
        <meta itemprop="addressLocality" content="Newport Beach">
        <meta itemprop="addressRegion" content="California">
        <meta itemprop="addressCountry" content="United States">
        <meta itemprop="postalCode" content="92660"></span>
        <span itemprop="openingHoursSpecification" itemscope itemtype="http://schema.org/OpeningHoursSpecification">
        <span itemprop="dayOfWeek" itemscope itemtype="http://schema.org/DayOfWeek">
        <meta itemprop="name" content="Mo-Fri"></span>
            <meta itemprop="opens" content="09:00">
        <meta itemprop="closes" content="17:00"></span>
