<nav class="main-nav">
    <div class="dis-t-xs wid-100" style="height:100%">
        <div id="nav-close-button">
            <div>x</div>
        </div>
        <div class="dis-t-c-xs ver-a-m-xs ">
            <div class="main-nav-panel tk-futura-pt text-center">
                <ul>

                    <li class="main-nav-panel-li waves-effect"><a href="/about">about</a></li>
                    <li class="main-nav-panel-li">
                        <ul class="collapsible" data-toggle="collapse">
                            <li>
                                <div class="collapsible-header waves-effect"><a data-toggle="collapse" href="#capabilities-collapse">capabilities</a></div>
                                <div class="collapsible-body collapse" id="capabilities-collapse">
                                    <ul>
                                        <li><a href="/capabilities/index">Overview / Quick Facts &raquo;</a></li>
                                        <li><a href="/capabilities/branding">Branding &raquo;</a></li>
                                        <li><a href="/capabilities/brand-naming">Brand Naming &raquo;</a></li>
                                        <li><a href="/capabilities/marketing">Marketing &raquo;</a></li>
                                        <li><a href="/capabilities/seo-sem">SEO / PPC (SEM) &raquo;</a></li>
                                        <li><a href="/capabilities/socialmedia">Social Media &raquo;</a></li>
                                        <li><a href="/capabilities/strategy">Strategy &raquo;</a></li>
                                        <li><a href="/capabilities/visual-identity">Visual Identity / Design &raquo;</a></li>
                                        <li><a href="/capabilities/website-design">Website Development &raquo;</a></li>
                                        <li class="main-nav-panel-li waves-effect"><a href="/logo-design">logo design</a></li>
                                        <li class="main-nav-panel-li waves-effect"><a href="/interior-design">interior design</a></li>
                                        <li class="main-nav-panel-li waves-effect"><a href="/commercials-video-production">video production</a></li>


                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="main-nav-panel-li waves-effect"><a href="/work">work</a></li>
                    <li class="main-nav-panel-li waves-effect"><a href="/testimonials">testimonials</a></li>
                    <li class="main-nav-panel-li waves-effect"><a href="/award-winning-agency">news/press releases</a></li>
                    <li class="main-nav-panel-li waves-effect"><a href="/blog">blog</a>

                    <li class="main-nav-panel-li waves-effect"><a href="/contact">contact</a></li>

                    {{--<li class="main-nav-panel-li">--}}
                        {{--<ul class="collapsible" data-toggle="collapse">--}}
                            {{--<li>--}}
                                {{--<div class="collapsible-header waves-effect"><a data-toggle="collapse" href="#capabilities-two-collapse">contact</a></div>--}}
                                {{--<div class="collapsible-body collapse" id="capabilities-two-collapse">--}}
                                    {{--<ul>--}}
                                        {{--<li><a href="/write-to-us">Write to Us &raquo;</a></li>--}}
                                        {{--<li><a href="/contact">Join Us &raquo;</a></li>--}}
                                        {{--<li><a href="/careers">Careers &raquo;</a></li>--}}

                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </div>
            <div class="main-nav-social">
                <hr>
                <a target="_blank" href="https://www.facebook.com/WBrandStudio?fref=ts"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="https://twitter.com/WBrandStudio"><i class="fa fa-twitter"></i></a>
                <a target="_blank" href="https://www.linkedin.com/company-beta/17884443/"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</nav>
