<!DOCTYPE html>
<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<html lang="en" class="@yield('customHTMLClass')">
    <head>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MBPM9SG');</script>
        <!-- End Google Tag Manager -->


        <!-- Bing Verification -->
        <meta name="msvalidate.01" content="CB945FD7AF5FE99662DD9CE14EB4CFB1" />


        <meta charset="utf-8">
        <meta name="msvalidate.01" content="1AC96880B2817F4B80A980A1E20B6473"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="@yield('description')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <title>@yield('title')</title>


        <!-- Robot Tag -->
        <meta name="robots" content="noindex">




        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{url('favicon.ico?v=1.0.0')}}">
        <meta name="author" content="W Brand Studio, Inc - W Brand Studio">
        <meta name="keywords" content="@yield('keywords')"/>

        <meta name="distribution" CONTENT="Global"/>
        <meta name="location" content="Newport Beach, Orange County, California"/>
        <meta name="location" content="OC, LA, CA"/>
        <meta name="abstract" content="@yield('abstract')"/>

        <!--Geo tag Generated -->
        <meta name="geo.region" content="US-CA"/>
        <meta name="geo.placename" content="Newport Beach"/>
        <meta name="geo.position" content="33.658606;-117.878111"/>

        <meta name="ICBM" content="33.658606, -117.878111"/>

        <!-- social meta -->
        <meta property="og:title" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:type" content="website"/>
        <meta property="og:description" content=""/>
        <meta property="og:image" content=""/>
        <meta property="fb:app_id" content="1003087983065455"/>
        <meta property="article:author" content="https://www.facebook.com/wbrandstudio"/>
        <meta property="article:publisher" content="https://www.facebook.com/wbrandstudio"/>

        <!-- twitter card -->
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@wollnerstudios"/>
        <meta name="twitter:creator" content="@wollnerstudios"/>
        <meta name="twitter:title" content=""/>
        <meta name="twitter:description" content=""/>
        <meta name="twitter:image" content=""/>
        <meta name="twitter:url" content=""/>




        @include('partials.header-js-css')

        <script async src="https://www.google.com/recaptcha/api.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        

        @include('partials.google-analytics')

        @if(app()->isLocal())
            <link href="{{asset('css/app.css')}}" rel="stylesheet">
        @else
            <link href="{{asset('css/app.css')}}" rel="stylesheet">
        @endif

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MBPM9SG"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div itemscope itemtype="http://schema.org/LocalBusiness" class="site-wrapper">
            @include('partials.header')
            @include('partials.nav')

            {{--@if (!Request::is('/') && !Request::is('/contact'))--}}
                {{--@include('partials.contacttab')--}}
            {{--@endif--}}

            <div class="page-wrap historical-container">
                @yield('brandingHeader')

                @yield('content')
            </div>

            @include('partials.footer')
        </div>

        <div id="overlay"></div>

        @include('partials.scripts')




    </body>
</html>
