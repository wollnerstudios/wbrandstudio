const WOW = require('./vendor/wow/wow.min').WOW;

$(function () {
    new WOW({
        callback: function (box) {
            $(box).removeClass('opacity-0');
            $(box).find('.opacity-0').removeClass('opacity-0')
        },
    }).init();
});

// $.extend($.lazyLoadXT, {
//   edgeY:  -100,
//   srcAttr: 'data-src'
// });
$.fn.redraw = function () {
    $(this).each(function () {
        var redraw = this.offsetHeight;
    });
};
$.fn.attrArray = function (options) {

    if (!this.length) {
        return;
    }

    var settings = $.extend({
        attr: '',
        split: ',',
        onComplete: null
    }, options);

    var attr = settings.attr,
        split = settings.split,
        result = this.attr(attr).split(split);
    newResult = [];
    $.each(result, function (key, value) {
        newResult.push(value);
    });

    settings.onComplete(newResult);

    return newResult;
};

function getScrollBarWidth() {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;

    if (w1 == w2) {
        w2 = outer.clientWidth;
    }

    document.body.removeChild(outer);

    return (w1 - w2);
}

$(function () {
    $('.menu-button').click(function () {
        var scrollBarWidth = getScrollBarWidth();

        $('body').toggleClass('nav-open');
        $('.page-wrap').addClass('moving');
        $('.main-header').addClass('moving');
        $('.site-wrapper').css('margin-right', scrollBarWidth + "px");
        $('.slide-in-gallery-box').fadeOut();
    });
    $('#nav-close-button').click(function () {
        $('body').removeClass('nav-open');
        $('.slide-in-gallery-box').fadeIn(0.1);
        setTimeout(redraw, 500);

        function redraw() {
            setTimeout(function () {
                $('.branding-section').offsetHeight;
            }, 300)
        }


        $('.site-wrapper').css('margin-right', "");
    });
});

$(function () {

    // $('.read-more-testimonials-button').magnificPopup({


    //  callbacks: {
    //    beforeOpen: function() {
    //       this.st.mainClass = this.st.el.attr('data-effect');
    //    }
    //  },
    // });

    $('#contact-contact-block').click(function () {
        setTimeout(function () {
            $('body').addClass('contact-open');
        }, 500)
    });
    $('.contact-form-close-button').click(function () {
        $('body').removeClass('contact-open');
    });






});
$('.brand-logo-box').click(function (e) {
    e.preventDefault();
    setTimeout(function () {
        window.location = '/';
    }, 500)
});
$('.email-box').click(function (e) {
    e.preventDefault();
    setTimeout(function () {
        window.location = '/contact';
    }, 500)
});

$('.branding-wave, .main-nav-panel-li a').click(function (e) {
    e.preventDefault();
    var goTo = this.getAttribute("href");
    setTimeout(function () {
        window.location = goTo;
    }, 500)
});

$(document).ready(function () {
    /* 	for (i = 0; i < 51; i++) {
     $('.long-header-style').css({'width':i+'%'});
     }	 */
    $('.long-header-style').animate({
        width: "50%"
    });

    // Waves.attach('.wollner-branding-logo', ['waves-button']);
    // Waves.attach('.filter', ['waves-block']);
    // Waves.attach('.main-nav-panel-li', ['waves-block']);
    // Waves.attach('.contact-block', ['waves-classic']);
    // Waves.attach('.email-box', ['waves-block']);
    // Waves.attach('.work-button-button',['waves-light']);
    // Waves.init();
    // var wow = new WOW({

    // 	callback:function(box) {
    // 		$(box).removeClass('opacity-0')
    // 		$(box).find('.opacity-0').removeClass('opacity-0')
    // 	},

    // });
    // wow.init();

    if ($('html').hasClass('work-page')) {

        $galleryWork = $('#slick-gallery-work');
        $galleryThumb = $('.slick-gallery-thumb');
        initializeSliders($galleryWork, $galleryThumb);
        galleryWorkHandler();
        galleryThumbHandler();
        initializeSliderMobile();
    }

});

$(document).ready(function () {

    $testimonialBlock = $('.testimonial-interactive-block');
    $testimonialCopy = $('.testimonial-interactive-block-copy');
    $testimonialImage = $('.testimonial-interactive-block-image');
    $readmorebutton = $('.read-more-testimonials-button');

    $readmorebutton.click(function () {
        $(this).closest('.testimonial-interactive-block').toggleClass('expanded');

        //$(this).closest('.testimonial-interactive-block').toggleClass('position-a');
        //$(this).text('close');
        $(".testimonial-scrollbar").scrollTop(0);
    });

    $('.testimonial-close-button').click(function () {
        $(this).closest('.testimonial-interactive-block').removeClass('expanded');
        $(".testimonial-scrollbar").scrollTop(0);
    });

    $('#slick-gallery-work').on('click', '.button-here', function (e) {
        $('html').addClass('gallery-open');
        $('.gallery-open-close-section').removeClass('pulse animated');

    });

});
function initializeSliderMobile() {

    $slickSliderWorkMobile = $('#slick-slider-work-mobile');

    $slickSliderWorkMobile.slick({
        infinite: false,
        arrows: false
    });

}
function initializeSliders($galleryWork, $galleryThumb) {

    $galleryWork.slick({
        infinite: false,
    });

    $galleryWork.on('afterChange', function (slick, currentSlide) {

        current = currentSlide.currentSlide;

        match = currentSlide.slideCount - 1;

        if (current == match) {
            $('.gallery-open-close-section').addClass('pulse animated');
        }

    });

    $galleryThumb.slick({
        infinite: false,
    });

    $galleryThumb.mixItUp({

        animation: {
            effects: 'fade',
            enable: false
        },

        selectors: {
            filter: '.mixfilter',
        },
        callbacks: {
            onMixLoad: function () {

                setTimeout(function () {
                    $('.work-gallery-section.invis-0').removeClass('invis-0');

                }, 200);

                setTimeout(function () {

                    $('.workName-page .invis-0').removeClass('invis-0')

                }, 500);

                setTimeout(function () {
                    $('.gallery-open .invis-0').removeClass('invis-0')
                }, 2000);

                $(this).mixItUp('setOptions', {
                    animation: {
                        enable: true,
                        effects: 'fade'
                    },

                });

            },

            // onMixEnd: function(){
            // 	console.log('end')
            // 	$('#gallery-container .mix').css({'margin-right':0})
            // }
        }
    });

}
$(document).ready(function () {
    $('.bio-button').click(function () {
        $(' #slide-in-about').removeClass('slideOutRight invis-0');
        $('#slide-in-about').addClass('slideInRight');

    });
    $('#about-close').click(function () {
        $('#slide-in-about').removeClass('slideInRight');
        $('#slide-in-about').addClass('slideOutRight');

    });
    // $('.form-close-button').click(function(){
    // 	$('.form-slide-section').removeClass('slideInLeft');
    // 	$('.form-slide-section').addClass('slideOutLeft');
    // });
});

function galleryWorkHandler() {

    $('.filter').click(function () {
        $('.filter.active').removeClass('active');
        $(this).addClass('active');
    });


    $('.work-button-button').click(function () {

        if ($('html').hasClass('gallery-open')) {

            $('html').removeClass('gallery-open');

        } else {

            $('html').addClass('gallery-open');

        }

    });


    $('.take-a-look').on('click', function () {
        $('#slick-gallery-work, .hide-this-box-for-gallery').removeClass('invis-0');
        $('#slick-gallery-work .slick-slide').each(function (key, value) {

            $galleryWork.slick('slickRemove', 0, false);

        });
        var url = $(this).attr('href');
        history.replaceState({}, 'work', url);
        $('html').removeClass('gallery-open');

        $(this).parent().siblings('img').attrArray({
            attr: 'data-main-img',
            split: ',',
            onComplete: function (val) {

                $.each(val, function (key, value) {

                    $galleryWork.slick('slickAdd', '<div><img class="width-100" src="' + value + '" alt=""></div>');
                });
            }
        });
        $description = $(this).parent().siblings('img').attr('data-main-description');
        $('.work-description-text').html($description);
        return false;
    });

}

function galleryThumbHandler() {
    var i = 1;
    var n = 0;

    var categoryAllImageArray = [];
    var categoryOneImageArray = [];
    var categoryTwoImageArray = [];
    var categoryThreeImageArray = [];
    var categoryFourImageArray = [];
    var categoryFiveImageArray = [];
    var categorySixImageArray = [];
    var recentlyClickedImageArray = [];

    var filterSlides;

    $('#gallery-container .category-all').each(function (index, value) {
        categoryAllImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-1').each(function (index, value) {
        categoryOneImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-2').each(function (index, value) {
        categoryTwoImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-3').each(function (index, value) {
        categoryThreeImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-4').each(function (index, value) {
        categoryFourImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-5').each(function (index, value) {
        categoryFiveImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container .category-6').each(function (index, value) {
        categorySixImageArray.push({
            value: value,
            parent: value.parentElement,
        });
    });

    $('#gallery-container').on('mixEnd', function (e, state) {
        $galleryThumb.slick('slickFilter', filterSlides)
    });

    function moveImages(array) {

        $galleryThumb.slick('slickGoTo', 0);

        $.each(recentlyClickedImageArray, function (key, value) {

            $(value.value).appendTo(value.parent)

        });

        if (array.length <= 4) {
            while (array.length != 5) {
                array.push({value: '<div class="gallery-thumb-image mix" style="display:inline-block"><img style="visibility:hidden" src="/assets/images/work/thumbnails/Branding/Ellis-branding.jpg"></div>'})
            }
        }

        $.each(array, function (key, value) {

            $('.slide-' + n + '').append(value.value);

            if (i % 8 == 0) {
                n++;
            }

            i++;
        });

        n = 0;

        i = 1;

        recentlyClickedImageArray = array;

        var slideLeft = Math.ceil((array.length / 8));

        filterSlides = [];

        for (p = 0; p < slideLeft; p++) {
            filterSlides.push('.slide-' + p);
        }

        filterSlides = filterSlides.join(", ");
    }


    $('.cat-1-filter').click(function (e) {

        if ($galleryThumb.mixItUp('isMixing')) {
            //
        } else {
            $galleryThumb.slick('slickUnfilter');
            setTimeout(handleMoveImages('.category-1', categoryOneImageArray), 100);
        }
    });

    function handleMoveImages(category, array) {
        if ($galleryThumb.mixItUp('isMixing')) {
            //
        } else {
            setTimeout(initiateMoveImages(category, array), 100);
        }
    }

    function initiateMoveImages(category, array) {
        $('#gallery-container').mixItUp('filter', category);
        moveImages(array);
    }

    $('.cat-2-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-2', categoryTwoImageArray), 100);
    });

    $('.cat-3-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-3', categoryThreeImageArray), 100);
    });

    $('.cat-4-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-4', categoryFourImageArray), 100);
    });

    $('.cat-5-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-5', categoryFiveImageArray), 100);
    });

    $('.cat-6-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-6', categorySixImageArray), 100);
    });

    $('.cat-all-filter').click(function () {
        $galleryThumb.slick('slickUnfilter');
        setTimeout(handleMoveImages('.category-all', categoryAllImageArray), 100);
    });
}

// Prevent link click jumping on homepage

$(function () {
    $('.take-a-look').on("click", function (e) {
        // e.preventDefault();
    });
});
