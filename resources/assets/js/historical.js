(function ($) {

	$.fn.historical = function (options) {

		var url,
			data = [],
			responseData,
			$container,
			initialState,
			itemSelector,
			innerHTML,
			replaceDelay,
			$navSelector = this;

		$.fn.historical.defaults = {

			innerHTML: true,
			itemSelector: 'section',
			container: 'section',
			documentReady: function () {},
			beforeReplace: function () {},
			afterReplace: function () {},
			onPopChange: function () {}
		};

		var opts = $.extend({}, $.fn.historical.defaults, options);
		itemSelector = opts.itemSelector;
		$container = $(opts.container);
		innerHTML = opts.innerHTML;
		initialState = $container.html();

		opts.documentReady.call(this)

		$navSelector.click(function () {

			data = [];
			url = $(this).attr('href');

			replaceDelay = $(this).attr('data-historical-delay');

			if (typeof replaceDelay === 'undefined') {
				replaceDelay = 0;
			}

			if (url == window.location.toString()) {
				return false;
			}

			getDatas(url, itemSelector, function (responseData) {

				$.each(responseData, function (key, value) {
					if (innerHTML) {
						data.push(value.innerHTML);
					} else {
						data.push(value.outerHTML);
					}
				});

				opts.beforeReplace.call(this, data)

				setTimeout(function () {

					$container.html(data);
					opts.afterReplace(data)
					opts.documentReady.call(this)
					history.pushState(data, null, url);

				}, replaceDelay)
			});

			return false;

		});

		function getDatas(url, dom, callback) {
			return $.ajax({

				url: url,
				type: 'GET',
				success: function (responseData) {
					var $source = $('<div>' + responseData + '</div>');
					callback($source.find(dom));
				},
			});
		}

		var popped = ('state' in window.history), initialURL = location.href

		$(window).bind("popstate", function (e) {

			var initialPop = !popped && location.href == initialURL
			popped = true
			if (initialPop) return

			popData = e.originalEvent.state;

			if (popData == null) {
				$container.html(initialState);
				opts.onPopChange(popData);
				opts.documentReady.call(this);

			} else {

				$container.html(popData);
				opts.onPopChange(popData);
				opts.documentReady.call(this);
			}
		});
	}

}(jQuery));