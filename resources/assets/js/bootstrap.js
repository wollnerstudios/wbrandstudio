window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
require('node-waves');
require('slick-carousel/slick/slick.js');

require('./vendor/jquery.mixitup.min');
require('./vendor/viewportHack');

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// let token = document.head.querySelector('meta[name="csrf-token"]');
