<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');

//Route::get('/index.html', function () {
//    return redirect('/');
//});

Route::get('/about', 'PagesController@about');
Route::get('/commercials-video-production', 'PagesController@commercialsVideoProduction');
Route::get('/interior-design', 'PagesController@interiorDesign');
Route::get('/logo-design', 'PagesController@logoDesign');
Route::get('/capabilities', 'CapabilitiesController@index');
Route::get('/capabilities/{capability}', 'CapabilitiesController@show');
Route::get('/work', 'WorkController@index');
Route::get('/work/{slug}', 'WorkController@show');
Route::get('/testimonials', 'PagesController@testimonials');
Route::get('thankyou', 'PagesController@thankyou');

Route::get('clients', ['as' => 'clients', function () {
    return view('clients', ['routeName' => 'clients']);
}]);

Route::get('contact', 'ContactController@index');
// STEP 2: FORM IS ROUTED TO THE CONTACTCONTROLLER'S SUBMIT FUNCTION
Route::post('contact', 'ContactController@submit');
Route::post('consultation', 'ContactController@submit');
Route::post('website-contact-submit', 'ContactController@websiteContact');

Route::get('write-to-us', 'PagesController@writeUs');
Route::get('careers', 'PagesController@careers');
Route::get('video', 'PagesController@video');
Route::get('increase-website-traffic', 'PagesController@increaseWebsiteTraffic');
Route::get('interview', 'PagesController@interview');
Route::get('digital-marketing-agency', 'PagesController@digitalMarketingAgency');
Route::get('award-winning-agency', 'PagesController@awardWinningAgency');
Route::get('marketing-firm', 'PagesController@marketingFirm');
Route::get('hospitality-marketing', 'PagesController@hospitalityMarketing');
Route::get('brand-agency', 'PagesController@branding');
Route::get('branding-agency', 'PagesController@brandingAgency');
Route::get('branding-agency-oc', 'PagesController@brandingAgencyOC');
Route::get('branding-company', 'PagesController@brandingCompany');
Route::get('branding-marketing-experts', 'PagesController@brandingMarketingExperts');
Route::get('branding-agency-ggl', 'PagesController@brandingAgencyGgl');
Route::get('branding-firm', 'PagesController@brandingFirm');
Route::get('advertising-agency', 'PagesController@advertisingAgency');
Route::get('branding-philosophy', 'PagesController@brandingPhilosophy');
Route::get('brand-refresh', 'PagesController@brandRefresh');
Route::get('/disclaimer', 'PagesController@disclaimer');
Route::get('/marketing-agency', 'PagesController@marketingagency');
Route::get('/top-marketing-agency', 'PagesController@topmarketingagency');
Route::get('/online-marketing-agency-in-orange-county-ca', 'PagesController@onlineMarketing');
Route::get('/custom-orange-county-website-builder-and-design-solutions', 'PagesController@customWebsiteDesigners');
Route::get('/modern-seo-companies-in-orange-county-for-search-engine-services', 'PagesController@searchEngineExperts');
Route::get('/full-service-marketing-agency-firm-in-orange-county-ca', 'PagesController@marketingAgencyLanding');
Route::get('/orange-county-website-builder-and-design-solutions', 'PagesController@customWebsiteDesigners');
Route::get('fastest-growing-branding-agency', 'PagesController@fastestGrowingBrandingAgency');
Route::get('biggest-small-agency', 'PagesController@biggestSmallAgency');
Route::get('top-branding-agency', 'PagesController@topBrandingAgency');
Route::get('pure-branding', 'PagesController@pureBranding');
Route::get('winning', 'PagesController@winning');
Route::get('madmen', 'PagesController@madmen');
Route::get('/web-design', 'PagesController@webDesign');
Route::get('/v/website-design', 'PagesController@vWebsiteDesign');
Route::get('/blog', 'BlogController@blog');
Route::get('/blog/blog-post/{id}', 'BlogController@blogpost');
Route::get('/privacy', 'PagesController@privacy');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
