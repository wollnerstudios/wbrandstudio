<?php

return [

    [
        'slug' => 'renew',
        'seoTitle' => 'Renew Landscapes | Branding and Website Design',
        'seoDescription' => 'When a high-end landscaping service in Long Beach needed to re-brand, they turned to the creative team at W Brand Studio for the right help.',
        'seoKeywords' => 'logo design, landscape marketing, vehicle wrap, website design, long beach marketing',
        'categories' => ['category-1', 'category-3', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/renew-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/renew-1.jpg', 'renew'],
            ['/assets/images/work/Large_Photos/renew-2.jpg', 'renew'],
            ['/assets/images/work/Large_Photos/renew-3.jpg', 'renew'],
        ],
        'mainDescription' => '
		We were tasked to create a high-end brand for a local landscaping service in Bixby Knolls, Long Beach, CA. Tasks included: branding, logo, website development, print and vehicle wrap. The website has received platinum awards from both The AVA Awards and Hermes Creative Awards by the Association of Marketing and Communication Professionals. <a href="/capabilities/website-design"><u>See why it is important to work with an award winning agency.</u></a>',
        'hover-effect-description' => 'Local branding, logo and website"',
        'hover-effect-client-name' => 'Renew Landscapes',

    ],

    [
        'slug' => 'Marriott',
        'seoTitle' => 'Marriott Downtown Los Angeles | Sub-Branding Project with W Brand Studio',
        'seoDescription' => 'Learn how we helped the Marriott Downtown Los Angeles with creating a familiar marketing touch point that communicates continuity of service beyond normal expectations.',
        'seoKeywords' => 'hotel branding, corporate marketing, Marriott marketing project, Los Angeles creative agency, re-branding, hotel marketing',
        'categories' => ['category-1', 'category-3', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/Marriott-local.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/marriott-5.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-tagline.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-4.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-ad-1.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-print-1.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-print-2.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-ad-2.jpg', 'marriot'],
            ['/assets/images/work/Large_Photos/marriott-ad-3.jpg', 'marriot'],
        ],
        'mainDescription' => '
		We generated a trademark tagline: "You\'re not a Stranger Here." The tagline was designed to explicate a specific and unique relationship with all of the hotel\'s guests. It was also reminiscent of the core brand promise of the "Marriott Way" while emphasizing the fact that the hotel is local and that it caters to local markets, offering a familial touch point that communicates continuity of service beyond normal expectations. The main touch point for us became the human touch. Using the employees as part of the hotel marketing turned into a success both internally for the employee moral, and externally for guests. ',
        'hover-effect-description' => 'Local branding, marketing, web, advertising',
        'hover-effect-client-name' => 'Marriott Hotel Downtown LA',

    ],

    [
        'slug' => 'yamaha-keyboard-division',
        'seoTitle' => 'Yamaha Piano | Motion Graphics and Website Development',
        'seoDescription' => 'For over 15 years, Yamaha has been partnering with W Brand Studio for print design, motion graphics and website development to improve their marketing.',
        'seoKeywords' => 'motion graphics, brochure design, website development, marketing agency, orange county, newport beach',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/yamaha-keyboard-division.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/Yamaha-CVP600.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Yamaha-CVP600-w2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Yamaha-CVP600-w3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Yamaha-CVP600-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Yamaha-photoshoot.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/yamaha-avantgrand-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/yamaha-avantgrand-2.jpg', 'alt'],
        ],
        'mainDescription' => '
		We have worked directly with Yamaha and MIDItainment for 15 years, creating everything from print design and motion graphics to website development and digital assets. Here are just a few of the projects we have had the opportunity to work on.',
        'hover-effect-description' => 'web, print, video graphics',
        'hover-effect-client-name' => 'Yamaha Piano Division',

    ],

    [
        'slug' => 'baci-di-roma',
        'seoTitle' => 'Baci di Roma Restaurant | Website Development and Photography',
        'seoDescription' => 'We updated the clients website to reflect the warm atmosphere and feel of the restaurant, which also led to more traffic on the site, and more traffic into the restaurant.',
        'seoKeywords' => 'restaurant marketing, restaurant website design, branding, photography, photography, web developers, orange county',
        'categories' => ['category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/Baci-di-Roma-1-5.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/baci-web-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/baci-print-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/baci-print-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/baci-photo-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/baci-photo-2.jpg', 'alt'],
        ],
        'mainDescription' => '
		The old site was invisible to search engines and did not reflect it\'s brick and mortar counterpart. We knew that the site needed to reflect the atmosphere and feel of the restaurant.  What we did not expect was that people now stayed on the website for almost 13 minutes on average. As the site picked up more visitors, so did the restaurant. Our work included photography, website development and print design.',
        'hover-effect-description' => 'website development, print design',
        'hover-effect-client-name' => 'Baci di Roma Trattoria',

    ],

    [
        'slug' => 'municode',
        'seoTitle' => 'municode | Website Development and Photography',
        'seoDescription' => 'municode',
        'seoKeywords' => 'municode, website design, branding, photography, photography, web developers, orange county',
        'categories' => ['category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/municode-logo.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/newhome/municode.jpg', 'alt'],

        ],
        'mainDescription' => '
		The old site was invisible to search engines and did not reflect it\'s brick and mortar counterpart. We knew that the site needed to reflect the atmosphere and feel of the restaurant.  What we did not expect was that people now stayed on the website for almost 13 minutes on average. As the site picked up more visitors, so did the restaurant. Our work included photography, website development and print design.',
        'hover-effect-description' => 'website development, print design',
        'hover-effect-client-name' => 'Baci di Roma Trattoria',

    ],
    [
        'slug' => 'the-edmon',
        'seoTitle' => 'The Edmon Restaurant | Logo Development and Web Design Project',
        'seoDescription' => 'The Edmon Restaurant has an art deco feel to their restaurant, and they looked to W Brand Studio to expand that look into logo design, menus and website development.',
        'seoKeywords' => 'logo design, restaurant marketing, restaurant website development, orange county agency, photography, menu development',
        'categories' => ['category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/edmon-1-5.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/edmon-web.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/edmon-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/edmon-door.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/edmon-photo-1.jpg', 'alt'],
        ],
        'mainDescription' => '
		The logo and website needed to reflect the nostalgic, art deco atmosphere and feel of the restaurant. Our work included logo design, website development, menus, and photography. We were awarded 2 Silver Awards from the American Advertising Federation\'s for our work. One locally in Orange County,  and on in strong competiton with national agencies in Los Angeles, Las Vegas, San Diego and other places on the West Coast.',
        'hover-effect-description' => 'website development, logo design',
        'hover-effect-client-name' => 'The Edmon',

    ],

    [
        'slug' => 'createapp-branding',
        'seoTitle' => 'CreateApp | Website Development and Design Project',
        'seoDescription' => 'Learn how CreateApp, an app development company in Singapore, partnered with the creative minds at W Brad Studio to create a corporate website and branding.',
        'seoKeywords' => 'website design, corporate branding, design, orange county web design, development',
        'categories' => ['category-1', 'category-3', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/createapp-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/createapp-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/createapp-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/createapp-3.jpg', 'alt'],
        ],
        'mainDescription' => '
		Weyland Tech and CreateApp are located in Singapore, however they chose to work with W Brand Studio for their branding and website development. Singapore is considered to be South East Asia\'s Silicon Valley, so it we felt beyond honored to have been chosen to help them in several countries.',
        'hover-effect-description' => 'Branding, Logo, website',
        'hover-effect-client-name' => 'CreateApp',
    ],

    [
        'slug' => 'meritage-branding',
        'seoTitle' => 'Meritage Real Estate | Marketing Project with W Brand Studio',
        'seoDescription' => 'See how Meritage Real Estate teamed up with the creative minds at W Brand Studio in Newport Beach to redesign their logo, branding and brochures.',
        'seoKeywords' => 'real estate marketing, branding, real estate graphic design, logo design, real estate agent branding',
        'categories' => ['category-2', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/meritage-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/meritage-0.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-5.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-brochure.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-6.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-7.jpg', 'alt'],
        ],
        'mainDescription' => '
		Branding, logo and brochure development.',
        'hover-effect-description' => 'Branding, Visual ID, Logo, Print',
        'hover-effect-client-name' => 'Meritage Realty',
    ],

    [
        'slug' => 'bowermaster',
        'seoTitle' => 'Bowermaster and Associate | Insurance Branding and Marketing Project',
        'seoDescription' => 'See how making strategic changes in our clients sales materials and presentations helped draw more business from new and past relationships.',
        'seoKeywords' => 'presentation, marketing assets, website development, orange county, newport beach, branding',
        'categories' => ['category-4', 'category-1', 'category-2', 'category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/Bowermaster-1-2.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/Bowermaster-web-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Bowermaster-web-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Bowermaster-old-and-new-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Bowermaster-stationary-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/Bowermaster-binder-1.jpg', 'alt'],
        ],
        'mainDescription' => '
		W Brand Studio was commissioned for a rebranding of the Bowermaster image. We also made significant changes in the way the company presented itself to its clients in all sales materials. Our work helped the sales staff stave off additional decline in existing clients and even drew the return of a few major former clients.',
        'hover-effect-description' => 'Branding, Logo, website',
        'hover-effect-client-name' => 'Bowermaster and Assoc.',
    ],

    [
        'slug' => 'theedmon',
        'seoTitle' => 'The Edmon Restaurant | Logo Development and Web Design Project',
        'seoDescription' => 'The Edmon Restaurant has an art deco feel to their restaurant, and they looked to W Brand Studio to expand that look into logo design, menus and website development.',
        'seoKeywords' => 'logo design, restaurant marketing, restaurant website development, orange county agency, photography, menu development',
        'categories' => ['category-3', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/theEdmon-design.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/theEdmon-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/theEdmon-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/theEdmon-3.jpg', 'alt'],
        ],
        'mainDescription' => '
The Edmon resides inside the Hollywood Historic Hotel. Built in 1927 by S. Charles Lee, the National Landmark captures the beauty of a bygone era. We designed their logo according to brand (not just design for design\'s sake), and created consistent design for menus, invites, website and more.',
        'hover-effect-description' => 'Logo, Website, Menus, Photography',
        'hover-effect-client-name' => 'The Edmon',
    ],

    [
        'slug' => 'calneva',
        'seoTitle' => 'Calneva Resort | Hospitality Branding and Marketing',
        'seoDescription' => 'See how W Brand Studio was able to create a program that truly sent the message to Calneva customers and boost the spirit of the team in within a short timeline.',
        'seoKeywords' => 'resort marketing, hospitality branding, graphic design, marketing agency, corporate branding, agency',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/CalNeva.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/CalNeva-marketing-ads.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/calneva-menu.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/calneva-menu-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/calneva-doorhangers.jpg', 'alt'],
        ],
        'mainDescription' => '
		Wollner and his team were able to take a struggling product, and within a very short period of time analyze and create a program that truly sent the message to our customers we wanted, but managed to boost the spirit of the entire team in the process. We saw not only direct increases in trackable profits, but were recognized by our competitive set for capitalizing on our uniqueness that W Brand Studio identified for our resort. 
Steve Tremewan, General Manager, CalNeva Resort Lake Tahoe',
        'hover-effect-description' => 'Branding, Marketing, Print',
        'hover-effect-client-name' => 'Cal Neva Lodge',
    ],

    [
        'slug' => 'buncher',
        'seoTitle' => 'Buncher Family Law Firm | Marketing and Branding by W Brand Studio',
        'seoDescription' => 'As a local, professional boutique law firm in Orange County, Buncher Law turned to the team at W Brand Studio for creative branding and marketing strategy.',
        'seoKeywords' => 'orange county branding, law marketing, legal marketing, corporate branding, website design, oc branding',
        'categories' => ['category-2', 'category-3', 'category-1', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Visual_Identity/buncher.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/buncher.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/buncher-web.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/buncher-web-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/buncher-web-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/buncher-web-4.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/buncher-folder.jpg', 'alt'],
        ],
        'mainDescription' => '
Buncher Law is a local, professional, preeminent boutique law firm in Orange County, California. They are your practical, rational and respected partner for the legal matters that involve Family Law, Business Litigation and Personal Injury.',
        'hover-effect-description' => 'Branding, Logo, Website, Photos',
        'hover-effect-client-name' => 'Buncher Law Corporation',
    ],
    [
        'slug' => 'millennium',
        'seoTitle' => 'Millennium Space System | Branding and Identity Project',
        'seoDescription' => 'Working with Millennium Space System involved a wide range of creative and marketing ideas, including slogan development and marketing materials.',
        'seoKeywords' => 'marketing experts, logo design, creative agency, brochure design, slogan development',
        'categories' => ['category-1', 'category-2', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/Millennium-1-2.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/millennium-ba.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-ba1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-brochures-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-brochures-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-brochures-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-10.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-4.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-5.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-6.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-7.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-8.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/millennium-9.jpg', 'alt'],
        ],
        'mainDescription' => '
		Our work included: branding, slogan, logo adjustment, corporate identity, stationary, brocures, including exterior and interior design and much more. 
We created the slogan "Creative Thinking Applied" that provides an umbrella under which the expert human component and affordable cost component can be sheltered. However, by no means does the slogan alone capture the Millennium core identity. It simply suggests that Millennium is an "outside-the-box", expert that is both efficient, innovative and creative on every level of a project. ',
        'hover-effect-description' => 'Branding, interior design, exterior design. print material',
        'hover-effect-client-name' => 'Millennium Space System',
    ],

    [
        'slug' => 'prr',
        'seoTitle' => 'PRR | Branding and Identity Project',
        'seoDescription' => 'PRR, including slogan development and marketing materials.',
        'seoKeywords' => 'marketing experts, logo design, creative agency, brochure design, slogan development',
        'categories' => ['category-1', 'category-2', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/newhome/prr.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/newhome/prr.jpg', 'alt'],

        ],
        'mainDescription' => '
		Our work included: branding, slogan, logo adjustment, corporate identity, stationary, brocures, including exterior and interior design and much more. 
We created the slogan "Creative Thinking Applied" that provides an umbrella under which the expert human component and affordable cost component can be sheltered. However, by no means does the slogan alone capture the Millennium core identity. It simply suggests that Millennium is an "outside-the-box", expert that is both efficient, innovative and creative on every level of a project. ',
        'hover-effect-description' => 'Branding, interior design, exterior design. print material',
        'hover-effect-client-name' => 'Millennium Space System',
    ],

    [
        'slug' => 'shoring',
        'seoTitle' => 'Shoring Engineers | Website Development and Brochure Design',
        'seoDescription' => 'The creative team at W Brand Studio was tasked by Shoring Engineers to develop a new website and brochure while incorporating their current branding.',
        'seoKeywords' => 'brochure design, website development, construction marketing, Newport beach agency, corporate branding',
        'categories' => ['category-5', 'category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/Shoring-2-1.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/shoring-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-3b.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-web.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-photos-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-photos-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/shoring-photos.jpg', 'alt'],
        ],
        'mainDescription' => '
		For over fifty years Shoring Engineers has been a pillar of strength from San Francisco to San Diego and beyond. Our task was to create a new website and brochure within their current brand strategy.',
        'hover-effect-description' => 'Print, Website',
        'hover-effect-client-name' => 'Shoring Engineers',
    ],

    [
        'slug' => 'subir',
        'seoTitle' => 'Subir Chopwdhury | Quality Management iPad App Design',
        'seoDescription' => 'As a thought leader in the quality management space , Subir Chopwdhury partnered with our team for the layout and design of their iPad application.',
        'seoKeywords' => 'iPad design, branding, marketing, orange county agency, Newport beach marketing, design firm',
        'categories' => ['category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/Subir-1-1.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/subir-0.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/subir-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/subir-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/subir-3.jpg', 'alt'],
        ],
        'mainDescription' => '
		Subir Chowdhury has been a thought leader in quality management strategy and methodology for more than 20 years. Currently Chairman and CEO of ASI Consulting Group, LLC, he leads Six Sigma and Quality Leadership implementation, and consulting and training efforts. When it came to quality in design, he came to W Brand Studio to create the design and layout of his iPad app about Quality Management.',
        'hover-effect-description' => 'Design, iPad App',
        'hover-effect-client-name' => 'Subir Chowdhury',
    ],

    [
        'slug' => 'ellis',
        'seoTitle' => 'Ellis Paint Company | Corporate Branding and Strategy',
        'seoDescription' => 'Our client was seeking a strategic evaluation of their current market position with major focus on being able to "speak with one voice", through a new concept.',
        'seoKeywords' => 'marketing evaluation, corporate rebranding, website development, marketing strategy',
        'categories' => ['category-1', 'category-2', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/Ellis-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/ellis-logo-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/ellis-before-after.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/ellis-colorchart-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/ellis-colorchart-2.jpg', 'alt'],

        ],
        'mainDescription' => '
		The branding process for Ellis Paint included a strategic evaluation of their current market position, a look at perceptions in the market place, and create a new direction for the company. The goal for their Corporate Identity refresh was to focus on being able to "speak with one voice" and to enhance and evolve an existing image through a new concept.',
        'hover-effect-description' => 'Branding, Logo, Print, web',
        'hover-effect-client-name' => 'Ellis Paint Company',
    ],

    [
        'slug' => 'sentry',
        'seoTitle' => 'Sentry Control Systems | SKIDATA | Website Design and Corporate Branding',
        'seoDescription' => 'As a parking, revenue control and access company, Sentry Control Systems partnered with W Brand Studio to enhance their strategic marketing and branding initiatives.',
        'seoKeywords' => 'corporate branding, marketing agency, logo development, Newport beach website design, orange county agency',
        'categories' => ['category-3', 'category-2', 'category-1', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/sentry.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/sentry-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sentry-biz-cards.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sentry-web.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sentry-signage.jpg', 'alt'],
        ],
        'mainDescription' => '
		Sentry is a parking, revenue control, and access systems integrator that offers world-class customer help and innovative quality solutions to optimize revenue for their customers. We have worked with Sentry since 2009 and have been involved with the initial branding and the current re-branding after SKIDATA became a major shareholder earlier this year. Our work included: branding, brand manual, logo development, website design, photography, vehicle wraps, interior and exterior designs, posters, signage and more. Visit www.sentrycontrol.com for more info.',
        'hover-effect-description' => 'Branding, Logo, Web, print, SEO/SEM',
        'hover-effect-client-name' => 'Sentry Control Systems',
    ],

    [
        'slug' => 'lgarde',
        'seoTitle' => 'L.Garde Aerospace | Marketing and Corproate Branding',
        'seoDescription' => 'When came time for a complete corporate rebranding project, L.Garde came to W Brand Studio for strategy and concept execution',
        'seoKeywords' => 'logo design, corporate branding, branding agency, website design, orange county agency, marketing company',
        'categories' => ['category-1', 'category-2', 'category-4', 'category-all'],
        'thumbImageFile' => ['assets/images/work/thumbnails/Branding/LGARDE-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/lgarde-old-new.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lgarde-old-new-interior.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lgarde-bizcards.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lgarde-letterhead.jpg', 'alt'],
        ],
        'mainDescription' => '
		L.Garde was founded in 1971 in Orange County, CA to support ballistic missile defense through the development and manufacture of inflatable targets and decoy systems. When it was time to refresh a dated image for an aerospace company, we came in with a Totally Branding concept which included a new logo, interior and exterior design, printed materials and more.',
        'hover-effect-description' => 'Branding, Logo, website, print',
        'hover-effect-client-name' => 'L.GARDE Space Technology',
    ],

    [
        'slug' => 'cstpaint',
        'seoTitle' => 'CST Paint | Product Design and Branding | Corporate Web Development',
        'seoDescription' => 'product marketing, product branding, print media, web design, corporate marketing',
        'seoKeywords' => 'website design, corporate branding, design, orange county web design, development',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/Sheeld-1-6.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/sheeld-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/CST-paint-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/CST-paint-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sheeld-prototype-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sheeld-prototype-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sheeld-prototype-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/sheeld-prototype-4.jpg', 'alt'],
        ],
        'mainDescription' => '
		Sheeld is a Waterborne Acrylic Heat Reflective Roof Coating designed to reflect solar heat and ultraviolet rays (UV) as well as a protective coating for most roof substrates. To demonstrate how the new paint will shield heat, we designed and created a prototype that could demonstrate the difference between this and other brands. We named the product SHEELD, designed paint labels, print materials and website. ',
        'hover-effect-description' => 'Naming, Logo, Prototype, Print, Labels',
        'hover-effect-client-name' => 'CST PAINT',
    ],

    [
        'slug' => 'coastal',
        'seoTitle' => 'coastal heart | Product Design and Branding | Corporate Web Development',
        'seoDescription' => 'product marketing, product branding, print media, web design, corporate marketing',
        'seoKeywords' => 'website design, corporate branding, design, orange county web design',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/newhome/coastalheart.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/newhome/coastalheart.jpg', 'alt'],

        ],
        'mainDescription' => '
		Sheeld is a Waterborne Acrylic Heat Reflective Roof Coating designed to reflect solar heat and ultraviolet rays (UV) as well as a protective coating for most roof substrates. To demonstrate how the new paint will shield heat, we designed and created a prototype that could demonstrate the difference between this and other brands. We named the product SHEELD, designed paint labels, print materials and website. ',
        'hover-effect-description' => 'Naming, Logo, Prototype, Print, Labels',
        'hover-effect-client-name' => 'CST PAINT',
    ],

    [
        'slug' => 'rynnJanowsky',
        'seoTitle' => 'Rynn & Janowsky | Law Firm Branding | Legal Firm Marketing and Design',
        'seoDescription' => 'As one of the largest Agricultural firms in the nation, Rynn & Janowsky worked with the creative group at W Brand Studio for a brand refresh and graphic design.',
        'seoKeywords' => 'graphic design, marketing, branding, orange county agency, Newport beach branding',
        'categories' => ['category-2', 'category-3', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Visual_Identity/RynnJ-visualID.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/rynnJanowsky-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rjlawweb2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rjlawweb1.jpg', 'alt'],
        ],
        'mainDescription' => '
		Since 1986, Rynn & Janowsky, LLP has been steadily growing to become one of the largest and busiest agricultural firms nationwide. We helped them with a brand refresh which included logo design, website development, photography, stationary, newsletters, business cards and misc. print designs.',
        'hover-effect-description' => 'Branding, Logo, Web, photos',
        'hover-effect-client-name' => 'Rynn & Janowsky Law Firm',
    ],

    [
        'slug' => 'lwv-branding',
        'seoTitle' => 'Laguna Woods Village | Re-branding Project with W Brand Studio',
        'seoDescription' => 'When an active adult community needed a fresh look, they turned to the team at W Brand Studio for creative help.',
        'seoKeywords' => 'logo design, marketing team, laguna hills, oc branding agency, newport beach',
        'categories' => ['category-1', 'category-2', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/LWV-branding.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/lwv-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lwv-bizCards.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lwv-vehicles.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lwv-envelope.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lwv-merch.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/lwv-coffee.jpg', 'alt'],
        ],
        'mainDescription' => '
		We embraced the difficult challenge of re-branding a 40-year-old "active adult" community, and produced a product that is fresh, current, attractive and appropriate. The new Laguna Woods Village logo, reflects the vitality and strength of the community. We also provided a comprehensive re-branding strategic plan. ',
        'hover-effect-description' => 'Branding, Logo, Signage, Vehicles, Print',
        'hover-effect-client-name' => 'Laguna Woods Village',
    ],

    [
        'slug' => 'eliant',
        'seoTitle' => 'Eliant | Re-branding Project with W Brand Studio',
        'seoDescription' => 'eliant',
        'seoKeywords' => 'logo design, marketing team, laguna hills, oc branding agency, newport beach',
        'categories' => ['category-1', 'category-2', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/eliant.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/newhome/questionsanswered.jpg', 'alt'],

        ],
        'mainDescription' => '
		We embraced the difficult challenge of re-branding a 40-year-old "active adult" community, and produced a product that is fresh, current, attractive and appropriate. The new Laguna Woods Village logo, reflects the vitality and strength of the community. We also provided a comprehensive re-branding strategic plan. ',
        'hover-effect-description' => 'Branding, Logo, Signage, Vehicles, Print',
        'hover-effect-client-name' => 'Laguna Woods Village',
    ],

    [
        'slug' => 'regency',
        'seoTitle' => 'Regency Hotel Miami | Hospitality Marketing and Design',
        'seoDescription' => 'See how the Regency Hotel in Miami looked for branding and graphic design help from the experienced group at W Brand Studio in Newport Beach',
        'seoKeywords' => 'hotel marketing, graphic design, hospitality branding, brochure design, hotel branding',
        'categories' => ['category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Logo/Regency-1-7.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/regency-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/RH-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/regency-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/regency-5.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/regency-email-signature.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/prieto-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/regency-prieto.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/regency-carrental.jpg', 'alt'],
        ],
        'mainDescription' => '
		With a distinctive boutique ambiance, the Regency is a leading choice amongst Miami hotels. Their personalized service, attention to detail, and independent mindset differentiate us from our competitors. From the busy corporate executive to the family of four, each Regency Hotel guest expects a pleasant, personalized stay. Hotel',
        'hover-effect-description' => 'Logo, Branding, misc',
        'hover-effect-client-name' => 'Regency Hotel Miami',
    ],

    [
        'slug' => 'REST-Magazine',
        'seoTitle' => 'REST Magazine Design Project | Layout and Photography with W Brand Studio',
        'seoDescription' => 'See how we help REST Magazine establish an emotional connection with their readership when strategically catering into psychographics over a demographic profile',
        'seoKeywords' => 'magazine marketing, boutique branding, newport beach agency, design, layout, orange county design',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/REST-design.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/restmagazine-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/restmagazine-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/restmagazine-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/restmagazine-4.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/restmagazine-5.jpg', 'alt'],
        ],
        'mainDescription' => '
		REST Magazine puts the spotlight on boutique hotels, green resorts and more. The magazine is differentiated by its ability to establish an emotional connection with readers by catering to their psychographics rather than demographic profile.  Tasks included naming, layout & design, writing all articles and take all the photography.',
        'hover-effect-description' => 'Naming, Logo, Articles, photos',
        'hover-effect-client-name' => 'Rest Magazine',
    ],

    [
        'slug' => 'vineyard',
        'seoTitle' => 'Vineyard Roz | Visual Identity and Logo',
        'seoDescription' => 'See how a premier wine lounge and restaurant in Southern California turned to the team at W Brand Studio for a fresh visual identity',
        'seoKeywords' => '',
        'categories' => ['category-2', 'category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Visual_Identity/ROZ-2-5.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/vineyard-roz-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/vineyard-roz.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/vineyard-roz-web.jpg', 'alt'],

        ],
        'mainDescription' => '
		We were contacted by Vineyard Roz to create a fresh visual identity and logo which would represent a premier wine lounge and restaurant offering California Coastal Cuisine and a premier wine collection from around the world. ',
        'hover-effect-description' => 'Logo, visual ID',
        'hover-effect-client-name' => 'Vineyard Roz Wine Lounge',
    ],

    [
        'slug' => 'rsque-brand',
        'seoTitle' => 'RSQUE BRAND | Animal Rescue Inspired Apparel | Branding and Web Design',
        'seoDescription' => 'As an animal rescue apparel company, RSQUE BRAND turned to W Brand Studio for logo, branding, apparel and booth design for the America Family Pet Expo.',
        'seoKeywords' => 'logo design, booth creation, apparel design, website development, animal rescue marketing, agency',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/RSQUE-BRAND-design.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/rsque-9.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rsque-8.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rsque-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rsque-3b.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rsque-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/rsque-7.jpg', 'alt'],
        ],
        'mainDescription' => 'One of our favorite things to do is to brand companies that donate part of their proceeds to animal rescue organizations. We named the startup company RSQUE BRAND, created the branding, visual identity, website, t-shirt design and booth for the Americas Family Pet Expo.',
        'hover-effect-description' => 'Naming, Logo, Articles, photos',
        'hover-effect-client-name' => 'RSQUE BRAND',
    ],

    [
        'slug' => 'nestle',
        'seoTitle' => 'Willy Wonka | Vehicle Design and Graphics',
        'seoDescription' => 'We worked with Nestle to design a creative and eye catching vehicle wrap design for their Willy Wonka candy brand.',
        'seoKeywords' => 'vehicle branding, car wrap, candy marketing, graphic design',
        'categories' => ['category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/Nestle-3-5.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/coming-soon.jpg', 'alt'],

        ],
        'mainDescription' => '
		Graphic design and illustration for Nestle and their Willy Wonka vehicle.',
        'hover-effect-description' => 'Illustration, vehicle wrap',
        'hover-effect-client-name' => 'Nestle, Willy Wonka',
    ],

    [
        'slug' => 'pekepom',
        'seoTitle' => 'Peke and Pom Dog Rescue | Logo and Graphic Design',
        'seoDescription' => 'As a dog rescue in Colorado, Peke and Pom needed a creative logo that represented their brand. So they turned to the creative, animal loving team at W Brand Studio',
        'seoKeywords' => 'animal shelter, dog rescue, logo design, animal marketing, graphic design, rebranding',
        'categories' => ['category-4', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Logo/PekePom-3-1.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/pekeandpom-logo.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/peke-and-pom-website.jpg', 'alt'],

        ],
        'mainDescription' => '
		We love to work with animal rescue organizations such as Peke and Pom Rescue from Colorado. We developed a new logo that would reflect on the breed and Colorado and a website which was much more effective when it comes to posting available dogs. Since their inception in 2007, they have rescued more than 1600 little dogs. A number that truly amazes us!',
        'hover-effect-description' => 'Logo, web, print',
        'hover-effect-client-name' => 'Peke & Pom Rescue',
    ],

    [
        'slug' => 'twa',
        'seoTitle' => 'The Wild Adventure | Website and Branding Project with W Brand Studio',
        'seoDescription' => 'See how we helped The Wild Adventure with their overall website and branding enhancements that include media printing design, covers and inserts',
        'seoKeywords' => 'website branding, web development, cd design, dvd design, cd covers, orange county branding',
        'categories' => ['category-3', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/TWA-2-3.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/twa-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/twa-2.jpg', 'alt'],
        ],
        'mainDescription' => '
		Work included website development and maintenance, misc. print projects such as logo development, DVD and CD covers and inserts.',
        'hover-effect-description' => 'Graphic Design, web',
        'hover-effect-client-name' => 'The Wild Adventure',
    ],

    [
        'slug' => 'transmarine',
        'seoTitle' => 'Transmarine | Website and Print Design Project',
        'seoDescription' => 'See how the team at W Brand Studio was able to create a website that mirrored the Transmarine brand while differentiating them from the competition.',
        'seoKeywords' => 'website design, website development, orange county brochure design, marketing agency, newport beach',
        'categories' => ['category-3', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Web_Interactive/transmarine.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/transmarine-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/transmarine-web-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/transmarine-web-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/transmarine-web-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/transmarine-folder-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/transmarine-folder-2.jpg', 'alt'],
        ],
        'mainDescription' => '
		Transmarine is a specialist in tanker and dry cargo operations, no matter what the problem or where the port. Our task was to create a website that mirrored their brand and differentiated them from their competitors. The project also included misc. print design.',
        'hover-effect-description' => 'website, print',
        'hover-effect-client-name' => 'Transmarine',
    ],

    [
        'slug' => 'gsroc',
        'seoTitle' => 'German Shepherd Rescue of Orange County | Web Design & Branding',
        'seoDescription' => 'See how we worked with the German Shepherd rescue of Orange County on their merchandise, branding and website development.',
        'seoKeywords' => 'web design, merchandise, corporate branding,  branding agency, orange county ca',
        'categories' => ['category-3', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/gsroc-thumb.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/gsroc-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/gsroc-2.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/gsroc-3.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/gsroc-4.jpg', 'alt'],
        ],
        'mainDescription' => '
		Work included website development, database development, branding, merchandise and more.',
        'hover-effect-description' => 'Naming, logo, website, database, print design, merchandise',
        'hover-effect-client-name' => 'German Shepherd Rescue',
    ],

    [
        'slug' => 'habitat',
        'seoTitle' => 'Habitat Hospitality | Logo Design and Website Project',
        'seoDescription' => 'Our branding and marketing design for Habitat Hospitality involved  developing a new visual identity and create their website & logo.',
        'seoKeywords' => 'graphic design, corporate rebranding, web development, orange county, newport beach, branding agency',
        'categories' => ['category-3', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Branding/habitat-thumb.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/habitat-1.jpg', 'alt'],

        ],
        'mainDescription' => '
		The work for Habitat Hospitality included logo development, website development and visual identity.',
        'hover-effect-description' => 'Visual Identity, logo, website development.',
        'hover-effect-client-name' => 'Habitat Hospitality'
    ],

    [
        'slug' => 'altonpark',
        'seoTitle' => 'Alton Park Realty | Branding and Design Project',
        'seoDescription' => 'Alton Park Realty turned to W Brand Studio for creative development and design of their visual identity, signage, corporate presentation portfolios, business cards and stationary..',
        'seoKeywords' => 'brand development, real estate marketing, logo design, presentation, marketing collateral',
        'categories' => ['category-3', 'category-2', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/alton-thumb.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/alton-park-1.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/alton-park-2.jpg', 'alt'],
        ],
        'mainDescription' => '
		The work for Alton Park Realty included basic branding, tagline development, visual identity, signage, presentation portfolios, stationary and business cards.',
        'hover-effect-description' => 'Visual ID, branding, logo, print',
        'hover-effect-client-name' => 'Alton Park Realty',
    ],

    [
        'slug' => 'coloraroma',
        'seoTitle' => 'Color and Aroma Magazine | Layout and Design Project with W Brand Studio',
        'seoDescription' => 'When Color and Aroma Magazine needed help with creative and layout design help, they trusted the experienced team at W Brand Studio to deliver results.',
        'seoKeywords' => 'photography, branding agency, orange county branding, photography, logo design, newport beach marketing',
        'categories' => ['category-3', 'category-5', 'category-all'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Design/color-aroma-thumb.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/color-aroma-mags.jpg', 'alt'],

        ],
        'mainDescription' => '
		Work incudes naming, logo, magazine layout and design, photography, writing articles and more. ',
        'hover-effect-description' => 'Naming, design, layout, logo and more',
        'hover-effect-client-name' => 'Color and Aroma Magazine',
    ],

    [
        'slug' => 'meritage-branding',
        'seoTitle' => 'meritage real estate visual identity',
        'seoDescription' => 'real estate marketing and website development',
        'categories' => ['category-4'],
        'thumbImageFile' => ['/assets/images/work/thumbnails/Logo/Meritage-3-8.jpg', 'alt'],
        'mainImageFiles' => [
            ['/assets/images/work/Large_Photos/meritage-0.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-5.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-brochure.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-6.jpg', 'alt'],
            ['/assets/images/work/Large_Photos/meritage-7.jpg', 'alt'],
        ],
        'mainDescription' => '
		The work completed for Meritage Realty included Visual Identity, logo development, stationary, business cards, signage and brochure development.',
        'hover-effect-description' => 'Logo, Visual ID',
        'hover-effect-client-name' => 'Meritage Realty',
    ],
];
