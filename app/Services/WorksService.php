<?php

namespace App\Services;

class WorksService
{
    protected $work;

    public function __construct()
    {
        $this->work = collect(require __DIR__.'/data/work.php');
    }

    public function getAll()
    {
        return $this->work;
    }

    public function getBySlug($slug)
    {
        return $this->work->where('slug', $slug)->first();
    }
}
