<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $data;

    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->data['_message'] = $data['message'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('W Brand Studio Contact Form');
        $this->from(['W Brand Studio Contact Form' => 'aidan@wbrandstudio.com']);
        $this->to(env('CONTACT_FORM_TARGET', 'aidandang@gmail.com'));

        return $this->view('mail.contact')->with($this->data);
    }
}
