<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
class PagesController extends Controller
{
    public function home()
    {
        return view('pages.home');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function commercialsVideoProduction()
    {
        return view('pages.commercials-video-production');
    }
     
     public function logoDesign()
    {
        return view('pages.logo-design');
    }  
    
    public function interiorDesign()
    {
        return view('pages.interior-design');
    }
    public function writeUs()
    {
        return view('pages.write-to-us');
    }

    public function careers()
    {
        return view('pages.careers');
    }

    public function testimonials()
    {
        return view('pages.testimonials');
    }

    public function thankyou()
    {
        return view('pages.thankyou');
    }

    public function video()
    {
        return view('pages.video');
    }

    public function marketingagency()
    {
        return view('pages.lp.marketing-agency');
    }

    public function topmarketingagency()
    {
        return view('pages.top-marketing-agency');
    }

    public function increaseWebsiteTraffic()
    {
        return view('pages.increase-website-traffic');
    }

    public function interview()
    {
        return view('pages.interview');
    }

    public function digitalMarketingAgency()
    {
        return view('pages.digital-marketing-agency');
    }

    public function awardWinningAgency()
    {
        return view('pages.award-winning-agency');
    }
   
    public function marketingFirm()
    {
        return view('pages.marketing-firm');
    }

    public function hospitalityMarketing()
    {
        return view('pages.lp.hospitality-marketing');
    }
    public function branding()
    {
        return view('pages.branding');
    }
    public function brandingAgency()
    {
        return view('pages.lp.branding-agency');
    }
    public function brandingAgencyOC()
    {
        return view('pages.lp.branding-agency');
    }
    public function brandingCompany()
    {
        return view('pages.lp.branding-company');
    }

    public function brandingMarketingExperts()
    {
        return view('pages.branding-marketing-experts');
    }

    public function privacy()
    {
        return view('pages.privacy');
    }

    public function brandingAgencyGgl()
    {
        return view('pages.branding-agency-ggl');
    }

    public function brandingFirm()
    {
        return view('pages.branding-firm');
    }

    public function advertisingAgency()
    {
        return view('pages.lp.advertising-agency');
    }

    public function brandingPhilosophy()
    {
        return view('pages.branding-philosophy');
    }

    public function brandRefresh()
    {
        return view('pages.brand-refresh');
    }

    public function disclaimer()
    {
        return view('pages.disclaimer');
    }

    public function onlineMarketing()
    {
        return view('pages.online-marketing-agency-orange-county');
    }

    public function customWebsiteDesigners()
    {
        return view('pages.custom-website-designers');
    }

    public function searchEngineExperts()
    {
        return view('pages.search-engine-experts');
    }

    public function marketingAgencyLanding()
    {
        return view('pages.marketing-agency-landing');
    }

    public function fastestGrowingBrandingAgency()
    {
        return view('pages.fastest-growing-branding-agency');
    }

    public function biggestSmallAgency(){
        return view('pages.biggest-small-agency');
    }

    public function topBrandingAgency(){
        return view('pages.top-branding-agency');
    }

    public function pureBranding(){
        return view('pages.pure-branding');
    }

    public function winning(){
        return view('pages.winning');
    }

    public function madmen(){
        return view('pages.madmen');
    }

    public function webDesign(){
        return view('pages.lp.web-design');
    }
    public function vWebsiteDesign(){
        $projects = Project::latest()->get();
        return view("pages.v.website-design")->with('projects', $projects);
    }
}
