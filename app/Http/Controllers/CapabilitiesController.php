<?php

namespace App\Http\Controllers;
use App\Project;
use Illuminate\Http\Request;

class CapabilitiesController extends Controller
{
    public function index()
    {
        return view('pages.capabilities.index');
    }

    public function show($capability)
    {
        if ($capability == 'index') {
            return redirect('/capabilities', 301);
        }
        if ($capability == 'website-design') {
            $projects = Project::latest()->get();
            return view("pages.capabilities.$capability")->with('projects', $projects);
        }
        try {
            return view("pages.capabilities.$capability");
        }
        catch (\Exception $e) {
            abort(404);
        }
    }
}
