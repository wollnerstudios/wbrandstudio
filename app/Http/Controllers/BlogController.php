<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;

class BlogController extends Controller
{
    public function blog() {
        $posts = Post::orderby('created_at', 'desc')->simplePaginate(12);
        $featured = Post::orderby('created_at', 'desc')->first();
        return view('pages.blog')->with('posts', $posts)->with('featured', $featured);

    }
    public function blogpost($id) {
        $post = Post::where('slug', $id)->first();
        return view('pages.blog-post')->with('post', $post);

    }
}
