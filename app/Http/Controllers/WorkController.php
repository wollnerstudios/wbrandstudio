<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\WorksService;

class WorkController extends Controller
{
    public function index()
    {
        $singlePage = false;

        return view('pages.work', array_merge($this->getSharedData(), compact('singlePage')));
    }

    public function show($slug)
    {
        $service = new WorksService();
        $work = $service->getBySlug($slug);
        $singlePage = true;

        if (! $work) {
            abort(404);
        }

        return view('pages.work', array_merge($this->getSharedData(), compact('work', 'singlePage')));
    }

    protected function getSharedData()
    {
        $allWork = (new WorksService())->getAll();
        $mobileImages = [
            ['imageFile' => ['/assets/images/mobile/work/baci-web-2.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/CalNeva-marketing-ads.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/calneva-menu-2.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/L-garde-exterior.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/lgarge-bizcards.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/marriott-5.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/meritage-0.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/millennium-1.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/millennium-6.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/shoring-2.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/shoring-4.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/shoring-4b.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/subir-1.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/subir-2.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/yamaha-avantgrand-1.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/Yamaha-CVP600.jpg', 'alt']],
            ['imageFile' => ['/assets/images/mobile/work/Yamaha-photoshoot.jpg', 'alt']],
        ];

        return compact('allWork', 'mobileImages');
    }
}
