<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Swift_Mailer;
use Swift_MailTransport;
use Illuminate\Support\Facades\Mail;
use Swift_Message;
use GuzzleHttp\Client;
use GuzzleHttp;

class ContactController extends Controller
{
    public function index()
    {
        return view('pages.contact');
    }

    // STEP 3: SUBMIT FUNCTION EXECUTES
    public function submit(Request $request)
    {
//        $contactForm = new ContactForm($request->only('name', 'company', 'email', 'phone', 'message'));
//
//        $contactForm->send(app('mailer'));
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'phone' => $request->phone,
            '_message' => $request->message,
            'service1' => $request->service1,
            'service2' => $request->service2,
            'service3' => $request->service3,
            'service4' => $request->service4,
            'service5' => $request->service5,
            'service6' => $request->service6,
            'service7' => $request->service7,
            'service8' => $request->service8,
            'service9' => $request->service9,
            'service10' => $request->service10,
            'service11' => $request->service11,
            'service12' => $request->service12,

        );
        /*$client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',

                ['form_params'=>
                    [
                        'secret'=>'6LcftLUUAAAAAMWQgxWOSA9WXb7r3AYhmHcAM9Eo',
                        'response'=>$request->recaptcha
                    ]
                ]
            ,
            ['Content-Type' => 'application/json']
        );

        $responseJSON = json_decode($response->getBody(), true);*/


        $email = $request->email;
   
        Mail::send('mail.contact', ['data' => $data], function ($message) use ($email) {
            $message->subject('New Contact Submission');
            $message->from('hello@wbrandstudio.com');
            $message->to('michael@wbrandstudio.com');
            $message->cc('wolstudios@mac.com');
            $message->cc('john@wbrandstudio.com');
            $message->cc('johnmancuso79@gmail.com');
        });
        return redirect('/thankyou');

        // REVERTED HCAPTCHA CHANGES
        // $data = array(
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'company' => $request->company,
        //     'phone' => $request->phone,
        //     '_message' => $request->message,
        //     'service1' => $request->service1,
        //     'service2' => $request->service2,
        //     'service3' => $request->service3,
        //     'service4' => $request->service4,
        //     'service5' => $request->service5,
        //     'service6' => $request->service6,
        //     'service7' => $request->service7,
        //     'service8' => $request->service8,
        //     'service9' => $request->service9,
        //     'service10' => $request->service10,
        //     'service11' => $request->service11,
        //     'service12' => $request->service12,
        //     // HCAPTCHA
        //     'secret' => env('HCAPTCHA_SITE_KEY'),
        //     'response' => $_POST['h-captcha-response']
        // );

        // // Initiates a cURL request to the hCaptcha endpoint.
        // // $ch is a cURL handle.
        // $ch = curl_init();
        // // curl_setopt() configure the request.
        // // The URL is set to the hCaptcha verification endpoint.
        // curl_setopt($ch, CURLOPT_URL, env('HCAPTCHA_VERIFY_URL'));
        // // The request method is set to POST.
        // curl_setopt($ch, CURLOPT_POST, true);
        // // The request body is set to $data.
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        // // CURLOPT_RETURNTRANSFER set to true to return the response as a string.
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // // $response is the response after its been configured.
        // $response = curl_exec($ch);
        // // $responseData is the $response converted from a JSON object to a PHP object.
        // $responseData = json_decode($response);

        // if($responseData->success) {
        //     $email = $request->email;

        //     Mail::send('mail.contact', ['data' => $responseData], function ($message) use ($email) {
        //         $message->subject('New Contact Submission');
        //         $message->from('hello@wbrandstudio.com');
        //         $message->to('christiandeandemesa@gmail.com');
        //         // $message->to('michael@wbrandstudio.com');
        //         // $message->cc('wolstudios@mac.com');
        //         // $message->cc('john@wbrandstudio.com');
        //         // $message->cc('johnmancuso79@gmail.com');
        //     });
        //     return redirect('/thankyou');
        // } else {
        //     echo 'Robot verification failed, please try again.';
        // }




    }

    public function consultContact(Request $request) {

        $consultForm = new ContactForm($request->only('name', 'email', 'phone', 'message'));

        $consultForm->send(app('mailer'));
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'company' => $request->company,
            'phone' => $request->phone,
            '_message' => $request->message,

        );



        $email = $request->email;
        Mail::send('mail.contact', ['data' => $data], function ($message) use ($email) {
            $message->subject('New Contact Submission');
            $message->from('hello@wbrandstudio.com');
            $message->to('michael@wbrandstudio.com');

        });
        return redirect('/thankyou');
    }

    public function websiteContact(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'price' => 'required',



        ]);


        $client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',

            ['form_params'=>
                [
                    'secret'=>'6LcftLUUAAAAAMWQgxWOSA9WXb7r3AYhmHcAM9Eo',
                    'response'=>$request->recaptcha
                ]
            ]
            ,
            ['Content-Type' => 'application/json']
        );

        $responseJSON = json_decode($response->getBody(), true);

        $email = $request->email;
        $content = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'company' => $request->company,
            'price' => $request->price,
            'page' => $request->page,
            'service1' => $request->service1,
            'service2' => $request->service2,
            'service3' => $request->service3,
            'service4' => $request->service4,
            'service5' => $request->service5,
            'service6' => $request->service6,
            'service7' => $request->service7,
            'service8' => $request->service8,
            'service9' => $request->service9,
            'service10' => $request->service10,
            'service11' => $request->service11,
            'service12' => $request->service12,
            'comments' => $request->comments,
        ];


        if($responseJSON['success']){
            Mail::send('mail.website', ['content' => $content], function ($message) use ($email) {
                $message->subject('New Contact Submission');
                $message->from('hello@wbrandstudio.com');
                $message->to('michael@wbrandstudio.com');

            });

            return redirect('/thankyou')->with('message', 'Your message has been sent!');
        }
        else {
            return back()->with('message', 'Captcha Error');
        }

    }
}
